﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;

namespace AfisStrip.Model
{

	/// <summary>
	/// Staticka trieda MyContainer je spolocnym uloziskom pre IoC container (UnityContainer), pomocou ktoreho 
	/// sa daju resolvovat a ziskavat instancie roznych sluzieb. 
	/// </summary>
	public static class MyContainer
	{

		/// <summary>
		/// Backing-field pre instanciu kontainera
		/// </summary>
		private static IUnityContainer _container = null; 


		/// <summary>
		/// Vrati aktualne platnu instanciu IoC kontainera. 
		/// 
		/// Tato property je read-only, aby nemohlo dojst "omylom" k zapisu a prepisu instancie kontainera. 
		/// K nastaveniu platnej instancie kontainera sluzi metoda SetContainer(). 
		/// 
		/// </summary>
		public static IUnityContainer Instance {
			get { return _container; }
		}


		public static void SetContainer(IUnityContainer container)
		{
			_container = container; 
		}



	}
}
