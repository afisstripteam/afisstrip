﻿using AfisStrip.Model.Entities;

namespace AfisStrip.Model.DataAccessLayer
{

	/// <summary>
	/// Interface definijuci, co vsetko musi splnat kazda implementacia poskytovatela (dohladavaca) 
	/// lietadiel. 
	/// </summary>
	public interface IAirplaneProvider
	{

		/// <summary>
		/// Podla zadanej registracnej znacky dohlada a vrati udaje o lietadle, alebo NULL ak 
		/// ziadne lietadlo sa nedokazalo najst.
		/// 
		/// Je podstatne, aby implementator zarucil vnimanie hodnoty registration ako case-insensitive, a sam si ju 
		/// normalizoval na pozadovany format (typicky na UPPER CASE)!   
		/// 
		/// Tato metoda je BLOKUJUCA - ak ma byt spustena na pozadi, volajuci si musi zabezpecit jej spustenie na pozadi.  
		/// </summary>
		/// <param name="registration"></param>
		/// <returns></returns>
		Airplane GetAirplane(string registration); 


	}
}
