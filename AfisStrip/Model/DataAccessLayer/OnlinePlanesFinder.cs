﻿using System;
using AfisStrip.Model.Entities;

namespace AfisStrip.Model.DataAccessLayer
{

	/// <summary>
	/// Implementacia "pristupu k datam", ktory pristupuje k datam na webovom portali UCL a snazi sa dohladat 
	/// lietadla podla imatrikulacky.
	/// 
	/// Vyuzit sa k tomu da portal: http://portal.caa.cz/letecky-rejstrik  
	/// 
	/// </summary>
	public class OnlinePlanesFinder : IAirplaneProvider
	{


		/// <summary>
		/// Pokusi sa dohladat lietadlo na webe UCL -- ak ho nenajde, tak vrati NULL. 
		/// 
		/// Okrem tejto blokujucej verzie moze tato trieda implementovat aj async metodu. 
		/// </summary>
		/// <param name="registration"></param>
		/// <returns></returns>
		public Airplane GetAirplane(string registration)
		{
			throw new NotImplementedException();
		}

	}
}
