﻿using AfisStrip.Model.Entities;
using System.Collections.Generic;
using System.Linq;
using AfisStrip.Libs;

namespace AfisStrip.Model.DataAccessLayer
{

	/// <summary>
	/// Zakladna trieda, ktora implementuje DataAccessLayer pre pristup a ukladanie dat do SQLite. 
	/// 
	/// Moze byt naimplementovana ako partial class, kde jednotlive "casti" mozu obsahovat specificke 
	/// metody tykajuce sa danej oblasti (napr. Airplanes, Strips, atd..) 
	/// </summary>
	public class DataAccessLayer : IAirplaneProvider
	{
		private const string DATABASE_FILE = @"db.sqlite";

		private SQLiteConnectionWithLock connection;
		private static DataAccessLayer instance = null;

		private DataAccessLayer() { }

		public static DataAccessLayer getInstance()
		{
			if(instance == null)
			{
				instance = new DataAccessLayer();
			}

			return instance;
		}

		/// <summary>
		/// Otevre pripojeni k databazi  try catch bloky u databazovych operaci (database locked), read write lock k metodam,
		/// 
		/// </summary>
		/// <returns></returns>
		private SQLiteConnectionWithLock getConnection()
		{
			if(this.connection == null)
			{
				this.connection = new SQLiteConnectionWithLock(new SQLiteConnectionString(DATABASE_FILE, true), SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);
				this.connection.CreateTable<Airplane>();
			}

			return this.connection;
		}

		/// <summary>
		/// Ziska zoznam uplne vsetkych lietadiel z DB. 
		/// Tato metoda je blokujuca, synchronna... ak sa ma volat v BG vlakne, t 
		/// </summary>
		/// <returns></returns>
		public List<Airplane> ListAllAirplanes()
		{
			using(this.getConnection().Lock())
			{
				return this.getConnection().Table<Airplane>().ToList();
			}
		}


		/// <summary>
		/// Dohlada lietadlo podla zadanej registracnej znacky. 
		/// Registracna znacka je case-insensitive, v DB sa uklada vzdy ako UPPER CASE. 
		/// </summary>
		/// <param name="registration"></param>
		/// <returns></returns>
		public Airplane GetAirplane(string registration)
		{
			using(this.getConnection().Lock())
			{
				return this.getConnection().Table<Airplane>().Where(plane => plane.Registration == registration).FirstOrDefault();
			}
		}



		/// <summary>
		/// Vlozi zadanu instanciu do DB.  TODO: je to ok updatovat enbo vkladat?
		/// 
		/// Metoda vrati vlozenu instanciu (napr. ak vlozenim doslo k vyplneniu auto-increment hodnot. 
		/// </summary>
		/// <param name="airplane"></param>
		/// <returns></returns>
		public Airplane Insert(Airplane airplane)
		{
			airplane.Registration = airplane.Registration.ToUpper();

			using(this.getConnection().Lock())
			{
				this.getConnection().InsertOrReplace(airplane);
			}

			return airplane;
		}


		/// <summary>
		/// Zmaze zadane lietadlo z DB (podla registracnej znacky). 
		/// </summary>
		/// <param name="airplane"></param>
		public void Delete(Airplane airplane)
		{
			using(this.getConnection().Lock())
			{
				this.getConnection().Delete<Airplane>(airplane.Registration);
			}
		}
	}
}
