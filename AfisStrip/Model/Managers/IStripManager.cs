﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AfisStrip.ViewModel.Wrappers;

namespace AfisStrip.Model.Managers
{


	/// <summary>
	/// Rozhranie IStripManager existuje hlavne za ucelom toho, aby instancia StripManager-a mohal byt 
	/// vlozena do DependencyInjector-a. 
	/// 
	/// </summary>
	public interface IStripManager
	{
		/// <summary>
		/// Tato metoda zapise udalost "Vzlet". Vykona vsetky potrebne suvisejuce akcie:
		///   * zmeni Status tohoto strip-u
		///   * zalozi novy Flight, ak ho este nema (resp. aj ked nejaky uz ma, tak vzniknutu situaciu skusi co najlepsie vyriesit)
		///	  ... pripadne dalsie potrebne akcie. 
		///   
		/// 
		/// Tato metoda prijima argument time, kedy k vzletu doslo -- umoznuje tak podvrhnut iny cas nez je aktualny. Ak sa chce pouzit 
		/// aktualny cas, do argumentu sa da dat jednoducho DateTime.Now. 
		/// </summary>
		/// <param name="time"></param>
		Task RecordTakeOffStandaloneAsync(StripWrapper stripW, DateTime time);


		Task RecordLandingAsync(StripWrapper stripW, DateTime time);


		Task RecordEnteringATZAsync(StripWrapper stripW, DateTime time);



		Task RecordLeavingATZAsync(StripWrapper stripW, DateTime time);


	}


}
