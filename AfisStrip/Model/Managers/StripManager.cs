﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AfisStrip.Model.DataService.Interface;
using AfisStrip.Model.Entities;
using AfisStrip.ViewModel.Wrappers;
using iTextSharp.text.pdf;

namespace AfisStrip.Model.Managers
{
	

	/// <summary>
	/// StripManager - manager trieda, ktory obsahuje metody, ktore umoznuju manipulaciu
	/// s instanciami StripNG -- ako napr. spravne ohlasovat vzlet, vstup do ATZ, opoustenie ATZ,
	/// zmenu hodnot a pod., a vhodne na tieto udalosti naviazat dalsie suvisejuce akcie, 
	/// ako napr. vytvorenie zaznamu do Smiraku, do Knihy priletov a odletov, a pod. 
	/// 
	/// Instancia tohoto managera je mozne ziskat z Dependency Injector-a. 
	/// 
	/// 
	/// </summary>
	public class StripManager : IStripManager
	{

		private IFlightsDataService _flightDS = null; 



		public StripManager(IFlightsDataService flightDS)
		{
			this._flightDS = flightDS; 

		}










		///  <summary>
		///  Tato metoda zapise udalost "Vzlet". Vykona vsetky potrebne suvisejuce akcie:
		///    * zalozi novy Flight, ak ho este nema (resp. aj ked nejaky uz ma, tak vzniknutu situaciu skusi co najlepsie vyriesit)
		///    * zmeni Status tohoto strip-u 
		///	   * pokusi sa odhadnut spravnu kategoriu letu (StripNG.Category) na zakladne ulohy (PlaneUsage.Objective)
		///    * [ak nase lietadlo] zapise _nejak_ vzlet do Smiraku -- odovzda mu novo-vytvoreny Flight? 
		///    * [ak cudzie lietadlo] pridat zaznam do kniy odletov a priletov  
		/// 	  ... pripadne dalsie potrebne akcie. 
		///      
		///     
		///  Tato metoda prijima argument time, kedy k vzletu doslo -- umoznuje tak podvrhnut iny cas nez je aktualny. Ak sa chce pouzit 
		///  aktualny cas, do argumentu sa da dat jednoducho DateTime.Now. 
		///  </summary>
		/// <param name="stripW"></param>
		/// <param name="time"></param>
		public async Task RecordTakeOffStandaloneAsync(StripWrapper stripW, DateTime time)
		{

			// -- 1a) skontroluj, ci ma Strip uz nejaky Flight? Je ukonceny? Nie je? 
			if (stripW.Strip.CurrentFlight != null && stripW.Strip.CurrentFlight.CurrentState != Flight.Status.FINISHED) {
				// -- pozor!! Strip ma priradeny Flight a je otvoreny!! Preco tuto instanciu este mam??

				throw new InvalidOperationException($"DEBUG:: POZOR!! Pri Take-off event-e bola najdena instancia Flight-u, ktora este nie je ukoncena! Flight: {stripW.Strip.CurrentFlight}");
			}

			
			// -- 1b) Zaloz novy Flight:
			var newFlight = new Flight(stripW.Strip.PlaneAndCrewAndUsage, FlightEventType.TAKE_OFF, time);
			stripW.Strip.CurrentFlight = newFlight;

			await this._flightDS.InsertNewFlightAsync(newFlight); 


			// -- 2a) nastav Status strip-u, vygeneruje spravny event pre let. 
			stripW.Strip.Status = FlightStatus.IN_FLIGHT;

			// -- 2b) nastav Category: 
			//			-- podla Objective: TOW_PLANE + LOCAL_FLIGHT + WINCH_FLIGHT --> LOCAL_FLIGHT, 
			//								OUT_AND_RETURN --> DEPARTURE
			//								ostatne   --> DEPARTURE
			if (stripW.Strip.PlaneAndCrewAndUsage.PlaneUsageTypeEnum == PlaneUsageTypeEnum.TOW_PLANE || stripW.Strip.PlaneAndCrewAndUsage.PlaneUsageTypeEnum == PlaneUsageTypeEnum.LOCAL_FLIGHT || stripW.Strip.PlaneAndCrewAndUsage.PlaneUsageTypeEnum == PlaneUsageTypeEnum.WINCH_FLIGHTS) {
				stripW.Strip.Category = FlightCategory.LOCAL_FLIGHT;
			} else if (stripW.Strip.PlaneAndCrewAndUsage.PlaneUsageTypeEnum == PlaneUsageTypeEnum.OUT_AND_RETURN) {
				stripW.Strip.Category = FlightCategory.DEPARTURE;
			} else {
				// -- bez specifikacie ulohy => 
				stripW.Strip.Category = FlightCategory.DEPARTURE; 
			}
				



			// -- 3) budem generovat zapis do Smiraku? Ako? 
			// TODO: vyriesit, co je treba spravi, aby sa zapisal let (vzlet) do smiraku? Nic? 


			// -- 4) budem generovat zapis do Knihy priletov a odletov? Ako? 
			// TODO: vyriesit, co je treba spravi, aby sa zapisal let (vzlet) do Kniy priletov a odletov? Nic? 




			//throw new NotImplementedException();
		}



		/// <summary>
		/// Tato metoda umoznuje zapisat udalost "Vzlet" pre vetron, ktory startuje v aerovleku so zadanym vlecnym lietadlom. 
		/// Vykona vsetky akcie ako RecordTakeOffStandalone pre vetron a tiez pre vlecnu, a navyse zapise aj ich vzajomne 
		/// previazanie (takze vlecnej zapise info "Vlek OK-xxxx", a vetronovi zapise "Vlecna: OK-xxx"). 
		/// 
		/// Tieto udaje sa nasledne prepisu tiez do Smiraku (napr. "uloha" vlecnej sa napise ako "Vlek OK-xxxx").  
		/// </summary>
		/// <param name="gliderStripWrapper"></param>
		/// <param name="time"></param>
		/// <param name="towPlaneStripWrapper"></param>
		public void RecordTakeOffWithTow(StripWrapper gliderStripWrapper, DateTime time, StripWrapper towPlaneStripWrapper)
		{
			throw new NotImplementedException();
		}


		/// <summary>
		/// Tato metoda by mala sluzit k tomu, aby bolo mozne dodatocne upravit cas vzletu lietadla tak, aby sa tento zmeneny cas 
		/// spravne vypropagoval do vsetkych suvisejucich dokumentov (ako napr. Smirak a Kniha priletov a odletov"). 
		/// 
		/// Typicky priklad je, ze AFISak si uvedomi az neskor, ze mu lietadlo uz vzlietlo, tak mu naklikne "Vzlet", ALE uvedomi 
		/// si svoju chybu a bude chciet opravit cas o par minut dozadu. Pomocou tejto metody by sa tato zmena mala 
		/// spravne vypropagovat na vsetky suvisejuce miesta. 
		/// 
		/// Rovnako tak, ak vzlet prebiehal v aerovleku, tak cas vzletu je treba upravit pre vetron aj pre vlecnu! 
		/// </summary>
		/// <param name="stripWrapper"></param>
		public void EditTakeOffTime(StripWrapper stripWrapper)
		{
			throw new NotImplementedException();
		}






		/// <summary>
		/// Tato metoda zapise udalost "Pristatia" a vykona vsetky potrebne suvisejuce akcie:
		///		* zmeni stav tohoto stripu: Status na ON_GROUND
		///		* zapise do aktualneho Flight-u cas pristania a Flight ukonci. 
		///		* na nastavenie Category nesaha (predpokladam, ze ak to bol ARRIVAL, alebo LOCAL_FLIGHT, tak sa kategoria pristanim nezmeni. 
		///		
		/// 
		/// Argument time umoznuje presne urcit, kedy k pristatiu doslo. Ak k nemu doslo "teraz", treba pouzit DateTime.Now. 
		/// </summary>
		/// <param name="time"></param>
		public async Task RecordLandingAsync(StripWrapper stripW, DateTime time)
		{
			var flight = stripW.Strip.CurrentFlight; 
			if (flight == null) {
				throw new InvalidOperationException($"Strip nemá asociovaný let, nelze jej ukončit přistáním.");
			}

			flight.CloseFlight(FlightEventType.LANDING, time);
			await this._flightDS.UpdateFlightAsync(flight);

			stripW.Strip.CurrentFlight = null;
			stripW.Strip.Status = FlightStatus.ON_GROUND; 
			

			//throw new NotImplementedException();
		}





		/// <summary>
		/// Tato metoda by mala sluzit k tomu, aby bolo mozne dodatocne upravit cas pristatia lietadla tak, aby sa tento zmeneny cas 
		/// spravne vypropagoval do vsetkych suvisejucich dokumentov (ako napr. Smirak a "Kniha priletov a odletov"). 
		/// 
		/// Typicky priklad je, ze AFISak si az po chvilne vsimne, ze mu nejake lietadlo uz pristalo, takze mu naklikne "Pristati", 
		/// ALE uvedomi si svoju chybu a bude chciet opravit cas o par minut dozadu. Pomocou tejto metody by sa tato zmena mala 
		/// spravne vypropagovat na vsetky suvisejuce miesta. 
		/// </summary>
		/// <param name="stripWrapper"></param>
		public void EditLandingTime(StripWrapper stripWrapper)
		{
			throw new NotImplementedException();
		}





		/// <summary>
		/// Tato metoda zapise udalost "Vstupil do ATZ" a vykona vsetky potrebne suvisejuce akcie:
		///		* skontroluje, ci existuje aktivny let, a) ak nie, vytvori novy, b) ak ano, zaloguje vhodny event.  
		///		* zapise udalost do aktualneho Flight-u, pripadne zalozi novy Flight, ktoreho zaciatocna udalost sa nastavi na "ENTERING_ATZ" 
		///		* zmeni stav tohoto stripu na "IN_FLIGHT" (do ATZ sa neda vstupit "po zemi" :-) )
		///		* pokusi sa doplnit kategoriu podla dodatocnych informacii ako napr. Objective, alebo ExitPoint 
		/// 
		/// Argument time umoznuje presne urcit, kedy k pristatiu doslo. Ak k nemu doslo "teraz", treba pouzit DateTime.Now. 
		/// </summary>
		/// <param name="time"></param>
		public async Task RecordEnteringATZAsync(StripWrapper stripW, DateTime time)
		{

			if (stripW.Strip.CurrentFlight == null) {
				// -- je to uplne novy strip => vytvorim novy flight, ktory zacina udalostou ENTERING_ATZ. 
				
				var newFlight = new Flight(stripW.Strip.PlaneAndCrewAndUsage, FlightEventType.ENTERING_ATZ, time);
				stripW.Strip.CurrentFlight = newFlight;
				await this._flightDS.InsertNewFlightAsync(newFlight); 

			} else {
				// takze tento strip UZ MA instanciu FLight-u. Vysetrim, aka varianta nastava: 
				var oldFlight = stripW.Strip.CurrentFlight; 

				switch (oldFlight.CurrentState) {
					case Flight.Status.NON_ACTIVE:
						// predosly let este nezacal, takze ho "zacnem" pomocou eventu ENTERING_ATZ: 

						oldFlight.OpenFlight(FlightEventType.ENTERING_ATZ, time);
						await this._flightDS.UpdateFlightAsync(oldFlight);

					    break;

					case Flight.Status.ACTIVE:
						// predosly let je aktivny, takze pridam novu udalost: 

						oldFlight.LogEvent(FlightEventType.ENTERING_ATZ, time, stripW.Strip.EntryPoint, stripW.Strip.Notes);
						await this._flightDS.UpdateFlightAsync(oldFlight);

						break;

					case Flight.Status.FINISHED:
						// -- predosly let je uz ukonceny, takze vytvorim uplne novu instanciu: 

						var newFlight = new Flight(stripW.Strip.PlaneAndCrewAndUsage, FlightEventType.ENTERING_ATZ, time);
						stripW.Strip.CurrentFlight = newFlight;
						await this._flightDS.InsertNewFlightAsync(newFlight);

						break;

				}

			}


			stripW.Strip.Status = FlightStatus.IN_FLIGHT;


			// -- nastav Category: 
			if (stripW.Strip.PlaneAndCrewAndUsage.PlaneUsageTypeEnum == PlaneUsageTypeEnum.TOW_PLANE || stripW.Strip.PlaneAndCrewAndUsage.PlaneUsageTypeEnum == PlaneUsageTypeEnum.LOCAL_FLIGHT || stripW.Strip.PlaneAndCrewAndUsage.PlaneUsageTypeEnum == PlaneUsageTypeEnum.WINCH_FLIGHTS) {
				stripW.Strip.Category = FlightCategory.LOCAL_FLIGHT;
			} else if (stripW.Strip.PlaneAndCrewAndUsage.PlaneUsageTypeEnum == PlaneUsageTypeEnum.OUT_AND_RETURN) {
				stripW.Strip.Category = FlightCategory.ARRIVAL;
			} else {
				// -- bez specifikacie ulohy => pozriem sa, ci ciel je "lkcm" - ak ano, nastav ARRIVAL, inak TRANSIT

				if (stripW.Strip.ExitPoint?.Trim().ToUpper() == "LKCM") {
					stripW.Strip.Category = FlightCategory.ARRIVAL;
				} else {
					stripW.Strip.Category = FlightCategory.TRANSIT;
				}
				
			}



			//throw new NotImplementedException();
		}





		/// <summary>
		/// Tato metoda zapise udalost "Opustil ATZ" a vykona vsetky potrebne suvisejuce akcie:
		///		* zmeni kategoriu tohoto stripu na "OUT_OF_ATZ"
		///		* zapise udalost do aktualneho Flight-u
		///		* rozhodne sa, ci aktualny Flight touto udalostou ukonci (napr. ak sa jedna o cudzie lietadlo), pripadne 
		///		  ci ponecha let aktivnym (pretoze sa napr. jedna o lietadlo vlastneho AK, ktore je potreba zapisat do smiraku, takze je treba pockat az po udalost LANDING). 
		/// 
		/// 
		/// Argument time umoznuje presne urcit, kedy k pristatiu doslo. Ak k nemu doslo "teraz", treba pouzit DateTime.Now. 
		/// </summary>
		/// <param name="time"></param>
		public async Task RecordLeavingATZAsync(StripWrapper stripW, DateTime time)
		{
			// -- 1) ziskaj aktualny let:
			var flight = stripW.Strip.CurrentFlight; 

			if (flight == null || flight.CurrentState != Flight.Status.ACTIVE) {
				throw new InvalidOperationException("Strip nemá asociovaný let, nelze jej ukončit opuštením ATZ.");
			}


			// -- 2) zmeni Category tohoto strip-u: 
			stripW.Strip.Category = FlightCategory.OUT_OF_ATZ;




			// -- 3) zapise udalost LEAVING_ATZ: 
			//
			// -- mam aktualny let ukoncit? Alebo ponechat otvoreny a len zalogovat novu udalost?
			//	  * ak to NIE JE nase lietadlo, let uzavriem:  

			if (stripW.Strip.PlaneAndCrewAndUsage?.Plane?.IsHomebasedAtThisAirport ?? false) {
				flight.LogEvent(FlightEventType.LEAVING_ATZ, time, stripW.Strip.Position + " | " + stripW.Strip.ExitPoint, stripW.Strip.Notes);
			} else {
				flight.CloseFlight(FlightEventType.LEAVING_ATZ, time);
				stripW.Strip.CurrentFlight = null; 
			}

			// -- na zaver uloz zmeny v instancii Flight: 
			await this._flightDS.UpdateFlightAsync(flight);

			

			//throw new NotImplementedException();
		}







	}
}
