﻿namespace AfisStrip.Model.Enums
{
	/// <summary>
	/// Enum ktery vyjadruje stav stripu - tj. zda je vse ok, zda je potreba kontrola
	/// nebo zda zacina udobi nejistoty (INCERFA)
	/// </summary>
	public enum StripCheckStatusEnum
	{
		UNKNOWN,
		OK,
		SAFETY_CHECK,
		INCERFA
	}
}
