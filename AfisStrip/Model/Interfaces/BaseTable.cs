﻿using System;
using AfisStrip.Libs.ObservableObjects;
using System.ComponentModel;
using System.Text;

namespace AfisStrip.Model.Interfaces
{
	[Obsolete("R: 14.2.2018 -- Tuto triedu povazujem k dnesnemu dnu za staru a nespravnu -- mala by byt vymazana. Perzistencia do SQLite by mala byt riesena v DAL-e")]
	public abstract class BaseTable : ObservableObject, IDataErrorInfo
	{

		public string Error
		{
			get
			{
				StringBuilder error = new StringBuilder();

				// iterate over all of the properties
				// of this object - aggregating any validation errors
				PropertyDescriptorCollection props = TypeDescriptor.GetProperties(this);
				foreach(PropertyDescriptor prop in props)
				{
					string propertyError = this[prop.Name];
					if(propertyError != string.Empty)
					{
						error.Append((error.Length != 0 ? ", " : "") + propertyError);
					}
				}

				return error.ToString();
			}
		}

		public virtual string this[string columnName]
		{
			get { return ""; }
		}
	}
}
