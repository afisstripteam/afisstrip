﻿using System;
using System.Collections.Generic;
using AfisStrip.Libs;

namespace AfisStrip.Model.Interfaces
{

	[Obsolete("R: 14.2.2018 -- Tuto triedu povazujem k dnesnemu dnu za staru a nespravnu -- mala by byt vymazana. Perzistencia do SQLite by mala byt riesena v DAL-e, cez klasicke connection do SQLite.")]
	public abstract class BaseRepository<T> where T : new()
	{
		protected SQLiteConnection connection;

		public BaseRepository(SQLiteConnection connection)
		{
			this.connection = connection;
		}


		public BaseRepository()
		{
		}

		public List<T> GetAll()
		{
			if(this.connection == null)
			{
				return null;
			}

			return new List<T>(this.connection.Table<T>());
		}

		abstract public T GetById(int ID);

		public int Update(T data)
		{
			if(this.connection == null)
			{
				return 0;
			}

			return this.connection.Update(data);
		}

		public int Insert(T data)
		{
			if(this.connection == null)
			{
				return 0;
			}

			return this.connection.Insert(data);
		}

		public int Delete(T data)
		{
			if(this.connection == null)
			{
				return 0;
			}

			return this.connection.Delete(data);
		}
	}
}
