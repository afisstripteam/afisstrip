﻿using System;
using AfisStrip.Model.Interfaces;
using AfisStrip.Model.Tables;
using System.Collections.Generic;
using System.Linq;

namespace AfisStrip.Model.Repositories
{

	[Obsolete("R: 14.2.2018 -- Tuto triedu povazujem k dnesnemu dnu za staru a nespravnu -- mala by byt vymazana. Perzistencia do SQLite by mala byt riesena v DAL-e")]
	public class FlightBookRepository : BaseRepository<FlightBook>
	{
		public FlightBookRepository()
		{ }

		public override FlightBook GetById(int ID)
		{
			return this.connection.Table<FlightBook>().Where(v => v.Id == ID).ToList()[0];
		}

		public List<FlightBook> GetByCallSign(string callSign)
		{
			if(callSign == null)
				return null;
			return connection.Table<FlightBook>().Where(v => v.CallSign == callSign).ToList();
		}
	}
}
