﻿using System;
using AfisStrip.Model.Interfaces;
using AfisStrip.Model.Tables;
using System.Linq;

namespace AfisStrip.Model.Repositories
{

	[Obsolete("R: 14.2.2018 -- Tuto triedu povazujem k dnesnemu dnu za staru a nespravnu -- mala by byt vymazana. Perzistencia do SQLite by mala byt riesena v DAL-e")]
	public class TimekeeperBlockRepository : BaseRepository<TimekeeperBlock>
	{
		public TimekeeperBlockRepository()
		{ }

		public override TimekeeperBlock GetById(int ID)
		{
			return connection.Table<TimekeeperBlock>().Where(v => v.Id == ID).ToList()[0];
		}

		public TimekeeperBlock GetFlyingByCallSign(string callSign)
		{
			if(callSign == null)
				return null;
			return connection.Table<TimekeeperBlock>().Where(v => v.CallSign == callSign && v.TakeOff >= v.Landing).ToList()[0];
		}
	}
}
