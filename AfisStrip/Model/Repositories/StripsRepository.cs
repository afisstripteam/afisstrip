﻿using System;
using AfisStrip.Model.Interfaces;
using AfisStrip.Model.Tables;
using System.Collections.Generic;
using System.Linq;

namespace AfisStrip.Model.Repositories
{

	[Obsolete("Trieda StripsRepository je stara a nespravna -- bude vymazana. Perzistencia do SQLite riesena v DAL-e")]
	public class StripsRepository : BaseRepository<Strip>
	{
		public StripsRepository()
		{ }

		public override Strip GetById(int ID)
		{
			return connection.Table<Strip>().Where(v => v.Id == ID).ToList()[0];
		}

		public List<Strip> SearchByCallSign(string callSign)
		{
			if(callSign == null)
				return null;
			return connection.Table<Strip>().Where(v => v.CallSign == callSign).ToList();
		}
	}
}
