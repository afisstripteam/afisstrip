﻿using AfisStrip.Libs.ObservableObjects;
using AfisStrip.Model.Entities;
using AfisStrip.Model.Interfaces;
using System;
using System.Windows.Input;
using AfisStrip.Libs;
using AfisStrip.Model.DataService;

namespace AfisStrip.Model.Tables
{

	[Obsolete("Trieda Strip je stara, nespravna, a bude zmazana -> miesto nej pouzivat StripNG!")] 
	public class Strip : BaseTable
	{
		private string _CallSign;
		private string _Type;
		private string _Captain;
		private string _Position;
		private string _Notes;
		private DateTime _LastContact;
		private string _Category;
		private string _Status;
		private int? _Order;
		private string _towedBy;
		private string _towes;

		[PrimaryKey, AutoIncrement]
		public int Id
		{
			get;
			set;
		}

		public string CallSign
		{
			get
			{
				return _CallSign;
			}
			set
			{
				var oldVal = _CallSign;
				_CallSign = value;

				if(oldVal != null)
					OnPropertyChanged(nameof(CallSign));
			}
		}

		public string Type
		{
			get
			{
				return _Type;
			}
			set
			{
				var oldVal = _Type;
				_Type = value;

				if(oldVal != null)
					OnPropertyChanged(nameof(Type));
			}
		}

		public string Captain
		{
			get
			{
				return _Captain;
			}
			set
			{
				var oldVal = _Captain;
				_Captain = value;

				if(oldVal != null)
					OnPropertyChanged(nameof(Captain));
			}
		}

		public string Position
		{
			get
			{
				return _Position;
			}
			set
			{
				var oldVal = _Position;
				_Position = value;

				if(oldVal != null)
					OnPropertyChanged(nameof(Position));
			}
		}

		public string Notes
		{
			get
			{
				return _Notes;
			}
			set
			{
				var oldVal = _Notes;
				_Notes = value;

				if(oldVal != null)
					OnPropertyChanged(nameof(Notes));
			}
		}

		public DateTime LastContact
		{
			get
			{
				return _LastContact;
			}
			set
			{
				var oldVal = _LastContact;
				_LastContact = value;

				if(oldVal != null)
					OnPropertyChanged(nameof(LastContact));
			}
		}

		public string Category
		{
			get
			{
				return _Category;
			}
			set
			{
				var oldVal = _Category;
				_Category = value;

				if(oldVal != null)
					OnPropertyChanged(nameof(Category));
			}
		}

		public string Status
		{
			get
			{
				return _Status;
			}
			set
			{
				var oldVal = _Status;
				_Status = value;

				if(oldVal != null)
				{
					OnPropertyChanged(nameof(Status));
					OnPropertyChanged(nameof(IsFlying));
					OnPropertyChanged(nameof(IsLanded));
				}
			}
		}

		public int? Order
		{
			get
			{
				return _Order;
			}
			set
			{
				var oldVal = _Order;
				_Order = value;

				if(oldVal != null)
					OnPropertyChanged(nameof(Order));
			}
		}


		/// <summary>
		/// IsHomebasedAtThisAirport
		/// TODO: Premenovat! na nieco presnejsie -- nezaujima nas vlastnik, ale ci je "nase" (kludne vlastnikom moze byt sukromnik alebo navsteva, pointa je, ci nas "zaujima" v zmysle ze ocakavame jeho navrat. 
		/// </summary>
		public bool IsHomebasedAtThisAirport
		{
			get
			{
				if(AirplanesDataService.getInstance().GetAirplaneAsync(this.CallSign) == null)
					return false;

				return true;
			}
		}

		/// <summary>
		/// TODO: presunut do StripWrapper-a? Asi to nema nic co do cinenie so samotnym Stripom (= zaznamom o lete pre riadiaceho). ✓
		/// </summary>
		public string TowedBy
		{
			get
			{
				return _towedBy;
			}
			set
			{
				var oldVal = _towedBy;
				_towedBy = value;

				if(oldVal != null)
					OnPropertyChanged(nameof(TowedBy));
			}
		}


		/// <summary>
		/// TODO: Co to je za hodnotu?! :-O To je hodnota koho tahne vlecna =D
		/// </summary>
		public string Towes
		{
			get
			{
				return _towes;
			}
			set
			{
				var oldVal = _towes;
				_towes = value;

				if(oldVal != null)
					OnPropertyChanged(nameof(Towes));
			}
		}

		/// <summary>
		/// TODO: presunut do StripWrapper-a a PREMENOVAT! "IsTowPlane"? ✓
		/// </summary>
		[Ignore]
		public bool isTow
		{
			get
			{
				return false; // TODO Vyresit towsdataservice
				//return TowsDataService.GetInstance.Tows.Where(x => x.CallSign == this.CallSign).Any();
			}
		}


		/// <summary>
		/// TODO: Zmazat! Nepotrebne! 
		/// </summary>
		[Ignore]
		public bool isNotTow
		{
			get
			{
				return !this.isTow;
			}
		}


		/// <summary>
		/// TODO: presunut do StripWrapper-a ✓
		/// </summary>
		[Ignore]
		public bool isTowed
		{
			get
			{
				return !string.IsNullOrEmpty(this.TowedBy);
			}
		}


		/// <summary>
		/// Indikuje jestli se jedna o letadlo zapsane do provozu
		/// </summary>
		[Ignore]
		public bool IsPlaneToTraffic
		{
			get
			{
				//return PlanesToTrafficDataService.GetInstance.Planes.Where(x => x.CallSign == this.CallSign).Any() || this.isTow;
				return this.isTow; //TODO: Predelat na konverter
			}
		}

		[Ignore]
		public bool IsFlying
		{
			get
			{
				if(this.Status != null && this.Status.ToLower().Contains("letí"))
				{
					return true;
				}

				return false;
			}
		}


		/// <summary>
		/// TODO: zmazat (duplikatne s !IsFlying)
		/// </summary>
		[Ignore]
		public bool IsLanded
		{
			get
			{
				return !this.IsFlying;
			}
		}

		/// <summary>
		/// TODO: Presunut do StripWrapper-u! 
		/// </summary>
		[Ignore]
		public bool EditMode { get; set; }


		/// <summary>
		/// TODO: Presunut do StripWrapper-u!  
		/// </summary>
		[Ignore]
		public bool ViewMode
		{
			get
			{
				return !this.EditMode;
			}
		}

		/// <summary>
		/// Letadlo ktere je spojene se stripem TODO: Presunut do StripWrapper-u! 
		/// </summary>
		[Ignore]
		public Airplane LinkedPlane { get; set; }


		/// <summary>
		/// TODO: Roman (7.4.2015) :: WTF?! Co robi v entite Command?! Commandy su zalezitostou ViewModel vrstvy! Presunut do StripWrapper-a! 
		/// </summary>
		[Ignore]
		public ICommand ChangeEditMode
		{
			get
			{
				return new RelayCommand((parameter) =>
				{
					this.EditMode = this.EditMode ? false : true;
					OnPropertyChanged(nameof(EditMode));
					OnPropertyChanged(nameof(ViewMode));
				});
			}
		}
		

		public Strip()
		{
			this.EditMode = false;
		}


		/// <summary>
		/// TODO: Roman (7.4.2015) :: WTF?! Co robi v entite Command?! Commandy su zalezitostou ViewModel vrstvy! Presunut do StripWrapper-a! 
		///						Navyse Entita nesmie sahat na GUI -- tu je pouzity MessageBox! 
		/// </summary>
		/// <param name="action"></param>
		/// <param name="objective"></param>
		private void FlightBookCommand(string action, string objective = null)
		{
			//var stripsCollection = DataService.StripsDataService.GetInstance.InATZ; TODO:
			//var towControl = DataService.StripsDataService.GetInstance.InATZ.Where(x => x.CallSign == this.TowedBy).FirstOrDefault(); TODO:
			//Strip towControl = new Strip(); //TODO: Po upraveni stripu odstranit

			//if(this.IsPlaneToTraffic || this.IsTowed /*|| TowsDataService.GetInstance.Tows.Where(x => x.CallSign == this.CallSign).Any()*/) //TODO: TowsDataService vyresit
			//{
			//	if(this.Category == StripsDataService.GetInstance.category["Odlet"])
			//	{
			//		var timekeeperBlockRecord = new TimekeeperBlock()
			//		{
			//			CallSign = this.CallSign,
			//			Type = this.Type,
			//			TakeOff = DateTime.Now,
			//			Crew = this.LinkedPlane.Owner
			//		};

			//		if(this.isTow && !String.IsNullOrEmpty(this.Towes))
			//		{
			//			timekeeperBlockRecord.Objective = "Vlek " + this.LinkedPlane.Registration;
			//		}

			//		//TimekeeperBlockDataService.GetInstance.TimekeeperBlock.Add(timekeeperBlockRecord); TODO: do viewmodelu

			//		if(towControl != null && this.IsTowed)
			//		{
			//			//var tow = TowsDataService.GetInstance.GetByCallSign(towControl.CallSign); //TODO

			//			if(towControl.Status == StripsDataService.GetInstance.status["Letí"])
			//			{
			//				MessageBox.Show("Vlečná je ještě ve vzduchu!");
			//				return;
			//			}

			//			towControl.LastContact = DateTime.Now;
			//			towControl.Towes = this.CallSign;
			//			towControl.Departured();

			//			OnPropertyChanged(nameof(CurrentTime));
			//		}

			//	} else if(this.Category == StripsDataService.GetInstance.category["Na zemi"])
			//	{
			//		//var record = TimekeeperBlockDataService.GetInstance.GetFlyingByCallSign(this.CallSign); TODO: DO Viewmodelu

			//		//if(record != null)
			//		//{
			//		//	record.Landing = DateTime.Now;
			//		//}
			//	}
			//} else
			//{
			//	//TODO: Toto presunou jinam, napr do strip data service
			//	//FlightBookDataService.GetInstance.FlightBook.Add(new FlightBook
			//	//{
			//	//	Action = action,
			//	//	CallSign = this.CallSign,
			//	//	Position = this.Position,
			//	//	Notes = this.Notes,
			//	//	Time = DateTime.Now
			//	//});
			//}
		}
	}
}
