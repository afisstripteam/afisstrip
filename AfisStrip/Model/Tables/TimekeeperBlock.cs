﻿using AfisStrip.Libs.Misc;
using AfisStrip.Model.Interfaces;
using System;
using AfisStrip.Libs;

namespace AfisStrip.Model.Tables
{
    public class TimekeeperBlock : BaseTable
    {
        private string _CallSign;
        private string _Type;
        private string _Crew;
        private string _Objective;
        private DateTime _TakeOff;
        private DateTime _Landing;

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string CallSign
        {
            get
            {
                return _CallSign;
            }
            set
            {
                _CallSign = value;
                OnPropertyChanged(nameof(CallSign));
            }
        }

        public string Type
        {
            get
            {
                return _Type;
            }
            set
            {
                _Type = value;
                OnPropertyChanged(nameof(Type));
            }
        }

        public string Crew
        {
            get
            {
                return _Crew;
            }
            set
            {
                _Crew = value;
                OnPropertyChanged(nameof(Crew));
            }
        }

        public string Objective
        {
            get
            {
                return _Objective;
            }
            set
            {
                _Objective = value;
                OnPropertyChanged(nameof(Objective));
            }
        }

        public DateTime TakeOff
        {
            get
            {
                return _TakeOff;
            }
            set
            {
                _TakeOff = TimeHelper.roundSeconds(value);
                OnPropertyChanged(nameof(TakeOff));
            }
        }

        public DateTime Landing
        {
            get
            {
                return _Landing;
            }
            set
            {
                _Landing = TimeHelper.roundSeconds(value);
                OnPropertyChanged(nameof(Landing));
            }
        }

        public TimekeeperBlock()
        {
            PropertyChanged += update;
        }

        private void update(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            //DBProvider.GetInstance.TimekeeperBlockRepository.Update(this); TODO: do viewmodelu
        }
    }
}
