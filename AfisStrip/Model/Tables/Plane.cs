﻿using System;
using AfisStrip.Libs;
using AfisStrip.Model.Interfaces;

namespace AfisStrip.Model.Tables
{
	[Obsolete("Tato mapovacia trieda pre ORM je stara -- treba ju vyhodit uplne.")]
    public class Plane : BaseTable
    {
        private string _CallSign;
        private string _Type;
        private string _Captain;
        private bool _PropertyOfAeroclub;
        private string _Objective;

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string CallSign
        {
            get
            {
                return this._CallSign;
            }
            set
            {
                if(this._CallSign != value)
                {
                    this._CallSign = value;
                    OnPropertyChanged(nameof(CallSign));
                }
            }
        }

        public string Type
        {
            get
            {
                return this._Type;
            }
            set
            {
                if(this._Type != value)
                {
                    this._Type = value;
                    OnPropertyChanged(nameof(Type));
                }
            }
        }

        public string Captain
        {
            get
            {
                return this._Captain;
            }
            set
            {
                if(this._Captain != value)
                {
                    this._Captain = value;
                    OnPropertyChanged(nameof(Captain));
                }
            }
        }

        public bool PropertyOfAeroclub
        {
            get
            {
                return this._PropertyOfAeroclub;
            }
            set
            {
                if(this._PropertyOfAeroclub != value)
                {
                    this._PropertyOfAeroclub = value;
                    OnPropertyChanged(nameof(PropertyOfAeroclub));
                }
            }
        }

        public string Objective
        {
            get
            {
                return this._Objective;
            }
            set
            {
                if(this._Objective != value)
                {
                    this._Objective = value;
                    OnPropertyChanged(nameof(Objective));
                }
            }
        }

        public void MergeFrom(Plane otherPlane)
        {
            if(otherPlane == null)
                return;

            if(otherPlane.CallSign != null)
            {
                this.CallSign = otherPlane.CallSign;
            }

            if(otherPlane.Captain != null)
            {
                this.Captain = otherPlane.Captain;
            }

            if(otherPlane.Objective != null)
            {
                this.Objective = otherPlane.Objective;
            }

            if(otherPlane.PropertyOfAeroclub)
            {
                this.PropertyOfAeroclub = otherPlane.PropertyOfAeroclub;
            }

            if(otherPlane.Type != null)
            {
                this.Type = otherPlane.Type;
            }
        }
    }
}
