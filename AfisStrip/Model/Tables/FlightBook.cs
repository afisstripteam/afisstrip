﻿using AfisStrip.Model.Interfaces;
using System;
using AfisStrip.Libs;

namespace AfisStrip.Model.Tables
{
    public class FlightBook : BaseTable
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string CallSign { get; set; }
        public string Action { get; set; }
        public DateTime Time { get; set; }
        public string Position { get; set; }
        public string Notes { get; set; }
    }
}
