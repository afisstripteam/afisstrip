﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AfisStrip.Model.DataService.Interface;
using AfisStrip.Model.Entities;

namespace AfisStrip.Model.DataService
{

	/// <summary>
	/// Implementacia PlaneUsageRecordDataService-u, ktory si zaznamy uchovava len u seba v pamati 
	/// (v lokalnej kolekcii zaznamov). 
	/// 
	/// Aplikacia AfisStrip-u je zodpovedna za to, ze bude svoj "workspace" pravidelne ukladat na disk pre 
	/// pripad obnovy po pade. Tiez by mohla kolekciu "letadel do provozu" priebezne ukladat samostatne, aby 
	/// sa tato mala kolekcia napr. dala obnovit, ked zapnem AfisStrip druhykrat pocas toho isteho dna. [tato feature je k diskusii, 15.3.2018].
	/// 
	/// </summary>
	public class PlaneUsageRecordsDataService : IPlaneUsageRecordsDataService
	{

		/// <summary>
		/// docasna kolekcia -- "databaza" -- zaznamov.  
		/// Akonahle bude naimplementovany DAL, tak sa data budu prenasat a nacitavat z/do DAL-u (a do databaze). 
		/// </summary>
		private readonly List<PlaneUsageRecord> items;


		/// <summary>
		/// Konstruktor. 
		/// </summary>
		public PlaneUsageRecordsDataService()
		{
			this.items = new List<PlaneUsageRecord>();
		}


		/// <summary>
		/// Vrati zoznam vsetkych aktualne znamych zaznamov o "lietadlach do provozu". 
		/// 
		/// Tato metoda je asynchronna, hoci jej beh bude typicky velmi rychly (vsetky data si uklada len lokalne v pamati). 
		/// Asynchronicita je pouzita za ucelom podpory "dobrych mravov", aby sa zvykalo na to, ze metody DataService-ov su asynchronne. 
		/// </summary>
		/// <returns></returns>
		public Task<IEnumerable<PlaneUsageRecord>> ListAllPlanesUsagesAsync()
		{
			throw new System.NotImplementedException();
		}

		
		public Task<PlaneUsageRecord> GetUsageForAirplaneAsync(string registration)
		{
			throw new System.NotImplementedException();
		}

		
		/// <summary>
		/// Prida novy zaznam PlaneUsageRecord-u do uloziska. 
		/// </summary>
		/// <param name="planeUsageRecord"></param>
		/// <returns></returns>
		public Task AddNewPlaneUsageRecordAsync(PlaneUsageRecord planeUsageRecord)
		{
			throw new System.NotImplementedException();
		}


		public Task RemovePlaneUsageRecordAsync(PlaneUsageRecord planeUsageRecord)
		{
			throw new System.NotImplementedException();
		}
	}
}