﻿using System.Collections.ObjectModel;
using System.Linq;
using AfisStrip.Model.DataService.Interface;
using AfisStrip.Model.Tables;

namespace AfisStrip.Model.DataService
{
	public class TowsDataService : ITowsDataService
	{
		private readonly ObservableCollection<PlaneTow> _items;

		public TowsDataService()
		{
			this._items = new ObservableCollection<PlaneTow>();
		}

		public ObservableCollection<PlaneTow> Tows
		{
			get
			{
				return this._items;
			}
		}

		public PlaneTow GetByCallSign(string callSign)
		{
			var query = this.Tows.Where(v => v.CallSign == callSign).ToList();

			return query.FirstOrDefault();
		}
	}
}
