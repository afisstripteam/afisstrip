﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AfisStrip.Model.DataService.Interface;
using AfisStrip.Model.Entities;

namespace AfisStrip.Model.DataService
{

	/// <summary>
	/// Tento data service poskytuje sluzby pre manipulaciu s letmi. 
	/// 
	/// Umoznuje ich pridavat, ukladat upravene lety, ziskavat zoznam letov podla specifikovaneho filtra a podobne. 
	/// 
	/// 
	/// </summary>
	public class FlightsDataService: IFlightsDataService 
	{

		public enum FlightTimeMode
		{
			BEGIN_OR_END,
			BEGIN,
			END
		}





		/// <summary>
		/// 15.2.2018 - len docasna kolekcia, kam si budem moct pocas debug-behov programu ukladat nove instancie Flight-u. 
		/// </summary>
		private List<Flight> _mockFlightsRepository = new List<Flight>();






		/// <summary>
		/// Vlozi novy zaznam Flight-u do databaze a priradi instancii Flight SqlId. ID-cko zmeni (vpise) priamo 
		/// v zadanej instancii flight. 
		/// 
		/// 
		/// </summary>
		/// <param name="flight"></param>
		/// <returns></returns>
		public async Task InsertNewFlightAsync(Flight flight)
		{
			//throw new NotImplementedException();

			// TODO: odstranit ukladanie do MOCK lokalnej kolekcie, a pridat ukladanie do DB. 
			this._mockFlightsRepository.Add(flight);

		}




		/// <summary>
		/// Ulozi zmeny v zadanej instancii flight. Predpoklada sa, ze instancia je uz ulozena! 
		/// 
		/// Zatial (k 15.2.2018) tato metoda NEMA (zamerne) schopnost InsertorUpdate -- je to potreba? Zatial si myslim ze nie je, preto to nepridavam. 
		/// </summary>
		/// <param name="flight"></param>
		/// <returns></returns>
		public async Task UpdateFlightAsync(Flight flight)
		{

			// TODO: pridat ukladanie do DB. Pre ukladanie instancii do MOCK DB nie je potrebna ziadna akcia, kedze kolekcia si pamata referencie na dane instancie, takze zmeny ktore niekto iny zapisal do instancie Flight-u su uz davno zapisane aj "v kolekcii". 
			

		}






		/// <summary>
		/// Ziska instanciu Flight podla zadaneho ID-cka. 
		/// POZOR!! Vzdy ziska CISTU novu instanciu z databaze! 
		/// 
		/// </summary>
		/// <param name="sqlId"></param>
		/// <returns></returns>
		public Flight GetFlightById(int sqlId)
		{
			throw new NotImplementedException();
			return null; 
		}


		/// <summary>
		/// Vrati zoznam letov, ktore ZACALI alebo SKONCILI v casovom useku stanovenom parametrami fromTime a toTime. 
		/// Ci sa berie cas zaciatku, konca, alebo ktorykolvek z nich sa udava parametrom mode. 
		/// 
		/// 
		/// </summary>
		/// <param name="fromTime"></param>
		/// <param name="toTime"></param>
		/// <param name="mode"></param>
		/// <returns></returns>
		public List<Flight> ListFlightsForTimeSpan(DateTime fromTime, DateTime toTime, FlightTimeMode mode = FlightTimeMode.BEGIN_OR_END)
		{
			throw new NotImplementedException();
			return null;
		}


		






	}

}
