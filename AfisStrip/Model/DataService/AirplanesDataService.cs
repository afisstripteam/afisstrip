﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AfisStrip.Model.DataAccessLayer;
using AfisStrip.Model.Entities;

namespace AfisStrip.Model.DataService
{


	/// <summary>
	/// Tato trieda implmentuje DataService, ktory poskytuje data o lietadlach pre ViewModel-y. 
	/// 
	/// Tato trieda je "vysoko-urovnovym" managerom, ktory napr.:
	///		- implementuje cachovanie vyslekov 
	///		- ak nema vysledok nacachovany, tak dohladava vysledky v lokalnej DB ako aj u "Externych poskytovatelov" 
	///		- DataService uz moze obsahovat optimalizacie pre GUI:
	///			- samotnu pracu vykonava na pozadi (aby volajuceho neblokovala) - Async
	///			- poskytuje Async metody (takze volajuci moze await-nut a ziskat tak vysledok). 
	///			 ... praca s ObservableCollection sa nedoporucuje, pretoze to je zalezitost View/ViewModelu, ako si to ukladaju... Navyse k ObservableCollection je nutne pristupovat z UI vlakna, takze to popiera pracu DataService-u na pozadi. 
	///  
	/// DataServis by sa mal tiez snazit "rozdavat" rovnake instancie pre rovnake entity... takze napr. ked 5 roznych miest poziada o GetAirplane("OK-ABC"), 
	/// vrati sa 5x ta ista instancia! K tomu tiez sluzi cachovanie... rozhodne by nemal instancie duplikovat (napr. kvoli tomu, ze vzdy vola DAL, ktory 
	/// sa o toto nestara). 
	///	
	/// </summary>
	public class AirplanesDataService
	{
		private static AirplanesDataService instance = null;



		/// <summary>
		/// Zoznam jednotlivych provider-ov, pomocou ktorych sa udaje lietadlach dohladavaju. 
		/// Typicky sa bude pouzivat "DataAccessLayer", ktory najde lokalne ulozene lietadla, a napr. OnlinePlanesFinder, 
		/// ktory dohlada udaje z internetu. 
		/// </summary>
		private readonly List<IAirplaneProvider> dataProviders = new List<IAirplaneProvider>(); 



		/// <summary>
		/// Interna cache pre lietadla. 
		/// </summary>
		private Dictionary<string, Airplane> _cacheAirplanes;


		/// <summary>
		/// Priznak, ci je cache lietadiel kompletne nacitana. Ked je nahodena na TRUE, DataService vie, 
		/// ze zoznam vsetkych znamych lietadiel moze poskytnut z cache, a nemusi sa dotazovat DataService-u. 
		/// </summary>
		private bool _airplaneCacheFullyLoaded;

		/// <summary>
		/// Vrati DAL vrstvu
		/// </summary>
		/// <returns></returns>
		private DataAccessLayer.DataAccessLayer DAL
		{
			get
			{
				return DataAccessLayer.DataAccessLayer.getInstance();
			}
		}


		private AirplanesDataService()
		{
			this._cacheAirplanes = new Dictionary<string, Airplane>();
		}


		public static AirplanesDataService getInstance()
		{
			if(instance == null)
			{
				instance = new AirplanesDataService();
			}

			return instance;
		}


		/// <summary>
		/// Asynchronna metoda na dohladanie lietadla podla jeho registracnej znacky. 
		/// </summary>
		/// <param name="registration"></param>
		/// <returns></returns>
		public async Task<Airplane> GetAirplaneAsync(string registration)
		{
			await Task.Run(() =>
			{
				if(!this._cacheAirplanes.ContainsKey(registration))
				{
					var airplane = this.DAL.GetAirplane(registration);

					if(airplane != null)
					{
						this._cacheAirplanes.Add(airplane.Registration, airplane);
					}
				}
			});

			Airplane toReturn;
			this._cacheAirplanes.TryGetValue(registration, out toReturn);

			return toReturn;
		}

		/// <summary>
		/// Tato metoda vrati seznam vsech letadel jako dvojci (volaci znak, letadlo)
		/// </summary>
		/// <returns></returns>
		public async Task<Dictionary<string, Airplane>> ListAllKnownAirplanesDictionaryAsync()
		{
			await this.ListAllKnownAirplanesAsync();

			return new Dictionary<string, Airplane>(this._cacheAirplanes);
		}


		/// <summary>
		/// Metoda pre ziskanie vsetkych znamych lietadiel - asynchronne. 
		/// 
		/// </summary>
		/// <returns></returns>
		public async Task<List<Airplane>> ListAllKnownAirplanesAsync()
		{
			List<Airplane> airplaneList = null;

			await Task.Run(() =>
			{
				if(this._airplaneCacheFullyLoaded)
				{
					airplaneList = new List<Airplane>(this._cacheAirplanes.Values);
				} else
				{
					this._cacheAirplanes.Clear();
					airplaneList = this.DAL.ListAllAirplanes();

					this._cacheAirplanes = airplaneList.ToDictionary(item => item.Registration);
					this._airplaneCacheFullyLoaded = true;
				}
			});

			return airplaneList;
		}



		/// <summary>
		/// Vyvola ulozenie daneho lietadla... 
		/// 
		/// Mimo inym si DataService sam sebe invaliduje cache. 
		/// </summary>
		/// <param name="airplane"></param>
		/// <returns></returns>
		public async Task SaveAirplaneAsync(Airplane airplane)
		{
			await Task.Run(() =>
			{
				this.DAL.Insert(airplane);
				this._airplaneCacheFullyLoaded = false;
			});
		}

		/// <summary>
		/// Vyvola vymazanie daneho lietadla na pozadi... 
		/// 
		/// Mimo inym si DataService sam sebe invaliduje cache. 
		/// </summary>
		/// <param name="airplane"></param>
		/// <returns></returns>
		public async Task DeleteAirplaneAsync(Airplane airplane)
		{
			await Task.Run(() =>
			{
				this.DAL.Delete(airplane);
				this._cacheAirplanes.Remove(airplane.Registration);
				this._airplaneCacheFullyLoaded = false;
			});
		}


		// TODO: pripadne dalsie manipulacne metody v style SaveAirplaneAsync() a DeleteAirplaneAsync()


	}
}
