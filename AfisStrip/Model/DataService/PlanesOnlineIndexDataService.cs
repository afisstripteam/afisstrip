﻿using System.Collections.ObjectModel;
using AfisStrip.Model.DataService.Interface;
using AfisStrip.Model.Tables;

namespace AfisStrip.Model.DataService
{
	public class PlanesOnlineIndexDataService : IPlanesOnlineIndexDataService
	{
		public ObservableCollection<Plane> FetchedPlanes
		{
			get
			{
				return new ObservableCollection<Plane>(); // To be implemented
			}
		}
	}
}
