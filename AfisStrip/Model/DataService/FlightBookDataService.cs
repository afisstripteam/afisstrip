﻿using System.Collections.ObjectModel;
using AfisStrip.Model.DataService.Providers;
using AfisStrip.Model.Tables;

namespace AfisStrip.Model.DataService
{
	public class FlightBookDataService : IFlightBookDataService
	{
		private readonly ObservableCollection<FlightBook> _items;

		public FlightBookDataService()
		{
			this._items = new ObservableCollection<FlightBook>();
		}

		public ObservableCollection<FlightBook> FlightBook
		{
			get
			{
				return this._items;
			}
		}
	}
}
