﻿using System.Collections.ObjectModel;
using System.Linq;
using AfisStrip.Model.DataService.Interface;
using AfisStrip.Model.Tables;

namespace AfisStrip.Model.DataService
{
	public class TimekeeperBlockDataService : ITimekeeperBlockDataService
	{
		private readonly ObservableCollection<TimekeeperBlock> _items;

		public TimekeeperBlockDataService()
		{
			this._items = new ObservableCollection<TimekeeperBlock>();
		}

		public ObservableCollection<TimekeeperBlock> TimekeeperBlock
		{
			get
			{
				return this._items;
			}
		}

		public TimekeeperBlock GetFlyingByCallSign(string callSign)
		{
			var data = this.TimekeeperBlock.Where(v => v.CallSign == callSign && v.TakeOff >= v.Landing).OrderBy(x => x.CallSign).ToList();

			if(data.Count > 0)
			{
				return data.First();
			}

			return null;
		}
	}
}
