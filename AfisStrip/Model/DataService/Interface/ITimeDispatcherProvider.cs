﻿using System.Windows.Threading;

namespace AfisStrip.Model.DataService.Interface
{
	public interface ITimeDispatcherProvider
	{
		DispatcherTimer TimeTrigger { get; }
	}
}