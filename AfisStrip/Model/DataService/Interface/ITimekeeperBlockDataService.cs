﻿using System.Collections.ObjectModel;
using AfisStrip.Model.Tables;

namespace AfisStrip.Model.DataService.Interface
{
	public interface ITimekeeperBlockDataService
	{
		ObservableCollection<TimekeeperBlock> TimekeeperBlock { get; }

		TimekeeperBlock GetFlyingByCallSign(string callSign);
	}
}