﻿using AfisStrip.Model.DataService.Providers;

namespace AfisStrip.Model.DataService.Interface
{
	public interface ISettingsProvider
	{
		ApplicationSettings AppSettings { get; set; }
	}
}