﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using AfisStrip.Model.Entities;
using AfisStrip.Model.Tables;

namespace AfisStrip.Model.DataService.Interface
{
	public interface IPlaneUsageRecordsDataService
	{

		/// <summary>
		/// Tato metoda vrati zoznam aktualne znamych zaznamov o "letadlach do provozu", alias 
		/// PlaneUsageRecord-ov (teda ktore lietadlo, kym leti aky ukol/typ letu). 
		/// </summary>
		/// <returns></returns>
		Task<IEnumerable<PlaneUsageRecord>> ListAllPlanesUsagesAsync(); 



		/// <summary>
		/// Tato metoda umozni dotazovat sa, ci existuje PlaneUsageRecord pre konkretne lietadlo 
		/// identifikovane registracnou znackou. 
		/// </summary>
		/// <param name="callSign"></param>
		/// <returns></returns>
		Task<PlaneUsageRecord> GetUsageForAirplaneAsync(string registration);




		Task AddNewPlaneUsageRecordAsync(PlaneUsageRecord planeUsageRecord);


		Task RemovePlaneUsageRecordAsync(PlaneUsageRecord planeUsageRecord);



	}
}