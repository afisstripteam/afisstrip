﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AfisStrip.Model.Entities;

namespace AfisStrip.Model.DataService.Interface
{

	/// <summary>
	/// Tento interface definuje metody, ktore poskytuje DataService pre entitu Flights.  
	/// 
	/// </summary>
	public interface IFlightsDataService
	{
		// TODO: 4.3.2017 :: casom doplnit vycet metod podla realnej implementacie FlightDataService-u.



		Task InsertNewFlightAsync(Flight flight);


		Task UpdateFlightAsync(Flight flight); 


		Flight GetFlightById(int sqlId);



		List<Flight> ListFlightsForTimeSpan(DateTime fromTime, DateTime toTime, FlightsDataService.FlightTimeMode mode = FlightsDataService.FlightTimeMode.BEGIN_OR_END); 



	}

}
