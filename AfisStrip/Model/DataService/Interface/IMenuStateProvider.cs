﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;

namespace AfisStrip.Model.DataService.Interface
{
	public interface IMenuStateProvider
	{
		event PropertyChangedEventHandler PropertyChanged;
		ICommand Export { get; }
		string FilenameExport { get; set; }
		string FlightDispatcher { get; set; }
		string FlightLevelSelectedItem { get; set; }
		List<string> FlightLevelSource { get; }
		string TrackSelectedItem { get; set; }
		bool IsTowInUse { get; set; }
		bool IsWinchInUse { get; set; }
	}
}