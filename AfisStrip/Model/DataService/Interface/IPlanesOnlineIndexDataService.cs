﻿using System.Collections.ObjectModel;
using AfisStrip.Model.Tables;

namespace AfisStrip.Model.DataService.Interface
{
	public interface IPlanesOnlineIndexDataService
	{
		ObservableCollection<Plane> FetchedPlanes { get; }
	}
}