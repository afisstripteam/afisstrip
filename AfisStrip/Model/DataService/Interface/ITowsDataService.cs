﻿using System.Collections.ObjectModel;
using AfisStrip.Model.Tables;

namespace AfisStrip.Model.DataService.Interface
{
	public interface ITowsDataService
	{
		ObservableCollection<PlaneTow> Tows { get; }

		PlaneTow GetByCallSign(string callSign);
	}
}