﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using AfisStrip.Model.Tables;
using AfisStrip.ViewModel.Wrappers;

namespace AfisStrip.Model.DataService.Interface
{
	public interface IStripsDataService
	{
		Dictionary<string, string> status { get; }
		Dictionary<string, string> category { get; }
		ObservableCollection<StripWrapper> InATZ { get; }
		ObservableCollection<StripWrapper> OutOfATZ { get; }
		Plane SelectedTow { get; set; }
		//List<string> Tows { get; }
	}
}