﻿using System.Collections.ObjectModel;
using AfisStrip.Model.Tables;

namespace AfisStrip.Model.DataService.Providers
{
	public interface IFlightBookDataService
	{
		ObservableCollection<FlightBook> FlightBook { get; }
	}
}
