﻿using AfisStrip.Model.Repositories;

namespace AfisStrip.Model.DataService.Interface
{
	public interface IDBProvider
	{
		FlightBookRepository FlightBookRepository { get; }
		PlanesDatabaseRepository PlanesDatabaseRepository { get; }
		StripsRepository StripsRepository { get; }
		TimekeeperBlockRepository TimekeeperBlockRepository { get; }
	}
}