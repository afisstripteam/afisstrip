﻿using AfisStrip.Model.DataService.Interface;
using AfisStrip.Model.Repositories;

namespace AfisStrip.Model.DataService.Providers
{
	public class DBProvider : IDBProvider
	{
		public StripsRepository StripsRepository { get; private set; }
		public FlightBookRepository FlightBookRepository { get; private set; }
		public PlanesDatabaseRepository PlanesDatabaseRepository { get; private set; }
		public TimekeeperBlockRepository TimekeeperBlockRepository { get; private set; }

		public DBProvider()
		{
			/*var directory = "database";
			System.IO.Directory.CreateDirectory(directory);
			var db = new SQLiteConnection(directory + "/strips.sqlite");

			db.CreateTable<FlightBook>();
			db.CreateTable<Strip>();
			db.CreateTable<Plane>();
			db.CreateTable<TimekeeperBlock>();*/

			this.StripsRepository = new StripsRepository();
			this.FlightBookRepository = new FlightBookRepository();
			this.PlanesDatabaseRepository = new PlanesDatabaseRepository();
			this.TimekeeperBlockRepository = new TimekeeperBlockRepository();
		}
	}
}
