﻿using System.Collections.Generic;
using System.Windows.Input;
using AfisStrip.Libs.ObservableObjects;
using AfisStrip.Model.DataService.Interface;
using AfisStrip.ViewModel;
using Microsoft.Win32;

namespace AfisStrip.Model.DataService.Providers
{
	public class MenuStateProvider : ObservableObject, IMenuStateProvider
	{
		public string FlightDispatcher { get; set; }

		private bool _isWinchInUse;
		public bool IsWinchInUse
		{
			get
			{
				return _isWinchInUse;
			}
			set
			{
				_isWinchInUse = value;
				OnPropertyChanged(nameof(IsWinchInUse));
				OnPropertyChanged(nameof(MainViewModel.ContactMessage));
			}
		}

		private bool _isTowInUse;
		public bool IsTowInUse
		{
			get
			{
				return _isTowInUse;
			}
			set
			{
				_isTowInUse = value;
				OnPropertyChanged(nameof(IsTowInUse));
				OnPropertyChanged(nameof(MainViewModel.ContactMessage));
			}
		}

		private string _trackSelectedItem;
		public string TrackSelectedItem
		{
			get
			{
				return _trackSelectedItem;
			}
			set
			{
				_trackSelectedItem = value;
				OnPropertyChanged(nameof(TrackSelectedItem));
				OnPropertyChanged(nameof(MainViewModel.ContactMessage));
			}
		}

		Dictionary<string, string> _flightLevelTable = new Dictionary<string, string> {
			{"Neaktivní", "Neaktivní"}, {"4000 ft", "1200 m"}, {"5000 ft", "1500 m"},
			{"FL60", "1800 m"}, {"FL65", "1975 m"}, {"FL70", "2100 m"}, {"FL75", "2275 m"}, {"FL80", "2400 m"}, {"FL85", "2575 m"}, {"FL90", "2750 m"},  {"FL95", "2875 m"}
		};

		public List<string> FlightLevelSource { get { return new List<string>(_flightLevelTable.Keys); } }

		private string _flightLevelSelectedItem;
		public string FlightLevelSelectedItem
		{
			get
			{
				return _flightLevelSelectedItem;
			}
			set
			{
				_flightLevelSelectedItem = value;
				OnPropertyChanged(nameof(FlightLevelSelectedItem));
				OnPropertyChanged(nameof(MainViewModel.ContactMessage));
			}
		}

		public string FilenameExport { get; set; }
		public ICommand Export
		{
			get
			{
				return new RelayCommand((parameter) =>
				{
					SaveFileDialog dialog = new SaveFileDialog()
					{
						Filter = "Csv Files(*.csv)|*.csv",
						DefaultExt = ".csv"
					};

					if(dialog.ShowDialog() == true)
					{
						FilenameExport = dialog.FileName;
						OnPropertyChanged(nameof(FilenameExport));
					}
				});
			}
		}
	}
}
