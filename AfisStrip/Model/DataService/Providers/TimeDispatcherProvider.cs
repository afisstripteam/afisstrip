﻿using System;
using System.Windows.Threading;
using AfisStrip.Model.DataService.Interface;

namespace AfisStrip.Model.DataService.Providers
{
	public class TimeDispatcherProvider : ITimeDispatcherProvider
	{
		public DispatcherTimer TimeTrigger
		{
			get;
			private set;
		}
		
		public TimeDispatcherProvider() 
		{
			this.TimeTrigger = new DispatcherTimer {
				Interval = TimeSpan.FromSeconds(1)
			};
			this.TimeTrigger.Start();
		}
	}
}
