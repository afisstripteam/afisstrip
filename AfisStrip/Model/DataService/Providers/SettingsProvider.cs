﻿using System.IO;
using AfisStrip.Model.DataService.Interface;
using Newtonsoft.Json;

namespace AfisStrip.Model.DataService.Providers
{
	public class SettingsProvider : ISettingsProvider
	{
		const string METAR_DEFAULT_URL = "http://meteo.rlp.cz/LKTB_meteo.htm";

		public ApplicationSettings AppSettings { get; set; }

		public SettingsProvider()
		{
			var settingsFile = @"settings.json";

			if(!File.Exists(settingsFile))
			{
				var appSettings = new ApplicationSettings();

				//If file not found use default settings
				appSettings.TimeFormat = "HH:mm";
				appSettings.AirportLongtitude = 16.555503;
				appSettings.AirportLatitude = 49.237304;
				appSettings.AirportTimezone = 2;
				appSettings.INCFAAfterMinutes = 50;
				appSettings.SafetyCheckAfterMinutes = 20;
				appSettings.MetarUrl = METAR_DEFAULT_URL;

				using(var fs = File.Open(@"settings.json", FileMode.CreateNew))
				using(var sw = new StreamWriter(fs))
				using(JsonWriter jw = new JsonTextWriter(sw))
				{
					jw.Formatting = Formatting.Indented;

					var serializer = new JsonSerializer();
					serializer.Serialize(jw, appSettings);
				}
			}

			var jsonSerializer = new JsonSerializer();

			using(var sr = new StreamReader(settingsFile))
			using(var jsonTextReader = new JsonTextReader(sr))
			{
				this.AppSettings = jsonSerializer.Deserialize<ApplicationSettings>(jsonTextReader);
			}

			// prechod na nepredzpracovany metar
			if (this.AppSettings.MetarUrl == "http://meteo.rlp.cz/LKTB_metrep.htm")
			{
				this.AppSettings.MetarUrl = METAR_DEFAULT_URL;
			}
		}
	}

	public class ApplicationSettings
	{
		public string TimeFormat { get; set; }
		public double AirportLongtitude { get; set; }
		public double AirportLatitude { get; set; }
		public int AirportTimezone { get; set; }
		public int INCFAAfterMinutes { get; set; }
		public int SafetyCheckAfterMinutes { get; set; }
		public string MetarUrl { get; set; }
	}
}
