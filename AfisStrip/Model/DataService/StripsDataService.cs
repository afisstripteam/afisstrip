﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using AfisStrip.Model.DataService.Interface;
using AfisStrip.Model.Entities;
using AfisStrip.Model.Tables;
using AfisStrip.ViewModel.Wrappers;

namespace AfisStrip.Model.DataService
{


	/// <summary>
	/// TODO: Ak tato trieda sluzi len ako jedna spolocna komponenta, ktora uchovava aktualny stav stripov nezavislo na 
	///		  aktualne zobrazenej stranke/pouzivanom ViewModel-e, tak sa musi spravit nasledovne:
	///		- premenovat to na StripsBoard
	///		- spravit z toho Singleton, a StandAlone triedu (NIE podedena z BaseDataService!!)
	///		- pridat managemnt metody ("presun mimo ATZ", "navrat do ATZ", ...) - tak aby bola logika implementovana v ramci StripsBoard-u, a nie 
	///		  roztrusena po ViewModel-och a roznych Command-och. 
	///		- 
	/// 
	/// </summary>
	public class StripsDataService : IStripsDataService
	{
		public StripsDataService()
		{
			this._items = new ObservableCollection<StripWrapper>();
		}

		public Dictionary<string, string> status { get; } = new Dictionary<string, string>() 
		{
			{"Letí", "Letí"},
			{"Na zemi", "Na zemi"},
			{"Neznámo", "Neznámo"}
		};

		public Dictionary<string, string> category { get; } = new Dictionary<string, string>() 
		{
			{"Odlet", "Odlet"},
			{"Přílet", "Přílet"},
			{"Na zemi", "Na zemi"},
			{"Mimo ATZ", "Mimo ATZ"}
		};

		private ObservableCollection<StripWrapper> inATZ;
		private ObservableCollection<StripWrapper> outOfATZ;
		private ObservableCollection<StripWrapper> _items;

		public ObservableCollection<StripWrapper> InATZ
		{
			get
			{
				if (inATZ == null) {
					this.inATZ = new ObservableCollection<StripWrapper>(this._items.Where(x => x.Strip.Category != FlightCategory.OUT_OF_ATZ).OrderBy(x => x.Strip.LastInformationChangedTime));
				}

				return this.inATZ;
			}
		}

		public ObservableCollection<StripWrapper> OutOfATZ
		{
			get
			{
				if (outOfATZ == null) {
					this.outOfATZ = new ObservableCollection<StripWrapper>(this._items.Except(this.InATZ));
				}

				return this.outOfATZ;
			}
		}


		//public List<string> Tows
		//{
		//	get
		//	{
		//		return this.inATZ.Where(y => (y.Strip.CurrentFlight.archive_UsageType == UsageType.TOW_PLANE)).Select(x => x.Strip.CurrentFlight.Plane.Registration).ToList();
		//	}
		//}

		public Plane SelectedTow { get; set; }
	}
}
