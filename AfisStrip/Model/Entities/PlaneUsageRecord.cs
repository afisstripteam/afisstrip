﻿
using System.Diagnostics.CodeAnalysis;

namespace AfisStrip.Model.Entities
{



	/// <summary>
	/// Vycet roznych typov "ucelu" (pouzitia) daneho lietadla... napr. prelet, miestna termika, oznacenie vlecnej, atd..  
	/// </summary>
	[SuppressMessage("ReSharper", "InconsistentNaming")]
	public enum PlaneUsageTypeEnum
	{
		UNKNOWN,
		TOW_PLANE,      // <-- vlecna
		WINCH_FLIGHTS,  // <-- lietanie na navijaku 
		LOCAL_FLIGHT,   // <-- miestna termika 
		OUT_AND_RETURN, // <-- prelet 
		OTHER
	}


	/// <summary>
	/// Tato trieda sluzi ako reprezentacia spojenia medzi (fyzickym) lietadlom,
	/// posadkou, a pripadne ucelom letu. Poskytuje meta-informacie (niekde bokom), 
	/// z ktorych sa da cerpat, ked sa chce pouzit dane lietadlo -- napr. doplnit posadku alebo ucel letu. 
	/// 
	/// 
	/// </summary>
	public class PlaneUsageRecord
	{

		/// <summary>
		/// Fyzicke lietadlo, ktore je pouzivane 
		/// </summary>
		public Airplane Plane { get; private set; }


		/// <summary>
		/// Posadka (kto v tom leti - napr. zapis do smiraka a podobne)
		/// </summary>
		public string Crew { get; set; }




		/// <summary>
		/// Typ letu - napr. ci to je vlecna, alebo vetron na prelet a pod. 
		/// </summary>
		public PlaneUsageTypeEnum PlaneUsageTypeEnum { get; set; }




		/// <summary>
		/// Konstruktor instancie -- vzdy vyzaduje vyplnit instanciu Airplane, pretoze bez nej nedava tato instancia zmysel. 
		///  
		/// </summary>
		/// <param name="plane"></param>
		/// <param name="crew"></param>
		/// <param name="planeUsageTypeEnum"></param>
		public PlaneUsageRecord(Airplane plane, string crew = null, PlaneUsageTypeEnum planeUsageTypeEnum = PlaneUsageTypeEnum.UNKNOWN)
		{
			this.Plane = plane;
			this.Crew = crew;
			this.PlaneUsageTypeEnum = planeUsageTypeEnum; 
		}


		public override string ToString()
		{
			return this.Plane + " @" + this.Crew + "[" + this.PlaneUsageTypeEnum + "]";
		}
	}


}
