﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AfisStrip.Libs;

namespace AfisStrip.Model.Entities
{


	/// <summary>
	/// Tato trieda reprezentuje jeden let (alebo tiez moze oznacovat jeden "pohyb" v riadenom priestore). 
	/// 
	/// "Let" je suvisle obdobie, ktore ma cas zaciatku a konca (napr. vzlet + pristanie, alebo vstup do ATZ + vystup z ATZ). 
	/// Kazdy let sa tyka konkretneho lietadla a konkretneho "uzitia" lietadla (PlaneUsageRecord) -- teda posadky a ucelu letu. 
	/// 
	/// Let moze byt "neaktivny", "otvoreny" alebo "uzavrety". "Neaktivnym" letom je taka instancia, ktora nema vyplneny cas startu, 
	/// pripadne ho ma v buducnosti. "Otvoreny" let je taky, ktory ma definovany cas zaciatku v minulosti a este nema definovany cas ukoncenia, 
	/// pripadne je cas ukoncenia v buducnosti. "Uzavrety" je taky, ktory ma definovany cas ukoncenia, ktory je v minulosti. 
	/// 
	/// Kym je let "otvoreny", pamata si "navaznosti" na ostatne "zive" instancie, napr. Plane alebo PlaneUsageRecord. 
	/// V momente, kedy sa let ukoncuje sa prepisu (zkopiruju) potrebne hodnoty do lokalnych atributov a referencie na externe objekty
	/// sa rusia. Tym dojde k "zakonzervovaniu" zaznamu. 
	/// 
	/// Kazdy let u seba obsahuje tiez zoznam Eventov -- event je nejaka udalost, ktoru obsluha zadala alebo zmenila do stripu. 
	/// Udalostami su typicky: zmeny stavov (vzlet/pristanie/opustenie ATZ/vstup do ATZ/aktualizacia polohy/poznamky, aktualizacia radioveho 
	/// kontaktu atd... Tieto udalosti sa v "surovej forme" mozu zaznamenavat hned ako nastavu. Pri pripadnom exporte moze existovat 
	/// "zlucovac", ktory jednotlive zmeny, ktore nastali v tej istej minute (v rozsahu +/- 45s), zluci dohromady. 
	/// 
	/// Zo zoznamu Eventov sa nasledne mozu generovat zaznamy do "knihy radiokorespondence". 
	/// 
	/// 
	/// Z instancie letu (Flight) sa nasledne daju generovat jednotlive zaznamy, napr. do Smiraku, Knihy priletov a odletov, 
	/// statistiky letoveho dna, atd. 
	/// 
	/// 
	/// Instancia Flight-u by sa mala (najlepsie) vytvarat az ked lietadlo vzlietlo -- teda ked sa zapisuje rovno BeginningTime a BeginningType. 
	/// Preferovany konstruktor je <code>Flight(PlaneUsageRecord crewAndUsageInfo, FlightEventType beginningEventType, DateTime beginningTime)</code>.  
	/// 
	/// 
	/// 
	/// 
	/// </summary>
	public class Flight
	{

		/// <summary>
		/// Enum stavu, v akom moze dany Flight byt.  
		/// </summary>
		public enum Status
		{
			NON_ACTIVE,	// -- ak cas zaciatku nastane v buducnosti (alebo este nie je definovany)
			ACTIVE,		// -- ak cas zaciatku je v minulosti a cas ukoncenia este nie je definovany (alebo je v buducnosti)
			FINISHED	// -- ak casy zaciatku aj ukoncenia su v minulosti 
		}



		/// <summary>
		/// ID-cko, pod ktorym je tento let ulozeny v SQLite databaze. 
		/// </summary>
		[PrimaryKey]
		public int SqlId { get; set; }




		/// <summary>
		/// Cas, kedy let "zacal". Moze byt napr. cas vzletu, alebo cas vstupu do ATZ. 
		/// </summary>
		public DateTime? BeginningTime { get; private set; }


		/// <summary>
		/// Typ udalosti, ktorou tento let zacal (napr. Vzlet, Vstup do ATZ, atd..) 
		/// </summary>
		public FlightEventType BeginningType {get; private set; }


		/// <summary>
		/// Cas, kedy tento let "skoncil". Moze byt napr. cas pristania, alebo cas opustenia ATZ.
		/// </summary>
		public DateTime? EndingTime { get; private set; }


		/// <summary>
		/// Typ udalosti, ktorou tento let skoncil (napr. Pristatie, Opustenie ATZ, atd..)
		/// </summary>
		public FlightEventType EndingType { get; private set; }


		

		/// <summary>
		/// Referencia na instanciu PlaneUsageRecord, ktora definuje lietadlo, posadku a ucel/typ letu. Kym je let "otvoreny", tak je 
		/// instancia Flight-u prepojena referenciou na instanciu PlaneUsageRecord-u, aby ked sa nieco upravi alebo zmeni, 
		/// aby sa zmena prejavila tiez tu. 
		///  
		/// Ked ale dojde k uzavretiu letu, tak sa vsetky udaje nakopiruju do samostatnych atributov a referencia 
		/// na PlneUsage sa odstrani (tym padom sa tato instancia Flight-u "zakonzervuje")
		/// </summary>
		public PlaneUsageRecord PlaneAndCrewInfo { get; set; }



		/// <summary>
		/// Archivny udaj -- "stand-alone" kopia registracnej znacky lietadla. Vyplna sa ked sa let uzavrie nakopirovanim
		/// z Plane instancie. 
		/// </summary>
		private string _archivePlaneRegistrationStr;



		/// <summary>
		/// Vrati registracku lietadla, ktoremu patri tento let. 
		/// Ak ma instancia Flight-u este referenciu na PlaneUsageRecord, tak nacita registraciu z tej instancie. Ak uz referenciu nema,
		/// pouzije sa "archivna hodnota". 
		/// </summary>
		public string PlaneRegistration
		{
			get
			{
				return this.PlaneAndCrewInfo?.Plane?.Registration ?? this._archivePlaneRegistrationStr; 
			}
		}





		/// <summary>
		/// Archivny udaj -- "stand-alone" kopia typu lietadla. Vyplna sa ked sa let uzavrie nakopirovanim z Plane instancie. 
		/// </summary>
		private string _archivePlaneTypeStr;



		/// <summary>
		/// Vrati typ lietadla, ktoremu patri tento let. 
		/// Ak ma instancia Flight-u este referenciu na PlaneUsageRecord, tak nacita registraciu z tej instancie. Ak uz referenciu nema,
		/// pouzije sa "archivna hodnota". 
		/// </summary>
		public string PlaneType
		{
			get
			{
				return this.PlaneAndCrewInfo?.Plane?.Type ?? this._archivePlaneTypeStr; 
			}
		}




		/// <summary>
		/// Archivny udaj -- "stand-alone" kopia posadky. Vyplna sa ked sa let uzavrie nakopirovanim z CrewAndUsageInfo instancie. 
		/// </summary>
		private string _archiveCrewStr;


		/// <summary>
		/// Vrati posadku lietadla, ktoremu patri tento let. 
		/// Ak ma instancia Flight-u este referenciu na PlaneUsageRecord, tak nacita registraciu z tej instancie. Ak uz referenciu nema,
		/// pouzije sa "archivna hodnota". 
		/// </summary>
		public string Crew
		{
			get
			{
				return this.PlaneAndCrewInfo?.Crew ?? this._archiveCrewStr; 
			}
		}




		/// <summary>
		/// Archivny udaj -- "stand-alone" kopia typu letu (napr. vlek, alebo prelet, alebo miestan termika). 
		/// Vyplna sa ked sa let uzavrie nakopirovanim z CrewAndUsageInfo instancie. 
		/// </summary>
		private PlaneUsageTypeEnum _archivePlaneUsageTypeEnum;



		/// <summary>
		/// Vrati typ pouzitia/letu, ku ktoremu sa viaze tento let. 
		/// Ak ma instancia Flight-u este referenciu na PlaneUsageRecord, tak nacita registraciu z tej instancie. Ak uz referenciu nema,
		/// pouzije sa "archivna hodnota". 
		/// </summary>
		public PlaneUsageTypeEnum PlaneUsageTypeEnum
		{
			get
			{
				return this.PlaneAndCrewInfo?.PlaneUsageTypeEnum ?? this._archivePlaneUsageTypeEnum; 
			}
		}



		/// <summary>
		/// Uloha -- oznacenie ulohy letu, ako sa zapisuje napr. do smiraku (napr. "2/15", "2/19", "Vlek OK-0931" atd..) 
		/// </summary>
		public string Objective { get; set; }



		/// <summary>
		/// Zoznam "udalosti", ktore su zaznamenane k tomuto flight-u.
		/// </summary>
		public List<FlightEvent> ListOfEvents { get; private set; } 




		/// <summary>
		/// Vrati aktualny status tejto instancie letu -- ci let este prebieha (ACTIVE) alebo uz je skonceny (FINISHED), pripadne este nezacal (NON_ACTIVE). 
		/// </summary>
		public Flight.Status CurrentState {
			get
			{
				if (this.BeginningTime == null || DateTime.Now < this.BeginningTime.Value) {
					return Status.NON_ACTIVE; 
				}

				if ( (this.BeginningTime != null && DateTime.Now >= this.BeginningTime.Value)  
					&& (this.EndingTime == null || DateTime.Now < this.EndingTime.Value)   ) {
					return Status.ACTIVE;
				}

				return Status.FINISHED; 
			}
		}





		/// <summary>
		/// "Minimalisticky" konstruktor pre zalozenie letu -- k vytvoreniu potrebuje len instanciu PlaneUsageRecord, aby vedel, 
		/// k akemu lietadlu a akej posadke sa tento let viaze. 
		/// 
		/// Tento konstruktor NIE JE DOPORUCENE pouzivat -- pretoze vytvori let, ktory je v stave NON_ACTIVE (este nezacal). 
		/// </summary>
		public Flight(PlaneUsageRecord planeAndCrewInfo)
		{
			this.ListOfEvents = new List<FlightEvent>(); 
			this.PlaneAndCrewInfo = planeAndCrewInfo;
			
		}



		/// <summary>
		/// Standartny konstruktor pre vytvorenie letu, ktory uz zacal. 
		/// K vytvoreniu potrebuje PlaneUsageRecord, ktory definuje ku ktoremu lietadlu a akej posadke sa tento zaznam viaze. 
		/// Tiez preberie cas zaciatku a typ udalosti, ktorou tento let zacal (napr. vzlet, alebo vstup do ATZ). 
		/// </summary>
		/// <param name="planeAndCrewInfo"></param>
		/// <param name="beginningEventType"></param>
		/// <param name="beginningTime"></param>
		public Flight(PlaneUsageRecord planeAndCrewInfo, FlightEventType beginningEventType, DateTime beginningTime) : this(planeAndCrewInfo)
		{
			this.BeginningType = beginningEventType;
			this.BeginningTime = beginningTime; 

		}



		
		/// <summary>
		/// Tato metoda umoznuje zalogovat vzniknutu udalost k tomuto letu. 
		/// Instanciu udalosti musi vytvorit a nalnit volajuci. 
		/// </summary>
		/// <param name="flightEvent"></param>
		public void LogEvent(FlightEvent flightEvent) 
		{
			this.ListOfEvents.Add(flightEvent);
		}





		/// <summary>
		/// Toto je pomocna proxy-metoda na zalogovanie udalosti. Umoznuje zadat jednotlive parametre ako parametry, 
		/// z ktorych vytvori instanciu FlightEvent-u a prida ju do zoznamu udalosti. Pridanie udalosti prebieha 
		/// pomocou metody LogEvent(FlightEvent). 
		/// 
		/// </summary>
		/// <param name="type"></param>
		/// <param name="time"></param>
		/// <param name="positionStr"></param>
		/// <param name="noteStr"></param>
		public void LogEvent(FlightEventType type, DateTime time, string positionStr = null, string noteStr = null)
		{
			var e = new FlightEvent() { Type = type, Time = time, Note = noteStr, Position = positionStr};
			this.LogEvent(e);

		}


		/// <summary>
		/// Metoda, ktorou sa da otvorit let, teda zapisat BeginningTime a BeginningType. ,
		/// Ak bol let uz otvoreny, tak tato metoda umoznuje PREPISAT pociatocne podmienky uz otvoreneho letu.  
		/// 
		/// </summary>
		/// <param name="beginningEventType"></param>
		/// <param name="beginningTime"></param>
		public void OpenFlight(FlightEventType beginningEventType, DateTime beginningTime)
		{
			this.BeginningTime = beginningTime;
			this.BeginningType = beginningEventType; 


		}



		/// <summary>
		/// Metoda, ktorou sa da uzavriet let, teda zapisat EndingTime a EndingType. 
		/// Ak let uz bol uzavrety, tak umoznuje zmenit ukoncovacie podmienky (cas a typ).
		///
		/// POZOR!! Pri uzavreti letu sa pokusi nakopirovat (zaarchivovat) hodnoty z pripojenych instancii 
		/// </summary>
		/// <param name="type"></param>
		/// <param name="time"></param>
		public void CloseFlight(FlightEventType type, DateTime time)
		{
			this.EndingTime = time;
			this.EndingType = type; 

			this.TryArchivateValues();

			// -- na zaver odstranim referenciu, cim sa tato instancia zakonzervuje. 
			this.PlaneAndCrewInfo = null; 
		}



		/// <summary>
		/// Interna metoda, ktora sa pokusi vykopirovat hodnoty z referencovanych instancii do internych atributov 'primitivnych typov',
		/// aby sa tym hodnoty zaarchivovali. 
		/// 
		/// </summary>
		private void TryArchivateValues()
		{
			if (this.PlaneAndCrewInfo != null) {
				var plane = this.PlaneAndCrewInfo.Plane; 
				if (plane != null) {
					this._archivePlaneRegistrationStr = plane.Registration;
					this._archivePlaneTypeStr = plane.Type; 
				}

				this._archiveCrewStr = this.PlaneAndCrewInfo.Crew;
				this._archivePlaneUsageTypeEnum = this.PlaneAndCrewInfo.PlaneUsageTypeEnum; 
			}

		}
		

	}



}
