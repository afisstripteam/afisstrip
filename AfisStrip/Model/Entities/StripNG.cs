﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace AfisStrip.Model.Entities
{



	/// <summary>
	/// Enum pre mozne stavy letu. 
	/// </summary>
	[SuppressMessage("ReSharper", "InconsistentNaming")]
	public enum FlightStatus
	{
		UNKNOWN,
		ON_GROUND,
		IN_FLIGHT
	}



	/// <summary>
	/// Mozne kategorie letov 
	/// </summary>
	[SuppressMessage("ReSharper", "InconsistentNaming")]
	public enum FlightCategory
	{
		UNKNOWN,
		LOCAL_FLIGHT,   // <-- miestny let
		DEPARTURE,      // <-- odlet (z letiska prec) 
		ARRIVAL,        // <-- prilet na letisko
		TRANSIT,        // <-- transit cez ATZ 
		OUT_OF_ATZ,     // <-- Mimo ATZ
		//OUT_AND_RETURN  // <-- odlet niekam prec ale s ocakavanym navratom.  15.2.2018: OUT_AND_RETURN zakomentovany, kedze nedava zmysel. Ked preletar vzlietol, tak je moj DEPARTURE traffic; ked opusti ATZ tak je OUT_OF_ATX, a ak sa nahlasil naspat do ATZ, tak je moj ARRIVAL 

	}




	/// <summary>
	/// Nova implementacia entity Strip. Reprezentuje jeden zaznam na "tabuli" stripov. 
	/// 
	/// Strip obsahuje instanciu PlaneUsageRecord (ktora obsahuje Plane) a dodatkove informacie, ktore si riadiaci 
	/// v stripe uklada. 
	/// 
	/// 
	/// Zatial ma docasne v nazve suffix "NG" (New Generation), ktora sa ale moze odstranit (refaktorovat) po tom, co sa 
	/// prestane pouzivat stara entita Strip-u. 
	/// 
	/// Este presne neviem, ci tato trieda bude alebo nebude implementovat INotifyPrpertyChanged -- ale radsej by som bol, 
	/// keby sa tato trieda obisla BEZ INotifyProp.Chag., pretoze to je ZLO! :)  Ako INPC wrapper pre triedu Strip 
	/// je mozne pouzit ViewModel.StripWrapper. 
	/// 
	/// 
	/// </summary>
	public class StripNG
	{

		
		/// <summary>
		/// Priradena instancia PlaneUsageRecord-u, ktora tiez obsahuje instanciu lietadla, posadku, a typ letu (napr. ci je to vlecna, 
		/// miestna termika alebo prelet). 
		/// 
		/// Toto je HLAVNY zdroj udajov (a identifikacie) pre Strip -- definuje volacku lietadla, takze definuje k akemu lietadlu 
		/// sa tento strip vztahuje. 
		/// 
		/// </summary>
		public PlaneUsageRecord PlaneAndCrewAndUsage { get; set; }



		/// <summary>
		/// Tato property obsahuje aktualnu instanciu letu, ktory Strip vyprodukoval na zaklade pouzitia instancie PlaneAndCrewUsage
		/// a zaroven doplnenim casov a typov, ako let vznikol alebo nevznikol. 
		/// 
		/// POZOR! Property moze mat hodnotu NULL, co znamena, ze aktualny Strip zatial NEMA ASOCIOVANY ziadnu instaciu let-u, 
		/// ktory by dany Strip popisoval -- napr. ze lietadlo je este na zemi, ale strip je len "predchystany". Az v momente, kedy 
		/// obsluha zada "Vzlet", tak Strip zabezpeci, ze sa vytvori nova instancia Flight-u, zapisu sa do nej vsetky zname informacie,
		/// zapise sa cas vzletu, a tym zacal novy Flight existovat. V priebehu letu sa zmenene udaje zo Strip-u mozu do Flight-u 
		/// prepisovat, pretoze mame referenciu na jej instancciu. Ked dojde k pristaniu, tak sa Flight uzavrie, a referencia sa zahodi 
		/// (Strip sa pripravi na novy vzlet lietadla, napr. vlecnej, ktora potiahne noveho vetrona). 
		/// 
		/// Property je nastavena ako Internal, pretoze k nej musi pristupovat StripManager. Nie je to idealne, lepsie by bolo, keby bola private,
		/// ale ak chceme mat obsluzne metody v StripManager-ovi, tak nemoze byt private.   
		/// </summary>
		internal Flight CurrentFlight { get; set; }



		/// <summary>
		/// Stringovy udaj o vstupnom bode, kde lietadlo vstupuje do okrsku.  
		/// </summary>
		public string EntryPoint { get; set; }


		/// <summary>
		/// Stringovy udaj o (predpokladanom) vystupnom bode, resp. kam lietadlo leti. 
		/// </summary>
		public string ExitPoint { get; set; }




		/// <summary>
		/// Stringovy udaj o "aktualnej" polohe (ktoru si sem vpisuje riadiaci). 
		/// </summary>
		public string Position { get; set; }



		/// <summary>
		/// Poznamka. 
		/// </summary>
		public string Notes { get; set; }



		/// <summary>
		/// Cas poslednej manipulacia so Strip-om, pricom sa menili nejake data v suvislosti s letom 
		/// (napr. aktualizoval sa popis, poloha, vstup/vystup z ATZ, atd..). 
		/// 
		/// Tento cas by sa mal zapisovat pri kazdej akcii s lietadlom. 
		/// </summary>
		public DateTime LastInformationChangedTime { get; private set; }


		/// <summary>
		/// Cas posledneho radioveho kontaktu -- teda ze uzivatel klikol na tlacitko "Aktualizovat radiovy kontakt". 
		/// </summary>
		public DateTime LastRadioContact { get; private set; }




		/// <summary>
		/// Status letu... ci leti, je na zemi, alebo co s nim je.  
		/// </summary>
		public FlightStatus Status { get; internal set; }



		/// <summary>
		/// Kategoria letu -- prilet/odlet/mimo ATZ/local...  
		/// </summary>
		public FlightCategory Category { get; set; }



		/// <summary>
		/// Poradove cislo strip-u - sluzi na zoradenie Stripov podla volby uzivatela. 
		/// </summary>
		public int? OrderNumber { get; set; }


		/// <summary>
		/// Proxy property, ktora zodpovie, ci stav letu je 'IN_FLIGHT'. 
		/// </summary>
		public bool IsFlying => this.Status == FlightStatus.IN_FLIGHT;


		// ========== [ Methods ] ====================
		/// <summary>
		/// Konstruktor -- vyzaduje zadat instanciu PlaneUsageRecord, pretoze bez nej nema Strip ziadny vyznam -- nema 
		/// ani prepojenie na lietadlo, posadku, ani na dalsie informacie.. 
		/// 
		/// </summary>
		/// <param name="planeAndCrewAndUsage"></param>
		public StripNG(PlaneUsageRecord planeAndCrewAndUsage)
		{
			this.PlaneAndCrewAndUsage = planeAndCrewAndUsage;
			this.LastInformationChangedTime = DateTime.Now; 
			this.Status = FlightStatus.UNKNOWN; 
		}

		/// <summary>
		/// Tato metoda sluzi k tomu, ze ohlasi instancii Strip-u, ze doslo k nejakej zmene informacii (zapisu polohy/poznamky), 
		/// ktore by sa mohli propagovat dalej -- typicky do historie udalosti aktivneho Flight-u. 
		/// </summary>
		/// <param name="time"></param>
		public void NotifyAboutPositionOrNoteUpdate(DateTime time)
		{
			throw new NotImplementedException();

		}

		/// <summary>
		/// Oznami stripu, ze (a kedy) prebehol radiovy kontakt s lietadlom -- typicky to bude vzdy najnovsi cas, teda 
		/// bude aktualizovany cas posledneho kontaktu. 
		/// 
		/// Radiovy kontakt (napr. "kontrola polohy a vysky") sa bude logovat aj do udalosti v ramci letu. 
		/// To je rozdiel medzi RecordRadioContact a RecordContactWithPlane
		///  
		/// </summary>
		/// <param name="time"></param>
		public void RecordRadioContact(DateTime time)
		{
			if (time > this.LastRadioContact) {
				this.LastRadioContact = time;
			}

			this.RecordLastInformationChangedTime(time);
		}

		/// <summary>
		/// Oznami stripu, kedy prebehol *akykolvek* kontakt s lietadlom. "Kontaktom" sa moze povazovat nejake radiove vysielanie, 
		/// vzlet, pristanie, vstup/vystup z ATZ, alebo radiove overenie polohy.
		/// 
		/// Typicky bude zadany vzdy najnovsi cas, teda bude aktualizovany cas posledneho kontaktu. 
		/// 
		/// "Obecny" kontakt sa nikam nezapisuje, pretoze je pravdepodobne, ze je zalogovana ina pridruzena udalost -- napr. "vzlet", 
		/// "pristatie", a pod. 
		/// 
		/// </summary>
		/// <param name="time"></param>
		public void RecordLastInformationChangedTime(DateTime time)
		{
			if (time > this.LastInformationChangedTime) {
				this.LastInformationChangedTime = time;
			}
		}
	}
}
