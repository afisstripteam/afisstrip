﻿
using System;
using AfisStrip.Libs;

namespace AfisStrip.Model.Entities
{

	/// <summary>
	/// Tato trieda reprezentuje jedno fyzicke lietadlo. 
	/// 
	/// Ma len zakladne atributy, ktore sa vztahuju k fyzickemu lietadlu (daneho kusu). 
	/// Z tejto triedy je mozne vytvarat aj "ad-hoc" instancie pre novo-spozorovane lietadla. 
	/// </summary>
	public class Airplane
	{

		private string _registration;
		/// <summary>
		/// Registracna znacka (napr. OK-ABC)
		/// 
		/// Unikatny identifikator, moze byt prim. klucom. 
		/// </summary>
		[PrimaryKey]
		public string Registration {
			get { return this._registration; }
			set { this._registration = value == null ? null : value.ToUpper(); }
		}


		/// <summary>
		/// Typ (typove oznacenie) lietadla.
		/// Napr. Z142
		/// </summary>
		public string Type { get; set; }


		/// <summary>
		/// Nazov vlastnika/provozovatela. 
		/// Zatial (v prvych verziach) sa nemusi rozlisovat medzi vlastnikom a provozovatelom. 
		/// 
		/// Tento udaj sluzi len ako informativny pre pripadne predvyplnenie posadky (napr. vlastnik = "Bartos", alebo "AK Krizanov")
		/// </summary>
		public string Owner { get; set; }


		/// <summary>
		/// Indikace toho jestli je letadlo "domaci", tzn. jestli ocekavame jeho navrat
		/// </summary>
		public bool IsHomebasedAtThisAirport { get; set; }


		/// <summary>
		/// Tato udalost sa vyvola, ked dojde k zmene niektoreho z atributov. 
		/// 
		/// </summary>
		public event EventHandler AttributesChanged; 

	}
}
