﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AfisStrip.Libs;

namespace AfisStrip.Model.Entities
{
	

	/// <summary>
	/// Tento event obsahuje moznosti, akeho typu mozu byt jednotlive udalosti, ktore suvisia s letom. 
	/// 
	/// 
	/// </summary>
	[SuppressMessage("ReSharper", "InconsistentNaming")]
	public enum FlightEventType
	{
		UNKNOWN,
		TAKE_OFF,
		LANDING,
		ENTERING_ATZ,
		LEAVING_ATZ,
		RADIO_CONTACT,
		POSITION_UPDATE,

	}




	/// <summary>
	/// Tato trieda reprezentuje jeden zaznam Udalosti, ktora suvisi s letom. 
	/// Kazda udalost ma svoj cas, a ma svoj typ -- napr. ze lietadlo vzlietlo, omunikovalo, alebo opustilo ATZ. 
	/// </summary>
	public class FlightEvent
	{

		[PrimaryKey]
		public int SqlId { get; set; }


		/// <summary>
		/// Typ udalosti 
		/// </summary>
		public FlightEventType Type { get; set; }


		/// <summary>
		/// Cas, kedy dana udalost nastala. 
		/// </summary>
		public DateTime Time { get; set; }


		/// <summary>
		/// Popis poloy, kde sa pri tejto udalosti lietadlo nachadzalo
		/// </summary>
		public string Position { get; set; }



		/// <summary>
		/// Poznamka, ktoru si dispecer v danom case pripisal/upravil. 
		/// </summary>
		public string Note { get; set; }


	}




}
