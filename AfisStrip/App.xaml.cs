﻿using System.Windows;
using AfisStrip.Model;
using AfisStrip.Model.DataService;
using AfisStrip.Model.DataService.Interface;
using AfisStrip.Model.DataService.Providers;
using AfisStrip.Model.Managers;
using AfisStrip.ViewModel;
using AfisStrip.ViewModel.Interfaces;
using Microsoft.Practices.Unity;

namespace AfisStrip
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		static App()
		{
		}

		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);
			IUnityContainer container = new UnityContainer();

			container.RegisterType<IFlightBookDataService, FlightBookDataService>(new ContainerControlledLifetimeManager());
			container.RegisterType<IPlanesOnlineIndexDataService, PlanesOnlineIndexDataService>(new ContainerControlledLifetimeManager());
			container.RegisterType<IPlaneUsageRecordsDataService, PlaneUsageRecordsDataService>(new ContainerControlledLifetimeManager());
			container.RegisterType<ITimekeeperBlockDataService, TimekeeperBlockDataService>(new ContainerControlledLifetimeManager());
			container.RegisterType<ITowsDataService, TowsDataService>(new ContainerControlledLifetimeManager());
			container.RegisterType<IStripsDataService, StripsDataService>(new ContainerControlledLifetimeManager());
			container.RegisterType<IFlightsDataService, FlightsDataService>(new ContainerControlledLifetimeManager());

			container.RegisterType<ITimeDispatcherProvider, TimeDispatcherProvider>(new ContainerControlledLifetimeManager());
			container.RegisterType<ISettingsProvider, SettingsProvider>(new ContainerControlledLifetimeManager());
			container.RegisterType<IMenuStateProvider, MenuStateProvider>(new ContainerControlledLifetimeManager());
			container.RegisterType<IDBProvider, DBProvider>(new ContainerControlledLifetimeManager());
			
			container.RegisterType<IMainViewModel, MainViewModel>();
			container.RegisterType<IEndOfDayViewModel, EndOfDayViewModel>();
			container.RegisterType<IFlightBookViewModel, FlightBookViewModel>();
			container.RegisterType<IPlanesToTrafficViewModel, PlanesToTrafficViewModel>();
			container.RegisterType<ITimekeeperBlockViewModel, TimekeeperBlockViewModel>();
			container.RegisterType<IStripsViewModel, StripsViewModel>();
			container.RegisterType<IStripManager, StripManager>();
			container.RegisterType<IPlanesDatabaseViewModel, PlanesDatabaseViewModel>();

			MyContainer.SetContainer(container);

			var mainWindow = container.Resolve<MainWindow>();
			Application.Current.MainWindow = mainWindow;
			Application.Current.MainWindow.Show();
		}
	}
}
