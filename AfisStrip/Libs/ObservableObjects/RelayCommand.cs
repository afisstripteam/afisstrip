﻿using System;
using System.Diagnostics;
using System.Windows.Input;

namespace AfisStrip.Libs.ObservableObjects
{
    public class RelayCommand : ICommand 
    { 
        #region Fields
        
        readonly Action<object> _execute;
        readonly Func<bool> _canExecute;
        
        #endregion // Fields 
        
        #region Constructors
        public RelayCommand(Action<object> execute) 
            : this(execute, () => true) 
        {
        }
        
        public RelayCommand(Action<object> execute, Func<bool> canExecute) 
        { 
            if (execute == null) throw new ArgumentNullException("execute");
            _execute = execute;
            _canExecute = canExecute;
        }
        
        #endregion // Constructors
        
        #region ICommand Members 
        
        [DebuggerStepThrough] 
        public bool CanExecute(object parameter) 
        { 
            return _canExecute();
        }
        public event EventHandler CanExecuteChanged 
        {
            add 
            {
                CommandManager.RequerySuggested += value;
            } 
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
        
        public void Execute(object parameter)
        {
            _execute(parameter);
        } 
        
        #endregion // ICommand Members 
    }
}
