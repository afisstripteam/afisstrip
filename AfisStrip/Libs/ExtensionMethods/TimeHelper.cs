﻿using System;

namespace AfisStrip.Libs.Misc
{
    public class TimeHelper
    {
        /// <summary>
        /// Nastavi milisekundy na 0
        /// </summary>
        /// <param name="toTruncate"></param>
        /// <returns></returns>
        public static DateTime truncatedMilliseconds(DateTime toTruncate)
        {
            return toTruncate.AddTicks(-(toTruncate.Ticks % TimeSpan.TicksPerSecond));
        }


        /// <summary>
        /// Zaokrouhli sekundy na cele minuty. Pokud je pocet sekund <= 30 zaokrouhli se dolu jinak nahoru
        /// </summary>
        /// <param name="timeToRound"></param>
        /// <returns></returns>
        public static DateTime roundSeconds(DateTime timeToRound)
        {
            var seconds = timeToRound.Second;

            timeToRound = truncatedMilliseconds(timeToRound);

            if(seconds <= 30)
            {
                return timeToRound.AddSeconds(-seconds);
            } else
            {
                return timeToRound.AddSeconds(60 - seconds);
            }
        }
    }
}
