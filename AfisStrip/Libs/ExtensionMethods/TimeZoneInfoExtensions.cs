﻿using System;

namespace AfisStrip.Libs.ExtensionMethods
{
	public static class TimeZoneInfoExtensions
	{
		/// <summary>
		/// Zatim funguje jen tak, že zjistí jestli je letní čas tak vrátí SELČ, jinak SEČ
		/// </summary>
		/// <param name="Source"></param>
		/// <returns></returns>
		public static string Abbreviation(this TimeZoneInfo Source)
		{
			if (Source.IsDaylightSavingTime(DateTime.Now)) {
				return "SELĆ";
			}

			return "SEČ";
		}
	}
}
