﻿using System.Text.RegularExpressions;
using ENG.WMOCodes.Codes;

namespace AfisStrip.Libs.ExtensionMethods
{
    public static class ExtensionMethods
    {
		/// <summary>
		/// Protoze lokani QNH je v sekci Remarks a knihovna na parsovani METARu
		/// Remarks pouze vyhodi jako string, tak potrebujeme tuto extension metodu,
		/// ktera z Remarks lokani QNH vytahne
		/// </summary>
		/// <param name="metar"></param>
		/// <returns>Lokalni QNH pokud existuje, jinak string.Empty</returns>
	    public static string ParseLocalQnh(this Metar metar)
	    {
		    if (metar.Remark == null)
		    {
			    return string.Empty;
		    }

		    var metarMatch = Regex.Match(metar.Remark, @"REG QNH (.*?)=.*?", RegexOptions.IgnoreCase);
		    if (metarMatch.Success)
		    {
			   return metarMatch.Groups[1].Value;
		    }

			return string.Empty;
	    }
	}
}
