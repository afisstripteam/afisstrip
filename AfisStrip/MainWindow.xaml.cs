﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using AfisStrip.ViewModel.Interfaces;

namespace AfisStrip
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		/// <summary>
		/// Initializes a new instance of the MainWindow class.
		/// </summary>
		public MainWindow(IMainViewModel mainViewModel)
		{
			InitializeComponent();

			this.DataContext = mainViewModel;
			this.Loaded += delegate { mainViewModel.ShowStartDispatcherPrompt(); };
			this.KeyDown += HandleEsc;
		}

		private void ExitFullscreenMode()
		{
			this.WindowStyle = System.Windows.WindowStyle.SingleBorderWindow;
			ResizeButton.Header = "Celoobrazovkový mód";
		}

		public void HandleEsc(object sender, KeyEventArgs e)
		{
			if(e.Key == Key.Escape && this.WindowStyle == System.Windows.WindowStyle.None)
				ExitFullscreenMode();
		}

		private void ResizeButton_click(object sender, RoutedEventArgs e)
		{
			if(this.WindowStyle == System.Windows.WindowStyle.None)
			{
				ExitFullscreenMode();                
			}
			else
			{
				this.WindowStyle = System.Windows.WindowStyle.None;
				this.WindowState = System.Windows.WindowState.Normal;
				this.WindowState = System.Windows.WindowState.Maximized;
				ResizeButton.Header = "Ukončit celoobrazovkový mód";
			}
		}

		private void MainWindow_OnMouseDown(object sender, MouseButtonEventArgs e)
		{
			Keyboard.ClearFocus();
		}
	}
}