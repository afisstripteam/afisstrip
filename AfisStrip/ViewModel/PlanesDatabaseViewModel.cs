﻿using AfisStrip.Libs.ObservableObjects;
using AfisStrip.Model.Entities;
using AfisStrip.ViewModel.Interfaces;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using AfisStrip.Model.DataService;
using AfisStrip.Model.DataService.Interface;

namespace AfisStrip.ViewModel
{
	public class PlanesDatabaseViewModel : IPageViewModel, IPlanesDatabaseViewModel
	{
		public ObservableCollection<Airplane> PlanesDatabaseCollection { get; set; }

		public ICommand AddNewPlane
		{
			get
			{
				return new RelayCommand(async (parameter) =>
				{
					if(!string.IsNullOrEmpty(this.NewPlaneCallSign))
					{
						Airplane newPlane = new Airplane()
						{
							Registration = this.NewPlaneCallSign.ToUpper(),
							Owner = this.NewPlaneOwner,
							Type = this.NewPlaneType,
							IsHomebasedAtThisAirport = this.NewPlaneIsHomebasedAtThisAirport
						};

						await AirplanesDataService.getInstance().SaveAirplaneAsync(newPlane);
						PlanesDatabaseCollection.Insert(0, newPlane);

						this.NewPlaneCallSign = "";
						this.NewPlaneOwner = "";
						this.NewPlaneType = "";
						this.NewPlaneIsHomebasedAtThisAirport = false;
						OnPropertyChanged(nameof(NewPlaneCallSign));
						OnPropertyChanged(nameof(NewPlaneOwner));
						OnPropertyChanged(nameof(NewPlaneType));
					}
				});
			}
		}

		public ICommand UpdateSelectedItem
		{
			get
			{
				return new RelayCommand(async (parameter) =>
				{
					if(this.GridSelectedItem != null)
					{
						await AirplanesDataService.getInstance().SaveAirplaneAsync(this.GridSelectedItem);
					}
				});
			}
		}

		public ICommand DeleteRecord
		{
			get
			{
				return new RelayCommand(async (parameter) =>
				{
					Airplane param = (Airplane)parameter;

					if(MessageBox.Show("Opravdu smazat letadlo " + param.Registration + "?", "Smazat", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
					{

						await AirplanesDataService.getInstance().DeleteAirplaneAsync(param);
						this.PlanesDatabaseCollection.Remove(param);

						OnPropertyChanged(nameof(PlanesDatabaseCollection));
					}
				});
			}
		}

		public string NewPlaneCallSign { get; set; }
		public string NewPlaneType { get; set; }
		public string NewPlaneOwner { get; set; }
		public bool NewPlaneIsHomebasedAtThisAirport { get; set; }

		private string _callSignFilter;
		private string _typeFilter;
		private string _ownerFilter;
		private bool _isHomebasedAtThisAirportFilter;
		private IMenuStateProvider _menuStateProvider;

		public string CallSignFilter
		{
			get
			{
				return _callSignFilter;
			}
			set
			{
				_callSignFilter = value;
				fillPlanesDatabaseData();
			}
		}

		public string TypeFilter
		{
			get
			{
				return _typeFilter;
			}
			set
			{
				_typeFilter = value;
				fillPlanesDatabaseData();
			}
		}

		public string OwnerFilter
		{
			get
			{
				return _ownerFilter;
			}
			set
			{
				_ownerFilter = value;
				fillPlanesDatabaseData();
			}
		}

		public bool IsHomebasedAtThisAirportFilter
		{
			get
			{
				return _isHomebasedAtThisAirportFilter;
			}
			set
			{
				_isHomebasedAtThisAirportFilter = value;
				fillPlanesDatabaseData();
			}
		}

		public Airplane GridSelectedItem { get; set; }

		public PlanesDatabaseViewModel(IMenuStateProvider menuStateProvider)
		{
			this._menuStateProvider = menuStateProvider;
			fillPlanesDatabaseData();

			this._menuStateProvider.PropertyChanged += OnMenuChanged;
		}

		/// <summary>
		/// Ziska a vyfiltruje vsechny dostupne letadla TODO: vyresit jestli se letadlo vraci do aeroklubu
		/// </summary>
		public async void fillPlanesDatabaseData()
		{
			var planesCollection = await AirplanesDataService.getInstance().ListAllKnownAirplanesAsync();

			Func<Airplane, bool> predicate = delegate(Airplane p)
			{
				if(!string.IsNullOrEmpty(this.CallSignFilter) && (p.Registration == null || !p.Registration.ToLower().Contains(this.CallSignFilter.ToLower())))
				{
					return false;
				}

				if(!string.IsNullOrEmpty(this.TypeFilter) && (p.Type == null || !p.Type.ToLower().Contains(this.TypeFilter.ToLower())))
				{
					return false;
				}

				if(!string.IsNullOrEmpty(this.OwnerFilter) && (p.Owner == null || !p.Owner.ToLower().Contains(this.OwnerFilter.ToLower())))
				{
					return false;
				}


				if(this.IsHomebasedAtThisAirportFilter && !p.IsHomebasedAtThisAirport)
				{
					return false;
				}

				return true;
			};

			planesCollection = planesCollection.Where(predicate).OrderBy(p => p.Registration).ToList();

			this.PlanesDatabaseCollection = new ObservableCollection<Airplane>(planesCollection);

			OnPropertyChanged(nameof(PlanesDatabaseCollection));
		}

		private void OnMenuChanged(object sender, PropertyChangedEventArgs e)
		{
			if(e.PropertyName == "FilenameExport")
			{
				var a = this._menuStateProvider;
				Export(a.GetType().GetProperty(e.PropertyName).GetValue(a, null).ToString());
			}
		}

		/// <summary>
		/// Exportuje seznam letadel do CSV
		/// </summary>
		/// <param name="path"></param>
		public void Export(string path)
		{
			using(StreamWriter sw = new StreamWriter(path, false, Encoding.Default))
			{
				sw.WriteLine(string.Format("sep=,"));
				sw.WriteLine(string.Format("{0}, dispečer {1}", DateTime.Now, this._menuStateProvider.FlightDispatcher));
				sw.WriteLine(string.Format("Volací znak, Typ, Majitel, Je majetek aeroklubu"));
				foreach(var plane in PlanesDatabaseCollection)
				{
					sw.WriteLine(string.Format("{0},{1},{2},{3}",
						plane.Registration, plane.Type, plane.Owner, plane.IsHomebasedAtThisAirport ? "Ano" : ""));
				}
			}
		}
	}
}
