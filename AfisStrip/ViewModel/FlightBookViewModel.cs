﻿using AfisStrip.Model.Tables;
using AfisStrip.ViewModel.Interfaces;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using AfisStrip.Model.DataService.Interface;
using AfisStrip.Model.DataService.Providers;

namespace AfisStrip.ViewModel
{
	public class FlightBookViewModel : IPageViewModel, IFlightBookViewModel
	{
		public ObservableCollection<FlightBook> FlightBookCollection { get; set; }

		private DateTime _dateTo;
		private DateTime _dateFrom;
		private string _callSignFilter;
		private string _actionFilter;
		private IFlightBookDataService _flightBookDataService;
		private IMenuStateProvider _menuStateProvider;

		public DateTime DateTo
		{
			get
			{
				return _dateTo;
			}
			set
			{
				_dateTo = value;
				if(value < _dateFrom)
				{
					DateFrom = value;
				}
				OnPropertyChanged(nameof(DateTo));
				fillFlightBookData();
			}
		}

		public DateTime DateFrom
		{
			get
			{
				return _dateFrom;
			}
			set
			{
				_dateFrom = value;
				OnPropertyChanged(nameof(DateFrom));
				fillFlightBookData();
			}
		}
		public string CallSignFilter
		{
			get
			{
				return _callSignFilter;
			}
			set
			{
				_callSignFilter = value;

				OnPropertyChanged(nameof(CallSignFilter));
				fillFlightBookData();
			}
		}

		public string ActionFilter
		{
			get
			{
				return _actionFilter;
			}
			set
			{
				_actionFilter = value;

				OnPropertyChanged(nameof(ActionFilter));
				fillFlightBookData();
			}
		}

		public FlightBookViewModel(IFlightBookDataService flightBookDataService, IMenuStateProvider menuStateProvider)
		{
			this._flightBookDataService = flightBookDataService;
			this._menuStateProvider = menuStateProvider;

			try
			{
				_dateTo = DateTime.Now;
				_dateFrom = _dateTo;

				fillFlightBookData();

			} catch(Exception ex)
			{
				Debug.WriteLine(ex);
			}

			this._menuStateProvider.PropertyChanged += OnMenuChanged;
		}

		public void fillFlightBookData()
		{
			var flightBookCollection = new ObservableCollection<FlightBook>(this._flightBookDataService.FlightBook
										.Where(
											x =>
												x.Time.Date >= _dateFrom.Date &&
												x.Time.Date <= _dateTo.Date)
										.OrderByDescending(x => x.Time));

			if(this.CallSignFilter != null && this.CallSignFilter != string.Empty)
			{
				flightBookCollection = new ObservableCollection<FlightBook>(flightBookCollection.Where(p => p.CallSign != null && p.CallSign.ToString().ToLower().Contains(this.CallSignFilter.ToLower())));
			}

			if(this.ActionFilter != null && this.ActionFilter != string.Empty)
			{
				flightBookCollection = new ObservableCollection<FlightBook>(flightBookCollection.Where(p => p.Action != null && p.Action.ToString().ToLower().Contains(this.ActionFilter.ToLower())));
			}

			this.FlightBookCollection = flightBookCollection;

			OnPropertyChanged(nameof(FlightBookCollection));
		}

		private void OnMenuChanged(object sender, PropertyChangedEventArgs e)
		{
			if(e.PropertyName == "FilenameExport")
			{
				var a = this._menuStateProvider;
				Export(a.GetType().GetProperty(e.PropertyName).GetValue(a, null).ToString());
			}
		}

		public void Export(string path)
		{
			using(StreamWriter sw = new StreamWriter(path, false, Encoding.Default))
			{
				sw.WriteLine(string.Format("sep=,"));
				sw.WriteLine(string.Format("{0}, dispečer {1}", DateTime.Now, this._menuStateProvider.FlightDispatcher));
				sw.WriteLine(string.Format("Volací znak, Akce, Čas, Trať / Poloha, Poznámky"));
				foreach(var strip in FlightBookCollection)
				{
					sw.WriteLine(string.Format("{0},{1},{2},{3},{4}",
														strip.CallSign, strip.Action, strip.Time, strip.Position, strip.Notes));
				}
			}
		}
	}
}
