﻿using System;
using AfisStrip.Model.Entities;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using AfisStrip.Libs;
using AfisStrip.Libs.ObservableObjects;
using AfisStrip.Model;
using AfisStrip.Model.DataService.Interface;
using AfisStrip.Model.Enums;
using AfisStrip.Model.Managers;
using Microsoft.Practices.Unity;

namespace AfisStrip.ViewModel.Wrappers
{

	/// <summary>
	/// Tato trieda predstavuje wrapper pre Strip-y, ktory je urceny pre vrstvu ViewModel-ov a vyssie (View). 
	/// Implementuje INPC aby bolo mozne na nu bindovat. 
	/// 
	/// Zabezpecuje len nejake utility (helper) metody ohladom Stripov... napr. moznost ako vyvolavat PropertyChanged pre 
	/// (proxy)property, ktora urcuje ofarbovanie stripu na cerveno po vyprsani limitu.  
	/// 
	/// Vsetky metody zo Strip.cs, ktore:
	///		- patria do ViewModel vrstvy 
	///		- suvisia s GUI
	///		- suvisia s "work-flow" (napr. vleky)
	/// 
	/// presunut sem do tohoto wrapper-a!  V entite Strip nemaju co hladat! 
	/// 
	/// </summary>
	public class StripWrapper : INotifyPropertyChanged, IDisposable
	{
		public event PropertyChangedEventHandler PropertyChanged;
		
		/// <summary>
		/// Samotna instancia Strip-u. 
		/// </summary>
		public StripNG Strip { get; protected set; }
		
		/// <summary>
		/// Konstruktor - vyplni instanciu Strip-u. 
		/// </summary>
		/// <param name="strip"></param>
		public StripWrapper(StripNG strip)
		{
			this.Strip = strip;
		}

		/// <summary>
		/// Vyvola udalost, ktora indikuje ze sa zmenili VSETKY vlastnosti tejto instancie -- takze vsetky binding-y by sa mali obnovit. 
		/// Funguje to tak, ze ako nazov zmenenej property sa zada prazdny string "". 
		/// </summary>
		public void RaiseAllPropertyChanged()
		{
			 this.RaisePropertyChanged(""); 
		}

		public void RaisePropertyChanged(string name)
		{
			var h = this.PropertyChanged;
			h?.Invoke(this, new PropertyChangedEventArgs(name));
		}

		/// <summary>
		/// Obsahuje callsign vleku
		/// </summary>
		public string TowedBy
		{
			get;
			set;
		}

		/// <summary>
		/// Indikuje jestli je letadlo ve vleku nebo ne
		/// </summary>
		public bool IsTowed
		{
			get
			{
				return !this.TowedBy.Equals(string.Empty);
			}
		}

		/// <summary>
		/// Vrati true pokud je letadlo vlek
		/// </summary>
		public bool IsTowPlane
		{
			get
			{
				return false; // TODO! nastavovat vlecnou danemu letadlu
				//return TowsDataService.GetInstance.Tows.Where(x => x.CallSign == this.Strip.PlaneAndCrewAndUsage.Plane.Registration).Any();
			}
		}

		private StripCheckStatusEnum _stripCheckStatus;
		/// <summary>
		/// Vyjadri aktualni stav stripu, zda je ok nebo je treba zkontrolovat.
		/// </summary>
		public StripCheckStatusEnum StripCheckStatus
		{
			get
			{
				return this._stripCheckStatus;
				
			}
			set
			{
				if (value == this._stripCheckStatus) {
					return;
				}

				this._stripCheckStatus = value;
				this.RaisePropertyChanged(nameof(this.StripCheckStatus));
			}
		}

		/// <summary>
		/// Aktualizuje hodnotu StripCheckStatus.
		/// </summary>
		public void UpdateStripCheckStatus()
		{
			if (!this.Strip.IsFlying) {
				this.StripCheckStatus = StripCheckStatusEnum.OK;
			} else {
				var settingsProvider = this.GetSettingsProvider();
				if (settingsProvider == null) {
					this.StripCheckStatus = StripCheckStatusEnum.UNKNOWN;
				}

				var minutesSinceLastContact = DateTime.Now.Subtract(this.Strip.LastRadioContact).Minutes;

				if (minutesSinceLastContact >= settingsProvider.AppSettings.INCFAAfterMinutes) {
					this.StripCheckStatus = StripCheckStatusEnum.INCERFA;
				} else if (minutesSinceLastContact >= settingsProvider.AppSettings.SafetyCheckAfterMinutes) {
					this.StripCheckStatus = StripCheckStatusEnum.SAFETY_CHECK;
				} else {
					this.StripCheckStatus = StripCheckStatusEnum.OK;
				}
			}

			
		}

		/// <summary>
		/// Metoda, pomocou ktorej sa oznami obalenemu Strip-u, ze "teraz" k nejakemu "kontaktu" s lietadlom -- bud k radiovemu kontaktu, 
		/// alebo obsluha zadala nejaky ukon, napr. ze lietadlo pristalo alebo vzlietla. 
		/// 
		/// Do Stripu sa zapise "kontakt" s aktualnym casom a vyvola sa PropertyChanged udalost, aby sa prekreslilo GUI. 
		/// 
		/// Tato metoda je typicky volana pri roznych udalostiach/akciach, ktore je mozne povazovat ze bol kontakt s lietadlom. 
		/// </summary>
		private void UpdateLastInformationChanged()
		{
			this.Strip.RecordLastInformationChangedTime(DateTime.Now);
			this.RaisePropertyChanged(nameof(this.Strip));
		}


		/// <summary>
		/// Metoda, ktorou sa explicitne oznami Strip-u, ze doslo k radiovemu komtaktu, co Strip moze vyhodnotit ze zapise 
		/// radiovy kontakt aj do historie udalosti. 
		/// 
		/// </summary>
		private void UpdateLastRadioContact()
		{
			this.Strip.RecordRadioContact(DateTime.Now);
			this.RaisePropertyChanged(nameof(this.Strip));
		}
		

		private ICommand _updateLastContactCommand;
		/// <summary>
		/// Command, kt. aktualizuje cas posledního kontaktu s daným letadlem. 
		/// </summary>
		public ICommand UpdateLastRadioContactCommand
		{
			get
			{
				if (this._updateLastContactCommand == null) {
					this._updateLastContactCommand = new RelayCommand((parameter) =>
					{
						this.UpdateLastRadioContact();
					});
				}

				return _updateLastContactCommand;
			}
		}

		private ICommand _landedCommand;

		public ICommand LandedCommand
		{
			get
			{
				if (this._landedCommand == null) {
					this._landedCommand = new RelayCommand(async (parameter) =>
					{
						//TODO: Co s vlecnou?
						//var cat = DataService.StripsDataService.GetInstance.category["Na zemi"];
						//svar stat = DataService.StripsDataService.GetInstance.status["Na zemi"];

						//if(this.Towes != null)
						//{
						//	var towed = StripsDataService.GetInstance.InATZ.Where(x => x.CallSign == this.Towes &&
						//		x.Status == DataService.StripsDataService.GetInstance.status["Letí"]).FirstOrDefault();

						//	if(towed != null)
						//	{
						//		towed.TowedBy = null;
						//	}

						//	this.Towes = null;
						//}

						try {
							if (this.Strip.Status != FlightStatus.ON_GROUND) {

								var stripManager = this.GetCurrentStripManager();
								await stripManager.RecordLandingAsync(this, DateTime.Now);  // <-- uzavrie flight a zapise zmeny do DB


								this.UpdateLastInformationChanged();


								this.RaisePropertyChanged(nameof(this.Strip));
							}
						} catch (Exception ex) {
							MessageBox.Show("Operaci nelze vykonat: " + ex.Message, "Chyba", MessageBoxButton.OK, MessageBoxImage.Warning); 
						}
					});
				}

				return this._landedCommand;
			}
		}



		private ICommand _takeoffCommand;
		public ICommand TakeoffCommand
		{
			get
			{
				if (this._takeoffCommand == null) {

					this._takeoffCommand = new RelayCommand(async (parameter) =>
					{
						try { 

							var stripManager = this.GetCurrentStripManager(); 
						
							//TODO: Start s vlecnou? Je lietadlo vetron? Ak ano, ktora vlecna ho taha? 

							if (this.Strip.Status != FlightStatus.IN_FLIGHT) {
								//this.Strip.Status = FlightStatus.IN_FLIGHT;
								//this.Strip.RecordTakeOffEvent(DateTime.Now);
								await stripManager.RecordTakeOffStandaloneAsync(this, DateTime.Now);

								this.UpdateLastInformationChanged();

								//TODO: Zapis do flight history

								this.RaisePropertyChanged(nameof(this.Strip));
							}

						} catch (Exception ex) {
							MessageBox.Show("Operaci nelze vykonat: " + ex.Message, "Chyba", MessageBoxButton.OK, MessageBoxImage.Warning);
						}

					});
				}

				return this._takeoffCommand;
			}
		}


		private ICommand _atzEnter;


		/// <summary>
		/// Command, ktery vyvolavany pri vstupu do ATZ.
		/// </summary>
		public ICommand AtzEnterCommand
		{
			get
			{
				if (this._atzEnter == null) {

					this._atzEnter = new RelayCommand(async (parameter) =>
					{
						try {

							var stripManager = this.GetCurrentStripManager();

							await stripManager.RecordEnteringATZAsync(this, DateTime.Now);
							/*
							-- obsah nasledovneho IF-u som zakomentoval, kedze v novej implementacii asi nie je potreba. 
							if (this.Strip.Status != FlightStatus.IN_FLIGHT) {
								this.Strip.Category = FlightCategory.UNKNOWN;
								//this.Strip.Status = FlightStatus.IN_FLIGHT;
								this.Strip.RecordEnteringATZ(DateTime.Now);
								this.UpdateLastContact();

								//TODO: Zapis do flight history
							}
							*/

							this.UpdateLastInformationChanged();

							this.RaisePropertyChanged(nameof(this.Strip));

						} catch (Exception ex) {
							MessageBox.Show("Operaci nelze vykonat: " + ex.Message, "Chyba", MessageBoxButton.OK, MessageBoxImage.Warning);
						}

					});
				}

				return this._atzEnter;
			}
		}


		private ICommand _atzLeft;
		/// <summary>
		/// Commmand volany pri opusteni atz.
		/// </summary>
		public ICommand AtzLeftCommand
		{
			get
			{
				if (this._atzLeft == null) {
					this._atzLeft = new RelayCommand(async (parameter) => 
					{
						try {  
							var stripManager = this.GetCurrentStripManager();

							await stripManager.RecordLeavingATZAsync(this, DateTime.Now);

							//this.Strip.Category = FlightCategory.OUT_OF_ATZ;
							//this.Strip.Status = FlightStatus.UNKNOWN;
							//this.Strip.RecordLeavingATZ(DateTime.Now);

							this.UpdateLastInformationChanged();


							this.RaisePropertyChanged(nameof(this.Strip));

						} catch (Exception ex) {
							MessageBox.Show("Operaci nelze vykonat: " + ex.Message, "Chyba", MessageBoxButton.OK, MessageBoxImage.Warning);
						}

					});
				}

				return this._atzLeft;
			}
		}

		public void Dispose()
		{
			this.Strip = null; 
		}


		/// <summary>
		/// Vrati aktualne platnu instanciu StripManager-a. 
		/// </summary>
		/// <returns></returns>
		private IStripManager GetCurrentStripManager()
		{
			return MyContainer.Instance.Resolve<IStripManager>();
		}


		/// <summary>
		/// Vrati aktualne platnu instanciu StripManager-a. 
		/// </summary>
		/// <returns></returns>
		private ISettingsProvider GetSettingsProvider() 
		{
			return MyContainer.Instance.Resolve<ISettingsProvider>();
		}
	}
}
