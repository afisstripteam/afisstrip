﻿using AfisStrip.Libs.ObservableObjects;
using AfisStrip.Model.Tables;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows.Forms;
using System.Windows.Input;
using AfisStrip.Model.DataService.Interface;
using AfisStrip.Model.DataService.Providers;
using AfisStrip.ViewModel.Interfaces;

namespace AfisStrip.ViewModel
{
	public class EndOfDayViewModel : IEndOfDayViewModel
	{
		private readonly IFlightBookDataService _flightBookDataService;
		private readonly IPlaneUsageRecordsDataService _planeUsageRecordsDataSrvice;
		private ITimekeeperBlockDataService _timekeeperBlockDataService;
		private IMenuStateProvider _menuStateProvider;

		public DateTime Today
		{
			get
			{
				return DateTime.Today;
			}
		}

		public string FlightDispatcher
		{
			get
			{
				return this._menuStateProvider.FlightDispatcher;
			}
		}

		public ObservableCollection<FlightBook> FlightBook
		{
			get
			{
				return this._flightBookDataService.FlightBook;
			}

		}

		public ObservableCollection<TimekeeperBlock> Smirak
		{
			get
			{
				return this._timekeeperBlockDataService.TimekeeperBlock;
			}

		}

		public ObservableCollection<PlaneToTraffic> PlanesToTraffic
		{
			get
			{
				throw new NotImplementedException();
				//return this._planeUsageRecordsDataSrvice.Planes;	// <-- zakomentovane 15.3.2018
			}

		}

		public BaseFont TahomaPath
		{
			get
			{
				var font = Environment.GetEnvironmentVariable("SystemRoot") + "\\fonts\\tahoma.ttf";
				return BaseFont.CreateFont(font, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			}
		}

		public ICommand PrintFlightBook
		{
			get
			{
				return new RelayCommand((parameter) =>
				{

					SaveFileDialog dlg = new SaveFileDialog();
					dlg.Filter = "Pdf Files|*.pdf";
					dlg.FilterIndex = 0;
					dlg.FileName = "kniha-" + DateTime.Today.ToString("yyyy-MM-dd");

					string fileName = string.Empty;

					if(dlg.ShowDialog() == DialogResult.OK)
					{
						fileName = dlg.FileName;

						//Create new PDF document
						Document document = new Document(PageSize.A4, 10f, 10f, 10f, 10f);

						try
						{
							PdfWriter.GetInstance(document, new FileStream(fileName, FileMode.Create));

							Paragraph title = new Paragraph("Kniha příletů a odletů", new Font(TahomaPath, 15));
							title.Alignment = 1;
							title.SpacingAfter = 5;

							Paragraph date = new Paragraph(Today.ToShortDateString(), new Font(TahomaPath, 13));
							title.Alignment = 0;
							title.SpacingAfter = 5;

							Paragraph dispather = new Paragraph("Dispečer: " + this.FlightDispatcher, new Font(TahomaPath, 13));
							title.Alignment = 2;
							title.SpacingAfter = 10;

							PdfPTable table = new PdfPTable(5);
							table.HorizontalAlignment = 1;
							table.SpacingBefore = 10;
							table.WidthPercentage = 100;

							var font = new Font(TahomaPath, 12);

							table.AddCell(new Phrase("Volací znak", font));
							table.AddCell(new Phrase("Akce", font));
							table.AddCell(new Phrase("Čas", font));
							table.AddCell(new Phrase("Poloha", font));
							table.AddCell(new Phrase("Poznámky", font));

							foreach(var plane in this.FlightBook)
							{
								table.AddCell(new Phrase(plane.CallSign, font));
								table.AddCell(new Phrase(plane.Action, font));
								table.AddCell(new Phrase(plane.Time.ToString("HH:mm"), font));
								table.AddCell(new Phrase(plane.Position, font));
								table.AddCell(new Phrase(plane.Notes, font));
							}

							document.Open();
							document.Add(title);
							document.Add(date);
							document.Add(dispather);
							document.Add(table);
						} catch(Exception)
						{
						} finally
						{
							document.Close();
						}
					}
				});
			}
		}

		public ICommand PrintSmirak
		{
			get
			{
				return new RelayCommand((parameter) =>
				{

					SaveFileDialog dlg = new SaveFileDialog();
					dlg.Filter = "Pdf Files|*.pdf";
					dlg.FilterIndex = 0;
					dlg.FileName = "smirak-" + DateTime.Today.ToString("yyyy-MM-dd");

					string fileName = string.Empty;

					if(dlg.ShowDialog() == DialogResult.OK)
					{
						fileName = dlg.FileName;

						//Create new PDF document
						Document document = new Document(PageSize.A4, 10f, 10f, 10f, 10f);

						try
						{
							PdfWriter.GetInstance(document, new FileStream(fileName, FileMode.Create));

							Paragraph title = new Paragraph("Šmírák", new Font(TahomaPath, 15));
							title.Alignment = 1;
							title.SpacingAfter = 5;

							Paragraph date = new Paragraph(Today.ToShortDateString(), new Font(TahomaPath, 13));
							title.Alignment = 0;
							title.SpacingAfter = 5;

							Paragraph dispather = new Paragraph("Dispečer: " + this.FlightDispatcher, new Font(TahomaPath, 13));
							title.Alignment = 2;
							title.SpacingAfter = 10;

							PdfPTable table = new PdfPTable(7);
							table.HorizontalAlignment = 1;
							table.SpacingBefore = 10;
							table.WidthPercentage = 100;

							var font = new Font(TahomaPath, 10);

							table.AddCell(new Phrase("Volací znak", font));
							table.AddCell(new Phrase("Typ", font));
							table.AddCell(new Phrase("Posádka", font));
							table.AddCell(new Phrase("Úkol", font));
							table.AddCell(new Phrase("Vzlet", font));
							table.AddCell(new Phrase("Přistání", font));
							table.AddCell(new Phrase("Doba letu", font));

							foreach(var plane in this.Smirak)
							{
								table.AddCell(new Phrase(plane.CallSign, font));
								table.AddCell(new Phrase(plane.Type, font));
								table.AddCell(new Phrase(plane.Crew, font));
								table.AddCell(new Phrase(plane.Objective, font));
								table.AddCell(new Phrase(plane.TakeOff.ToString("HH:mm"), font));
								if(plane.TakeOff != null && plane.Landing != null && plane.Landing != DateTime.MinValue)
								{
									table.AddCell(new Phrase(plane.Landing.ToString("HH:mm"), font));
									table.AddCell(new Phrase(string.Format("{0} min", Math.Ceiling(plane.Landing.Subtract((DateTime)plane.TakeOff).TotalMinutes)), font));
								} else
								{
									table.AddCell(new Phrase(""));
									table.AddCell(new Phrase(""));
								}
							}

							document.Open();
							document.Add(title);
							document.Add(date);
							document.Add(dispather);
							document.Add(table);
						} catch(Exception)
						{
						} finally
						{
							document.Close();
						}
					}
				});
			}
		}

		public EndOfDayViewModel(IFlightBookDataService flightBookDataService,
			IPlaneUsageRecordsDataService planeUsageRecordsDataService,
			ITimekeeperBlockDataService timekeeperBlockDataService,
			IMenuStateProvider menuStateProvider)
		{
			this._flightBookDataService = flightBookDataService;
			this._planeUsageRecordsDataSrvice = planeUsageRecordsDataService;
			this._timekeeperBlockDataService = timekeeperBlockDataService;
			this._menuStateProvider = menuStateProvider;
		}
	}
}
