﻿using AfisStrip.Libs.ObservableObjects;
using AfisStrip.Model.Entities;
using AfisStrip.Model.Tables;
using AfisStrip.ViewModel.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using AfisStrip.Model.DataService;
using AfisStrip.Model.DataService.Interface;
using AfisStrip.Model.Enums;
using AfisStrip.ViewModel.Wrappers;
using Nito.AsyncEx;

namespace AfisStrip.ViewModel
{
	public class StripsViewModel : IPageViewModel, IStripsViewModel
	{
		private const int FLASHING_PAUSE_SECONDS = 3;
		
		public ObservableCollection<StripWrapper> StripsCollection { get; set; }
		public ObservableCollection<StripWrapper> StripsOutOfAtzCollection { get; set; }
		
		public StripNG NewStrip { get; set; }

		public Plane SelectedTow
		{
			get
			{
				return this._stripsDataService.SelectedTow;
			}
		}

		public IEnumerable<PlaneUsageRecord> TowsCollection
		{
			get
			{
				var tempList = new List<PlaneUsageRecord>();
				tempList.Add(new PlaneUsageRecord(new Airplane {Registration = "OK-TEST"}));
				tempList.Add(new PlaneUsageRecord(new Airplane {Registration = "OK-TEST2"}));

				return tempList;
			}
		}

		public ICommand AddNewStrip
		{
			get
			{
				return new RelayCommand(async (parameter) =>
				{
					if (!string.IsNullOrEmpty(this.NewStrip.PlaneAndCrewAndUsage.Plane.Registration)) {

						var linkedPlane = await AirplanesDataService.getInstance().GetAirplaneAsync(this.NewStrip.PlaneAndCrewAndUsage.Plane.Registration);
						var newStrip = this.NewStrip;

						if (linkedPlane == null) {
							var newAirplaneRecord = new Airplane {Registration = this.NewStrip.PlaneAndCrewAndUsage.Plane.Registration};
							newStrip.PlaneAndCrewAndUsage = new PlaneUsageRecord(newAirplaneRecord);
							newStrip.RecordLastInformationChangedTime(DateTime.Now);
							newStrip.RecordRadioContact(DateTime.Now);
						}
						//TODO: Insert do databaze

						var stripWrapped = new StripWrapper(newStrip);

						this.NewStrip = this.createEmptyStrip();

						this.StripsCollection.Insert(0, stripWrapped);
						this.OnPropertyChanged(nameof(this.NumberOfPlanes));
						this.OnPropertyChanged(nameof(this.TowsCollection));
						this.OnPropertyChanged(nameof(this.NewStrip));
					}
				});
			}
		}

		private readonly ITowsDataService _towsDataService;
		private readonly IStripsDataService _stripsDataService;
		private readonly ITimeDispatcherProvider _timeDispatcherProvider;
		private readonly IMenuStateProvider _menuStateProvider;

		public StripWrapper ListSelectedItem { get; set; }
		public StripWrapper OutATZGridSelectedItem { get; set; }
		public INotifyTaskCompletion<List<string>> CallSigns { get; set; }

		public int NumberOfPlanes
		{
			get
			{
				return this.StripsCollection.Where(x => x.Strip.Category == FlightCategory.LOCAL_FLIGHT).ToList().Count; //TODO: Jake kategorie?
			}
		}

		public ICommand DeleteStrip
		{
			get
			{
				return new RelayCommand((parameter) =>
				{
					if(MessageBox.Show("Opravdu smazat?", "Smazat", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
					{
						var param = (StripWrapper)parameter;
						//param.delete(); //TODO: Smazání stripu z DB
						this.StripsCollection.Remove(param);

						this.OnPropertyChanged(nameof(this.StripsCollection));
						this.OnPropertyChanged(nameof(this.NumberOfPlanes));
					}
				});
			}
		}

		public StripsViewModel(ITowsDataService towsDataService,
			IStripsDataService stripsDataService,
			ITimeDispatcherProvider timeDispatcherProvider,
			IMenuStateProvider menuStateProvider,
			IDBProvider dbProvider)
		{
			this._towsDataService = towsDataService;
			this._stripsDataService = stripsDataService;
			this._timeDispatcherProvider = timeDispatcherProvider;
			this._menuStateProvider = menuStateProvider;

			this.NewStrip = this.createEmptyStrip();

			//TODO: nasledujici vyhodit, neodhackovava se to
			try
			{
				this.StripsCollection = this._stripsDataService.InATZ;
				this.StripsCollection.CollectionChanged += this.ATZCollectionUpdated;

				this.StripsOutOfAtzCollection = this._stripsDataService.OutOfATZ;
				this.StripsOutOfAtzCollection.CollectionChanged += this.OutOfATZCollectionUpdated;

			} catch(Exception ex)
			{
				Debug.WriteLine(ex);
			}
			
			this._timeDispatcherProvider.TimeTrigger.Tick += this.TimeTimerCallBack;
			this._menuStateProvider.PropertyChanged += this.OnMenuChanged;

			this.CallSigns = NotifyTaskCompletion.Create(this.LoadAllCallSigns);
		}


		/// <summary>
		/// Nacte vsechny zname letadla jako dvojici (volaci znak, letadlo)
		/// </summary>
		private async Task<List<string>> LoadAllCallSigns() 
		{
			var allPlanes = await AirplanesDataService.getInstance().ListAllKnownAirplanesDictionaryAsync();
			return allPlanes.Keys.ToList();
		}

		/// <summary>
		/// Tento ticker se jednou za cast vzbudi a zkontroluje zda nektery ze stripu nepresel do stavu 
		/// SAFETY_CHECK nebo INCERFA.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void TimeTimerCallBack(object sender, EventArgs e)
		{
			var seconds = (int)DateTime.Now.Second;
			if (seconds % FLASHING_PAUSE_SECONDS != 0) {
				return;
			}

			var anySafetyCheck = false;
			foreach (var stripWrapper in this.StripsCollection) {
				stripWrapper.UpdateStripCheckStatus();
				if (stripWrapper.StripCheckStatus == StripCheckStatusEnum.SAFETY_CHECK) {
					anySafetyCheck = true;
				}

				if (stripWrapper.StripCheckStatus == StripCheckStatusEnum.SAFETY_CHECK || stripWrapper.StripCheckStatus == StripCheckStatusEnum.INCERFA) {
					stripWrapper.RaisePropertyChanged(nameof(StripWrapper.StripCheckStatus));
				}
				stripWrapper.RaisePropertyChanged(nameof(StripWrapper.Strip));
			}
			
			if (anySafetyCheck && seconds % FLASHING_PAUSE_SECONDS * 2 == 0) {
				var mediaPlayer = new MediaPlayer();
				mediaPlayer.Open(new Uri(Environment.CurrentDirectory + @"\alert.wav"));
				mediaPlayer.Play();
			}
		}

		private void ATZCollectionUpdated(object sender, NotifyCollectionChangedEventArgs e)
		{
			var senderEnumerable = sender as IEnumerable<StripWrapper>;

			if(e.Action == NotifyCollectionChangedAction.Add)
			{
				//var strip = senderEnumerable.First();

			} else if(e.Action == NotifyCollectionChangedAction.Remove)
			{
			}

			this.OnPropertyChanged(nameof(this.StripsCollection));
			this.OnPropertyChanged(nameof(this.NumberOfPlanes));

			return;
		}

		private void OutOfATZCollectionUpdated(object sender, NotifyCollectionChangedEventArgs e)
		{
			var senderEnumerable = sender as IEnumerable<StripWrapper>;

			if(e.Action == NotifyCollectionChangedAction.Add)
			{
				var strip = senderEnumerable.First();
				//strip.ATZLeft();//

			} else if(e.Action == NotifyCollectionChangedAction.Remove)
			{
			}

			this.OnPropertyChanged(nameof(this.StripsOutOfAtzCollection));
		}

		private void OnMenuChanged(object sender, PropertyChangedEventArgs e)
		{
			this.OnPropertyChanged(e.PropertyName);

			if(e.PropertyName == "FilenameExport")
			{
				var a = this._menuStateProvider;
				this.Export(a.GetType().GetProperty(e.PropertyName).GetValue(a, null).ToString());
			}
		}

		/// <summary>
		/// Tato metoda vytvori a vrati prazdny strip, ktery je pripraveny na vyplneni dat.
		/// </summary>
		/// <returns></returns>
		private StripNG createEmptyStrip()
		{
			var airplanePlaceholder = new Airplane();
			var planeUsagePlaceholder = new PlaneUsageRecord(airplanePlaceholder);
			return new StripNG(planeUsagePlaceholder);
		}

		public void Export(string path)
		{
			using(StreamWriter sw = new StreamWriter(path, false, Encoding.Default))
			{
				sw.WriteLine("sep=,");
				sw.WriteLine($"{DateTime.Now}, dispečer {this._menuStateProvider.FlightDispatcher}");
				sw.WriteLine("Volací znak, Typ, Velitel, Trať / Poloha, Poznámky, Poslední kontakt, Kategorie, Stav");
				foreach(var stripWrapper in this.StripsCollection)
				{
					var strip = stripWrapper.Strip;
					sw.WriteLine(
						$"{strip.PlaneAndCrewAndUsage.Plane},{strip.PlaneAndCrewAndUsage.Plane.Type},{strip.PlaneAndCrewAndUsage.Crew},{strip.Position},{strip.Notes},{strip.LastInformationChangedTime.ToLocalTime()},{strip.Category},{strip.Status}");
				}

				sw.WriteLine("");
				sw.WriteLine("\"Letadla, která opustila ATZ\"");
				sw.WriteLine("");
				foreach(var stripWrapper in this.StripsOutOfAtzCollection)
				{
					var strip = stripWrapper.Strip;
					sw.WriteLine(
						$"{strip.PlaneAndCrewAndUsage.Plane},{strip.PlaneAndCrewAndUsage.Plane.Type},{strip.PlaneAndCrewAndUsage.Crew},{strip.Position},{strip.Notes},{strip.LastInformationChangedTime.ToLocalTime()},{strip.Category},{strip.Status}");
				}
			}
		}
	}

	class NotesDatagridComboBox : List<string>
	{
		public NotesDatagridComboBox()
		{
			this.Add("VLEČNÁ");
			this.Add("VÝSADKY");
			this.Add("NAVIJÁK");
			this.Add("POZORNOST");
		}
	}
}
