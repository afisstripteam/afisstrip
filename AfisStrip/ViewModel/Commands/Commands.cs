﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AfisStrip.ViewModel.Commands
{
    public static class Commands
    {
        #region "GRID DOUBLE CLICK COMMAND"
        public static readonly DependencyProperty GridDoubleClickProperty =
          DependencyProperty.RegisterAttached("GridDoubleClickCommand", typeof(ICommand), typeof(Commands),
                            new PropertyMetadata(new PropertyChangedCallback(AttachOrRemoveGridDoubleClickEvent)));

        public static ICommand GetGridDoubleClickCommand(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(GridDoubleClickProperty);
        }

        public static void SetGridDoubleClickCommand(DependencyObject obj, ICommand value)
        {
            obj.SetValue(GridDoubleClickProperty, value);
        }

        public static void AttachOrRemoveGridDoubleClickEvent(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            Grid grid = obj as Grid;
            if(grid != null)
            {
                ICommand cmd = (ICommand)args.NewValue;

                if(args.OldValue == null && args.NewValue != null)
                {
                    grid.MouseDown += ExecuteGridDoubleClick;
                } else if(args.OldValue != null && args.NewValue == null)
                {
                    grid.MouseDown -= ExecuteGridDoubleClick;
                }
            }
        }

        private static void ExecuteGridDoubleClick(object sender, MouseButtonEventArgs args)
        {
            if(args.ClickCount > 0 && args.ClickCount % 2 == 0)
            {
                DependencyObject obj = sender as DependencyObject;
                ICommand cmd = (ICommand)obj.GetValue(GridDoubleClickProperty);
                if(cmd != null)
                {
                    if(cmd.CanExecute(obj))
                    {
                        cmd.Execute(obj);
                    }
                }
            }
        }
        #endregion



        #region "Control enter command"
        public static readonly DependencyProperty ControlPressEnterCommandProperty =
            DependencyProperty.RegisterAttached("ControlPressEnterCommand", typeof(ICommand), typeof(Commands),
                            new PropertyMetadata(new PropertyChangedCallback(AttachOrRemoveControlPressEnterEvent)));

        // declare an attached dependency property
        public static ICommand GetControlPressEnterCommand(Control obj)
        {
            return (ICommand)obj.GetValue(ControlPressEnterCommandProperty);
        }

        public static void SetControlPressEnterCommand(Control obj, ICommand value)
        {
            obj.SetValue(ControlPressEnterCommandProperty, value);
        }

        public static void AttachOrRemoveControlPressEnterEvent(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            Control control = obj as Control;
            if(obj != null)
            {
                ICommand cmd = (ICommand)args.NewValue;

                if(args.OldValue == null && args.NewValue != null)
                {
                    control.PreviewKeyDown += Control_KeyDown;
                } else if(args.OldValue != null && args.NewValue == null)
                {
                    control.PreviewKeyDown -= Control_KeyDown;
                }
            }
        }
        private static void Control_KeyDown(object sender, KeyEventArgs e)
        {
            // if user didn't press Enter, do nothing
            if(!e.Key.Equals(Key.Enter))
                return;

            // execute the command, if it exists
            var cmd = GetControlPressEnterCommand((Control)sender);
            if(cmd != null)
                cmd.Execute(null);
        }
        #endregion

    }
}
