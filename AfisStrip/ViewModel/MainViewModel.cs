﻿using AfisStrip.Libs.ObservableObjects;
using AfisStrip.ViewModel.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using AfisStrip.Libs.ExtensionMethods;
using AfisStrip.Model.DataService.Interface;
using AfisStrip.Model.Entities;
using AfisStrip.View;
using ENG.WMOCodes.Codes;
using ENG.WMOCodes.Decoders;
using ENG.WMOCodes.Formatters;
using Innovative.SolarCalculator;
using Microsoft.Practices.Unity;

namespace AfisStrip.ViewModel
{
	public class MainViewModel : IPageViewModel, IMainViewModel
	{
		private UserControl _currentView;
		private DispatcherTimer _metarTimer;
		private Metar _metar;
		private SolarTimes sunCalculator;


		private DateTime todaySunSet
		{
			get
			{
				if (this.sunCalculator == null)
				{
					return DateTime.MinValue;
				}

				return this.sunCalculator.Sunset.ToLocalTime();
			}
		}

		public ICommand SwitchViewsCommand { get; private set; }

		public ICommand EndDay
		{
			get
			{
				return new RelayCommand((parameter) =>
				{
					var message = string.Empty;
					if(this._stripsDataService.OutOfATZ.Any(x => x.Strip.PlaneAndCrewAndUsage?.Plane?.IsHomebasedAtThisAirport ?? false))
					{
						message = "Mimo ATZ se stále nachází letadlo zapsané do provozu, opravdu pokračovat?";

					} else
					{
						message = "Opravdu ukončit?";
					}

					var mb = MessageBox.Show(message, "Pokračovat?", MessageBoxButton.OKCancel);

					if(mb == MessageBoxResult.OK)
					{
						var changeDialog = this._container.Resolve<EndOfDay>();
						changeDialog.ShowDialog();
						if(changeDialog.DialogResult == true)
						{
							Environment.Exit(0);
						} else
						{
							changeDialog.Close();
						}
					}
				});
			}
		}

		private ICommand _showMetarInfoCommand;
		public ICommand ShowMetarInfoCommand
		{
			get
			{
				if (this._showMetarInfoCommand == null)
				{
					this._showMetarInfoCommand = new RelayCommand((parameter) =>
					{
						var message = string.Empty;
						if (this._metar == null)
						{
							message = "Zatím nejsou k dispozici žádná METAR data.";
						} else
						{
							message = "Originální METAR:\n" + this._metar.ToCode();

							var metarFormatter = new ENG.WMOCodes.Formatters.ShortInfoFormatter.MetarFormatter(CultureInfo.GetCultureInfo("cs-cz"));
							message += "\n\nDekódovaný METAR:\n" + metarFormatter.ToString(this._metar);

							var localQnh = this._metar.ParseLocalQnh();
							if (!string.IsNullOrWhiteSpace(localQnh)) {
								message += $"Regionální QNH: {localQnh}";
							}
						}

						MessageBox.Show(message, "Metar info", MessageBoxButton.OK);
					});
				}

				return this._showMetarInfoCommand;
			}
		}

		public UserControl CurrentView
		{
			get
			{
				return this._currentView;
			}
			set
			{
				if(value != this._currentView)
				{
					this._currentView = value;
					this.OnPropertyChanged(nameof(this.CurrentView));
				}
			}
		}

		public string FlightDispatcher
		{
			get
			{
				return "Dispečer: " + this._menuStateProvider.FlightDispatcher;
			}
		}

		public string ActualTime
		{
			get
			{
				return DateTime.Now.ToString(this._settingsProvider.AppSettings.TimeFormat);
			}
		}

		public string ActualUTCTime
		{
			get
			{
				return DateTime.UtcNow.ToString(this._settingsProvider.AppSettings.TimeFormat);
			}
		}

		public string LastMeteoUpdated { get; set; }

		public string Visibility
		{
			get { return this._metar?.Visibility.ToCode(); }
		}

		public string Wind
		{
			get
			{
				if (this._metar == null)
				{
					return string.Empty;
				}

				var formatter = new ENG.WMOCodes.Formatters.ShortInfoFormatter.MetarFormatter(CultureInfo.GetCultureInfo("cs-cz"));
				return formatter.GetWindShortString(this._metar.Wind);
			}
		}

		public string QNH
		{
			get
			{
				var localQnh = this._metar?.ParseLocalQnh();
				if (string.IsNullOrEmpty(localQnh)) {
					localQnh = "-";
				}
				return this._metar?.Pressure.QNH + " / " + localQnh;
			}
		}

		public string SunSet
		{
			get
			{
				
				return this.todaySunSet.ToString(this._settingsProvider.AppSettings.TimeFormat) + " " + TimeZoneInfo.Local.Abbreviation();
			}
		}

		public string TimeToSunSet
		{
			get
			{
				var timeNow = DateTime.Now;
				if(timeNow <= this.todaySunSet)
				{
					var result = this.todaySunSet - DateTime.Now;
					return new TimeSpan(result.Hours, result.Minutes, 0).ToString(@"hh\:mm");
				} else
				{
					return new DateTime(timeNow.Year, timeNow.Month, timeNow.Day, 0, 0, 0).ToString(this._settingsProvider.AppSettings.TimeFormat);
				}
			}
		}


		public string ContactMessage
		{
			get
			{
				StringBuilder contactMessage = new StringBuilder();
				var menuProvider = this._menuStateProvider;

				contactMessage.Append("OK-XXX, ");

				var hasTraffic = true;
				var traffic = string.Empty;
				if (menuProvider.IsTowInUse && menuProvider.IsWinchInUse)
				{
					traffic = "aerovlekový i navijákový";
				}
				else if (menuProvider.IsTowInUse)
				{
					traffic = "aerovlekový";
				}
				else if (menuProvider.IsWinchInUse)
				{
					traffic = "navijákový";
				}
				else
				{
					hasTraffic = false;
				}

				if (hasTraffic)
				{
					contactMessage.Append("na letišti " + traffic + " provoz kluzáků");
				}
				else
				{
					contactMessage.Append("letiště bez provozu");
				}

				contactMessage.Append(", dráha v užívání " + menuProvider.TrackSelectedItem + ", okruhy ");

				if (menuProvider.TrackSelectedItem.Equals("16"))
				{
					contactMessage.Append("pravé");
				}
				else
				{
					contactMessage.Append("levé");
				}

				contactMessage.Append(", pokračujte (kam), oznamte (kde)");

				return contactMessage.ToString();
			}
		}

		public int NumberOfPlanes
		{
			get
			{
				return this._stripsDataService.InATZ.Count; //TODO: Jake kategorie?
			}
		}


		/** MENU combobox selection **/
		private bool _isWinchInUse = false;
		public bool IsWinchInUse
		{
			get { return this._isWinchInUse; }
			set
			{
				this._isWinchInUse = value;
				this._menuStateProvider.IsWinchInUse = value;
				this.OnPropertyChanged(nameof(this.IsWinchInUse));
			}
		}

		private bool _isTowInUse = false;
		public bool IsTowInUse
		{
			get { return this._isTowInUse; }
			set
			{
				this._isTowInUse = value;
				this._menuStateProvider.IsTowInUse = value;
				this.OnPropertyChanged(nameof(this.IsTowInUse));
			}
		}

		List<string> _trackNumbersSource = new List<string> { "16", "34" };
		public List<string> TrackNumberSource { get { return this._trackNumbersSource; } }

		string _trackSelectedItem = null;
		public string TrackSelectedItem
		{
			get { return this._trackSelectedItem; }
			set
			{
				this._trackSelectedItem = value;
				this._menuStateProvider.TrackSelectedItem = value;
				this.OnPropertyChanged(nameof(this.IsWinchInUse));
			}
		}

		public List<string> FlightLevelSource { get { return this._menuStateProvider.FlightLevelSource; } }
		string _flightLevelSelectedItem = null;
		private IUnityContainer _container;
		private IStripsDataService _stripsDataService;
		private ITimeDispatcherProvider _timeDispatcherProvider;
		private ISettingsProvider _settingsProvider;
		private IMenuStateProvider _menuStateProvider;

		public string FlightLevelSelectedItem
		{
			get { return this._flightLevelSelectedItem; }
			set
			{
				this._flightLevelSelectedItem = value;
				this._menuStateProvider.FlightLevelSelectedItem = value;
				this.OnPropertyChanged(nameof(this.FlightLevelSelectedItem));
			}
		}

		public ICommand ChangeDispatcher
		{
			get
			{
				return new RelayCommand((parameter) =>
				{
					var changeDialog = new ChangeDispatcher();
					changeDialog.ShowDialog();
					if(changeDialog.DialogResult == true)
					{
						this._menuStateProvider.FlightDispatcher = changeDialog.FlightDispatcher;
						this.OnPropertyChanged(nameof(this.FlightDispatcher));
					} else
					{
						changeDialog.Close();
					}
				});
			}
		}


		public ICommand Export
		{
			get
			{
				return this._menuStateProvider.Export;
			}
		}

		public void ShowStartDispatcherPrompt()
		{
			//var startDialog = new StartPrompt(); // TODO: Odkomentovat zatim pouze pro testovani zakomentovano -> protoze je to otravne
			//startDialog.ShowDialog();
			//if(startDialog.DialogResult == true)
			//{
			//	this._menuStateProvider.FlightDispatcher = startDialog.FlightDispatcher;
			//	this.OnPropertyChanged(nameof(this.FlightDispatcher));
			//}
			//else
			//{
			//	Environment.Exit(1);
			//}
		}

		public MainViewModel(IUnityContainer container, 
			IStripsDataService stripsDataService, 
			ITimeDispatcherProvider timeDispatcherProvider, 
			ISettingsProvider settingsProvider,
			IMenuStateProvider menuStateProvider)
		{
			this._stripsDataService = stripsDataService;
			this._timeDispatcherProvider = timeDispatcherProvider;
			this._container = container;
			this._settingsProvider = settingsProvider;
			this._menuStateProvider = menuStateProvider;

			this.TrackSelectedItem = this._trackNumbersSource[0];
			this.FlightLevelSelectedItem = this.FlightLevelSource[0];

			this.SwitchViewsCommand = new RelayCommand(
				(parameter) =>
				{
					this.CurrentView = (UserControl)this._container.Resolve(parameter as Type);
				});
			this.SwitchViewsCommand.Execute(typeof(Strips));

			this.sunCalculator = this.InitSunCalculator();

			this._timeDispatcherProvider.TimeTrigger.Tick += this.TimeTimerCallBack;

			this._metarTimer = new DispatcherTimer();
			this._metarTimer.Tick += this.MetarTimerCallBack;
			this._metarTimer.Start();
			this.MetarTimerCallBack(null, null);
			this._menuStateProvider.PropertyChanged += this.OnMenuChanged;
			this._stripsDataService.InATZ.CollectionChanged += InAtzOnCollectionChanged;
		}

		private void InAtzOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
		{
			this.OnPropertyChanged(nameof(this.NumberOfPlanes));
		}

		private void OnMenuChanged(object sender, PropertyChangedEventArgs e)
		{
			this.OnPropertyChanged(e.PropertyName);
		}

		private SolarTimes InitSunCalculator()
		{
			return new SolarTimes(
				DateTime.Now.ToUniversalTime(),
				this._settingsProvider.AppSettings.AirportLatitude,
				this._settingsProvider.AppSettings.AirportLongtitude
			);
		}

		private void TimeTimerCallBack(object sender, EventArgs e)
		{
			this.OnPropertyChanged(nameof(this.ActualTime));
			this.OnPropertyChanged(nameof(this.ActualUTCTime));
			this.OnPropertyChanged(nameof(this.TimeToSunSet));

			if(DateTime.Now.Hour == 0 && DateTime.Now.Minute == 0)
			{
				this.sunCalculator = this.InitSunCalculator();
				this.OnPropertyChanged(nameof(this.SunSet));
			}
		}

		private void MetarTimerCallBack(object sender, EventArgs e)
		{
			string htmlCode;
			try
			{
				using(var client = new WebClient())
				{
					htmlCode = client.DownloadString(this._settingsProvider.AppSettings.MetarUrl);
				}
				this._metarTimer.Interval = TimeSpan.FromMinutes(5.0);
			} catch
			{
				this._metarTimer.Interval = TimeSpan.FromSeconds(30.0);
				this.LastMeteoUpdated = "Chyba s připojením k internetu, další pokus proběhne do 30 sekund";
				this.OnPropertyChanged(nameof(this.LastMeteoUpdated));
				return;
			}

			var isOk = false;
			var metarMatch = Regex.Match(htmlCode, @".*?(METAR.*?=).*?", RegexOptions.IgnoreCase);
			if (metarMatch.Success)
			{
				var metarString = metarMatch.Groups[1].Value;
				this._metar = new MetarDecoder().Decode(metarString);
				isOk = true;
			}

			var toolTipMessage = string.Empty;

			if(!isOk)
			{
				this._metarTimer.Interval = TimeSpan.FromSeconds(20.0);
				toolTipMessage = "Poslední aktualizace nebyla úspěšná, další proběhne do 20 sekund";
			} else if(this._metarTimer.Interval != TimeSpan.FromMinutes(5.0))
			{
				this._metarTimer.Interval = TimeSpan.FromMinutes(5.0);
			} else
			{
				toolTipMessage = string.Format("Poslední aktualizace {0:HH:mm} / další v {1:HH:mm}", DateTime.Now, DateTime.Now + this._metarTimer.Interval);
			}

			this.LastMeteoUpdated = toolTipMessage;

			this.OnPropertyChanged(nameof(this.LastMeteoUpdated));
			this.OnPropertyChanged(nameof(this.Visibility));
			this.OnPropertyChanged(nameof(this.QNH));
		}
	}
}
