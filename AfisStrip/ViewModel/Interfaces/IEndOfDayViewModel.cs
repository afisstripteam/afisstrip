﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using AfisStrip.Model.Tables;
using iTextSharp.text.pdf;

namespace AfisStrip.ViewModel.Interfaces
{
	public interface IEndOfDayViewModel
	{
		ObservableCollection<FlightBook> FlightBook { get; }
		string FlightDispatcher { get; }
		ObservableCollection<PlaneToTraffic> PlanesToTraffic { get; }
		ICommand PrintFlightBook { get; }
		ICommand PrintSmirak { get; }
		ObservableCollection<TimekeeperBlock> Smirak { get; }
		BaseFont TahomaPath { get; }
		DateTime Today { get; }
	}
}