﻿using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Input;

namespace AfisStrip.ViewModel.Interfaces
{
	public interface IMainViewModel
	{
		string ActualTime { get; }
		string ActualUTCTime { get; }
		ICommand ChangeDispatcher { get; }
		UserControl CurrentView { get; set; }
		ICommand EndDay { get; }
		ICommand Export { get; }
		void ShowStartDispatcherPrompt();
		string FlightDispatcher { get; }
		string FlightLevelSelectedItem { get; set; }
		List<string> FlightLevelSource { get; }
		string LastMeteoUpdated { get; set; }
		string QNH { get; }
		string SunSet { get; }
		ICommand SwitchViewsCommand { get; }
		string TimeToSunSet { get; }
		bool IsTowInUse { get; set; }
		List<string> TrackNumberSource { get; }
		string TrackSelectedItem { get; set; }
		string Visibility { get; }
		string Wind { get; }
		bool IsWinchInUse { get; set; }
	}
}