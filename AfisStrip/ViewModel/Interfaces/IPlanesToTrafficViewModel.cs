﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using AfisStrip.Model.Tables;

namespace AfisStrip.ViewModel.Interfaces
{
	public interface IPlanesToTrafficViewModel
	{
		ICommand AddNewPlane { get; }

		/*
		 -- zakomentovane nepotrebne veci 15.3.2018 -- pri prerabke ViewModel-u. 
		ICommand AddTow { get; }
		List<string> CallSigns { get; }
		string NewPlaneCallSign { get; set; }
		string NewPlaneCrew { get; set; }
		string NewPlaneObjective { get; set; }
		string NewTowCallSign { get; set; }
		string NewTowCaptain { get; set; }
		string NewTowType { get; set; }
		ObservableCollection<PlaneToTraffic> PlanesToTrafficCollection { get; set; }
		ObservableCollection<PlaneTow> TowsCollection { get; set; }
		*/

		//void Export(string path);	// <-- k comu?! 15.3.2018 zakomentovane, ak sa nenajde dovod, odstranit. 
		//void insertToStrips(string callSign, bool isTow = false); // <-- k comu?! 15.3.2018 zakomentovane, ak sa nenajde dovod, odstranit. 
	}
}