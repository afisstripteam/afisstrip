﻿using System;
using AfisStrip.Libs.ObservableObjects;
using AfisStrip.Model.Tables;

namespace AfisStrip.ViewModel.Interfaces
{
	public interface ITimekeeperBlockViewModel
	{
		string ActionFilter { get; set; }
		string CallSignFilter { get; set; }
		DateTime DateFrom { get; set; }
		DateTime DateTo { get; set; }
		TimekeeperBlock GridSelectedItem { get; set; }
		TrulyObservableCollection<TimekeeperBlock> TimekeeperBlockCollection { get; set; }

		void Export(string path);
	}
}