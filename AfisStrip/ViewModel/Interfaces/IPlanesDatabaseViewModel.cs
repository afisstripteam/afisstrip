﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using AfisStrip.Model.Entities;

namespace AfisStrip.ViewModel.Interfaces
{
	public interface IPlanesDatabaseViewModel
	{
		ICommand AddNewPlane { get; }
		string CallSignFilter { get; set; }
		ICommand DeleteRecord { get; }
		Airplane GridSelectedItem { get; set; }
		bool IsHomebasedAtThisAirportFilter { get; set; }
		string NewPlaneCallSign { get; set; }
		bool NewPlaneIsHomebasedAtThisAirport { get; set; }
		string NewPlaneOwner { get; set; }
		string NewPlaneType { get; set; }
		string OwnerFilter { get; set; }
		ObservableCollection<Airplane> PlanesDatabaseCollection { get; set; }
		string TypeFilter { get; set; }
		ICommand UpdateSelectedItem { get; }

		void Export(string path);
		void fillPlanesDatabaseData();
	}
}