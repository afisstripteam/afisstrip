﻿using System;
using System.Collections.ObjectModel;
using AfisStrip.Model.Tables;

namespace AfisStrip.ViewModel.Interfaces
{
	public interface IFlightBookViewModel
	{
		string ActionFilter { get; set; }
		string CallSignFilter { get; set; }
		DateTime DateFrom { get; set; }
		DateTime DateTo { get; set; }
		ObservableCollection<FlightBook> FlightBookCollection { get; set; }

		void Export(string path);
		void fillFlightBookData();
	}
}