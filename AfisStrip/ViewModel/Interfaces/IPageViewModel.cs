﻿using AfisStrip.Libs.ObservableObjects;

namespace AfisStrip.ViewModel.Interfaces
{
    public abstract class IPageViewModel : ObservableObject
    {
        public string Title { get; set; }
    }
}
