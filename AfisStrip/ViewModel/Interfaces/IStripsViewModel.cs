﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using AfisStrip.Model.Entities;
using AfisStrip.Model.Tables;
using AfisStrip.ViewModel.Wrappers;
using Nito.AsyncEx;

namespace AfisStrip.ViewModel.Interfaces
{
	public interface IStripsViewModel
	{
		ICommand AddNewStrip { get; }
		INotifyTaskCompletion<List<string>> CallSigns { get; }
		ICommand DeleteStrip { get; }
		StripWrapper ListSelectedItem { get; set; }
		int NumberOfPlanes { get; }
		StripWrapper OutATZGridSelectedItem { get; set; }
		ObservableCollection<StripWrapper> StripsCollection { get; set; }
		ObservableCollection<StripWrapper> StripsOutOfAtzCollection { get; set; }
		IEnumerable<PlaneUsageRecord> TowsCollection { get; }
		Plane SelectedTow { get; }

		void Export(string path);
	}
}