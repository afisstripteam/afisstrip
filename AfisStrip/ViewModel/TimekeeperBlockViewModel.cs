﻿using AfisStrip.Libs.ObservableObjects;
using AfisStrip.Model.Tables;
using AfisStrip.ViewModel.Interfaces;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using AfisStrip.Model.DataService.Interface;

namespace AfisStrip.ViewModel
{
	public class TimekeeperBlockViewModel : IPageViewModel, ITimekeeperBlockViewModel
	{
		public TrulyObservableCollection<TimekeeperBlock> TimekeeperBlockCollection { get; set; }

		private DateTime _dateTo;
		private DateTime _dateFrom;
		private string _callSignFilter;
		private string _actionFilter;
		private ITimekeeperBlockDataService _timekeeperBlockDataService;
		private IMenuStateProvider _menuStateProvider;

		public DateTime DateTo
		{
			get
			{
				return _dateTo;
			}
			set
			{
				_dateTo = value;
				if(value < _dateFrom)
				{
					DateFrom = value;
				}
				OnPropertyChanged(nameof(DateTo));
				timekeeperBlockData();
			}
		}

		public DateTime DateFrom
		{
			get
			{
				return _dateFrom;
			}
			set
			{
				_dateFrom = value;
				OnPropertyChanged(nameof(DateFrom));
				timekeeperBlockData();
			}
		}
		public string CallSignFilter
		{
			get
			{
				return _callSignFilter;
			}
			set
			{
				_callSignFilter = value;

				OnPropertyChanged(nameof(CallSignFilter));
				timekeeperBlockData();
			}
		}

		public string ActionFilter
		{
			get
			{
				return _actionFilter;
			}
			set
			{
				_actionFilter = value;

				OnPropertyChanged(nameof(ActionFilter));
				timekeeperBlockData();
			}
		}

		public TimekeeperBlock GridSelectedItem { get; set; }

		public TimekeeperBlockViewModel(ITimekeeperBlockDataService timekeeperBlockDataService,
			IMenuStateProvider menuStateProvider)
		{
			this._timekeeperBlockDataService = timekeeperBlockDataService;
			this._menuStateProvider = menuStateProvider;

			try
			{
				_dateTo = DateTime.Now;
				_dateFrom = _dateTo;

				timekeeperBlockData();

			} catch(Exception ex)
			{
				Debug.WriteLine(ex);
			}

			this._menuStateProvider.PropertyChanged += OnMenuChanged;
		}

		private void timekeeperBlockData()
		{
			var timekeeperBlockCollection = new TrulyObservableCollection<TimekeeperBlock>(this._timekeeperBlockDataService.TimekeeperBlock
										.Where(
											x =>
												x.TakeOff.Date >= _dateFrom.Date &&
												x.TakeOff.Date <= _dateTo.Date)
										.OrderBy(x => x.Id));

			if(this.CallSignFilter != null && this.CallSignFilter != string.Empty)
			{
				timekeeperBlockCollection = new TrulyObservableCollection<TimekeeperBlock>(timekeeperBlockCollection.Where(p => p.CallSign != null && p.CallSign.ToString().ToLower().Contains(this.CallSignFilter.ToLower())));
			}

			this.TimekeeperBlockCollection = timekeeperBlockCollection;

			OnPropertyChanged(nameof(TimekeeperBlockCollection));
		}

		private void OnMenuChanged(object sender, PropertyChangedEventArgs e)
		{
			if(e.PropertyName == "FilenameExport")
			{
				var a = this._menuStateProvider;
				Export(a.GetType().GetProperty(e.PropertyName).GetValue(a, null).ToString());
			}
		}

		public void Export(string path)
		{
			using(StreamWriter sw = new StreamWriter(path, false, Encoding.Default))
			{
				sw.WriteLine(string.Format("sep=,"));
				sw.WriteLine(string.Format("{0}, dispečer {1}", DateTime.Now, this._menuStateProvider.FlightDispatcher));
				sw.WriteLine(string.Format("Volací znak, Typ, Posádka, Úkol, Vzlet, Přistání, Doba letu"));
				foreach(var plane in TimekeeperBlockCollection)
				{
					int minutes = 0;
					if(plane.Landing != null && plane.TakeOff != null && plane.TakeOff < plane.Landing)
					{
						minutes = (int)Math.Ceiling(plane.Landing.Subtract(plane.TakeOff).TotalMinutes);
					}

					sw.WriteLine(string.Format("{0},{1},{2},{3},{4},{5},{6}",
						plane.CallSign, plane.Type, plane.Crew, plane.Objective, plane.TakeOff, plane.Landing, minutes + " min"));
				}
			}
		}
	}
}
