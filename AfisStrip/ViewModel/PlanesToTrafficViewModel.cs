﻿using AfisStrip.Libs.ObservableObjects;
using AfisStrip.Model.Entities;
using AfisStrip.Model.Tables;
using AfisStrip.ViewModel.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Input;
using AfisStrip.Model.DataService;
using AfisStrip.Model.DataService.Interface;
using AfisStrip.ViewModel.Wrappers;

namespace AfisStrip.ViewModel
{
	public class PlanesToTrafficViewModel : IPageViewModel, IPlanesToTrafficViewModel
	{
		private Dictionary<string, Airplane> allPlanes = new Dictionary<string, Airplane>();



		/* 
		 R: 15.3.2018 -- nasledovne atributy su NEPOTREBNE!  Budu nahradene novou datovou strukturou objektov (PlaneUsageRecord) 

		public string NewPlaneCallSign { get; set; }
		public string NewPlaneCrew { get; set; }
		public string NewPlaneObjective { get; set; }
		public PlaneUsageTypeEnum NewPlanePlaneUsageTypeEnum {
			get;
			set;
		}

		private string _newTowCallSign;

		public string NewTowType { get; set; }
		public string NewTowCaptain { get; set; }

		public ObservableCollection<PlaneTow> TowsCollection { get; set; }

		*/

		/// <summary>
		/// Instancia "pseudo-zaznamu", ktory sa pouziva ako ciel pre binding hodnot z formularovych inputov. 
		/// Ked je stlacene tlacitko "Vlozit", tak sa informacie z tejto instancie zoberu, vhodne sa transformuju, 
		/// a vytvori sa nova instancia PlaneUsageRecord-u, ktory sa ulozi do PlanesToTrafficDataService-u. . 
		/// </summary>
		public PlaneUsageRecord NewPlaneUsageRecord { get; set; }

		
		/// <summary>
		/// Zoznam aktivnych PlaneUsageRecord-ov, ktore sa maju zobrazovat na stranke v tabulke. 
		/// </summary>
		public ObservableCollection<PlaneUsageRecord> AllPlaneUsageRecords { get; set; }



		

		/// <summary>
		/// Zoznam znamych volacich znaciek lietadiel -- tento zoznam je nabindovany do AutoComplete-u. 
		/// </summary>
		public List<string> AllKnownCallSigns
		{
			get
			{
				return this._allKnownCallsigns;
			}
		}


		private List<string> _allKnownCallsigns = new List<string>() { "OK-ABC", "OK-6555", "OK-demo zoznam" };






		private IPlaneUsageRecordsDataService planeUsageRecordsDataService;
		private ITowsDataService towsDataService;
		private IStripsDataService stripsDataService;
		private IMenuStateProvider menuStateProvider;






		public List<PlaneUsageTypeEnum> PlaneUsageOptions {
			get;
			set;
		}




		/// <summary>
		/// Konstruktor 
		/// </summary>
		/// <param name="planeUsageRecordsDataService"></param>
		/// <param name="towsDataService"></param>
		/// <param name="stripsDataService"></param>
		/// <param name="menuStateProvider"></param>
		public PlanesToTrafficViewModel(IPlaneUsageRecordsDataService planeUsageRecordsDataService,
										ITowsDataService towsDataService,
										IStripsDataService stripsDataService,
										IMenuStateProvider menuStateProvider )
		{
			this.planeUsageRecordsDataService = planeUsageRecordsDataService;
			this.towsDataService = towsDataService;
			this.stripsDataService = stripsDataService;
			this.menuStateProvider = menuStateProvider;

			this.PlaneUsageOptions = this.ListAllUsageTypeOptions();


			var plane = new Airplane();

			this.NewPlaneUsageRecord = new PlaneUsageRecord(new Airplane() { Registration = "OK-BLB" });
			this.NewPlaneUsageRecord.Crew = "clovek";
			this.NewPlaneUsageRecord.PlaneUsageTypeEnum = PlaneUsageTypeEnum.TOW_PLANE;  


			//this.PlanesToTrafficCollection = this._planeUsageRecordsDataService.Planes;
			//this.TowsCollection = this._towsDataService.Tows;


			//loadAllPlanes();	// TODO: 15.3.2018 -- spustit asynchronne nacitanie lietadiel treba vyriesit inak. 
		}



/*
  -- tato metoda bola vypnuta 15.3.2018 -- vlecne sa uz zadavaju jednotne do letadel do provozu. 

		public string NewTowCallSign
		{
			get
			{
				return _newTowCallSign;
			}
			set
			{
				this._newTowCallSign = value;

				Airplane selectedPlane;
				this.allPlanes.TryGetValue(value, out selectedPlane);

				if(selectedPlane != null)
				{
					if(selectedPlane.Owner != string.Empty)
					{
						this.NewTowCaptain = selectedPlane.Owner;
						OnPropertyChanged(nameof(NewTowCaptain));
					}

					if(selectedPlane.Type != string.Empty)
					{
						this.NewTowType = selectedPlane.Type;
						OnPropertyChanged(nameof(NewTowType));
					}
				} else
				{
					this.NewTowCaptain = "";
					this.NewTowType = "";

					OnPropertyChanged(nameof(NewTowCaptain));
					OnPropertyChanged(nameof(NewTowType));
				}
			}
		}
*/


/* 
   -- tato metoda je cela zlaaaa -- "async void" ?! WTF?!  Treba ju prepisat, a tuto staru implementaciu zahodit. 

		/// <summary>
		/// Vlozi novy strip a pokusi se k nemu najit letadlo v databazi
		/// </summary>
		/// <param name="callSign"></param>
		/// <param name="isTow"></param>
		public async void insertToStrips(string callSign, bool isTow = false)
		{
			var selectedPlane = await AirplanesDataService.getInstance().GetAirplaneAsync(callSign);


			if (selectedPlane == null)
			{
				selectedPlane = new Airplane {Registration = callSign };
			}

			var planeUsage = new PlaneUsageRecord(selectedPlane, selectedPlane.Owner);

			var newStrip = new StripNG(planeUsage)
			{
				Notes = "",
				Position = ""
			};

			var stripWrapped = new StripWrapper(newStrip); //TODO: Vytvareni wrapperu

			if(isTow)
			{
				this._stripsDataService.InATZ.Insert(0, stripWrapped);
			} else
			{
				this._stripsDataService.InATZ.Add(stripWrapped);
			}
		}

*/

		public async void AddNewPlaneCommandBody(object parameter)
		{

			var pur = this.NewPlaneUsageRecord; 

			// -- najdi lietadlo podla registracky v databazi: 
			// planesDataService.FindPlaneAsync(...). 

			var airplane = new Airplane() { Registration = "OK-TMP", IsHomebasedAtThisAirport = true };


			var newPUR = new PlaneUsageRecord(airplane, pur.Crew, pur.PlaneUsageTypeEnum); 


			this.NewPlaneUsageRecord = new PlaneUsageRecord(new Airplane());	// <-- resetne hodnoty vo formulari. 
			this.OnPropertyChanged(nameof(NewPlaneUsageRecord));


			await this.planeUsageRecordsDataService.AddNewPlaneUsageRecordAsync(newPUR); 


		}



		public ICommand AddNewPlane
		{
			get
			{
				return new RelayCommand(this.AddNewPlaneCommandBody);

				return new RelayCommand((parameter) =>
				{
					/*
					 -- povodna implementacia "AddNewPlane" -- 15.3.2018 zakomentovane, pretoze ju treba komplet prerobit. 
					if (this.NewPlaneCallSign != null && this.NewPlaneCallSign != string.Empty)
					{
						PlaneToTraffic newPlane = new PlaneToTraffic()
						{
							CallSign = this.NewPlaneCallSign.ToUpper(),
							Captain = this.NewPlaneCrew,
							Objective = this.NewPlaneObjective,
							
							PropertyOfAeroclub = true
						};

						PlanesToTrafficCollection.Insert(0, newPlane);
						insertToStrips(newPlane.CallSign);

						this.NewPlaneCallSign = "";
						this.NewPlaneCrew = "";
						this.NewPlaneObjective = "";
						OnPropertyChanged(nameof(NewPlaneCallSign));
						OnPropertyChanged(nameof(NewPlaneCrew));
						OnPropertyChanged(nameof(NewPlaneObjective));
					}
					*/ 


				});
			}
		}




/*
   -- tato metoda zakomentovana 15.3.2018 -- vlecne sa uz pridavaju jednotne ako letadla do provozu. 

		public ICommand AddTow
		{
			get
			{
				return new RelayCommand((parameter) =>
				{
					if(this.NewTowCallSign != null && this.NewTowCallSign != string.Empty)
					{
						PlaneTow tow = new PlaneTow()
						{
							CallSign = this.NewTowCallSign.ToUpper(),
							Type = this.NewTowType,
							Captain = this.NewTowCaptain,
							PropertyOfAeroclub = true
						};

						this.TowsCollection.Insert(0, tow);
						insertToStrips(tow.CallSign, true);

						this.NewTowCallSign = "";
						this.NewTowType = "";
						this.NewTowCaptain = "";
						OnPropertyChanged(nameof(NewTowCallSign));
						OnPropertyChanged(nameof(NewTowType));
						OnPropertyChanged(nameof(NewTowCaptain));
					}
				});
			}
		}
*/



/*
  -- tuto metodu "async void" -- inicializator property -- je treba prepisat! Asynchronne nacitanie sa moderne riesi uplne inym sposob (napr. BgLazyLoader-om, alebo z nejakej Async kniznice). 
		/// <summary>
		/// Nacte vsechny zname letadla jako dvojici (volaci znak, letadlo)
		/// </summary>
		private async void loadAllPlanes()
		{
			this.allPlanes = await AirplanesDataService.getInstance().ListAllKnownAirplanesDictionaryAsync();
			OnPropertyChanged(nameof(CallSigns));
		}
*/

/*
   -- metoda OnMenuChanged k 15.3.2018 nemala ziadnu referenciu -- je zrejme zbytocna. 
		private void OnMenuChanged(object sender, PropertyChangedEventArgs e)
		{
			if(e.PropertyName == "FilenameExport")
			{
				var a = this._menuStateProvider;
				Export(a.GetType().GetProperty(e.PropertyName).GetValue(a, null).ToString());
			}
		}
*/

		/// <summary>
		/// Export do CSV suboru? 
		/// </summary>
		/// <param name="path"></param>
		public void ExportToCSV(string path)
		{
			using(StreamWriter sw = new StreamWriter(path, false, Encoding.Default))
			{
				sw.WriteLine(string.Format("sep=,"));
				sw.WriteLine(string.Format("{0}, dispečer {1}", DateTime.Now, this.menuStateProvider.FlightDispatcher));
				sw.WriteLine(string.Format("Volací znak, Posádka, Typ použití"));
				foreach(var planeUsage in this.AllPlaneUsageRecords)
				{
					sw.WriteLine(string.Format("{0},{1},{2}",
						planeUsage.Plane.Registration, planeUsage.Crew, planeUsage.PlaneUsageTypeEnum));
				}
			}
		}


		/// <summary>
		/// Vyprodukuje zoznam moznosti, ktore obsahuje enum PlaneUsageRecord. 
		/// </summary>
		/// <returns></returns>
		private List<PlaneUsageTypeEnum> ListAllUsageTypeOptions()
		{
			var options = Enum.GetValues(typeof(PlaneUsageTypeEnum));
			return options.OfType<PlaneUsageTypeEnum>().ToList();
		}

	}
}
