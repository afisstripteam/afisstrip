﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using AfisStrip.Model.Entities;
using AfisStrip.Model.Enums;
using AfisStrip.ViewModel.Wrappers;

namespace AfisStrip.Converters
{
	/// <inheritdoc />
	/// <summary>
	/// Tento konvertor se stara o prevod stavu stripu na barvu pozadi stripu.
	/// 
	/// Defaultne je bily, pokud je SAFETY_CHECK pak blika mezi cervenou a bilou, pokud je INCEFA pak je cerveny.
	/// Pri priletu je modry a pri odletu do zelena.
	/// </summary>
	public class BackgroundColorMultiValueConverter : IMultiValueConverter
	{
		private static readonly SolidColorBrush DEFAULT_COLOR = Brushes.White;
		private static readonly SolidColorBrush INCEFA_COLOR = Brushes.Red;
		private static readonly SolidColorBrush ARRIVAL_COLOR = Brushes.MediumTurquoise;
		private static readonly SolidColorBrush DEPARTURE_COLOR = Brushes.LightGreen;

		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			var stripCheckStatus = values[0] is StripCheckStatusEnum ? (StripCheckStatusEnum) values[0] : StripCheckStatusEnum.UNKNOWN;
			var stripCategory = values[1] is FlightCategory ? (FlightCategory) values[1] : FlightCategory.UNKNOWN;

			if (stripCheckStatus == StripCheckStatusEnum.INCERFA)
			{
				return INCEFA_COLOR;
			}

			if (stripCheckStatus == StripCheckStatusEnum.SAFETY_CHECK)
			{
				var seconds = (int)DateTime.Now.Second;
				if (seconds % 2 == 0)
				{
					return INCEFA_COLOR;
				}

				return DEFAULT_COLOR;
			}

			if (stripCategory == FlightCategory.DEPARTURE)
			{
				return DEPARTURE_COLOR;
			}

			if (stripCategory == FlightCategory.TRANSIT)
			{
				return ARRIVAL_COLOR;
			}

			return DEFAULT_COLOR;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
