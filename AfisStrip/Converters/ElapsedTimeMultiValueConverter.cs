﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace AfisStrip.Converters
{
	public class ElapsedTimeMultiValueConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(values[0] is DateTime) || !(values[1] is DateTime)) {
				return DependencyProperty.UnsetValue;
			}

			if (DateTime.Now.Subtract((DateTime)values.First()).TotalMinutes >= 5 /*SettingsProvider.GetInstance.AppSettings.INCFAAfterMinutes*/ && (bool)values[3]) {//TODO: settingsprovider inj
				return "INCERFA";
			} else if (DateTime.Now.Subtract((DateTime)values.First()).TotalMinutes >= 5/*SettingsProvider.GetInstance.AppSettings.SafetyCheckAfterMinutes */&& (bool)values[3])//TODO: settingsprovider inj
			{
				var message = "SAFETY CHECK";
				var seconds = (int)DateTime.Now.Second;
				if (seconds % 2 == 0 && !values[2].Equals(message)) {
					var mediaPlayer = new MediaPlayer();
					mediaPlayer.Open(new Uri(Environment.CurrentDirectory + @"\alert.wav"));
					mediaPlayer.Play();
				}

				return message;
			}

			return string.Format("{0:HH:mm} (-{1:N0} min)", (DateTime)values.First(), DateTime.Now.Subtract((DateTime)values.First()).TotalMinutes);
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
