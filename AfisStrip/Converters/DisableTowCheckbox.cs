﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace AfisStrip.Converters
{
    class DisableTowCheckbox : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string strValue = value.ToString();
            if(strValue.Length >= 4)
                return false;
            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
