﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using AfisStrip.Model.Entities;

namespace AfisStrip.Converters
{
	class PlaneStatusConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if(!(values.First() is DateTime) || values[2] == DependencyProperty.UnsetValue)
				return DependencyProperty.UnsetValue;

			if(DateTime.Now.Subtract((DateTime)values.First()).TotalMinutes >= 5 /*SettingsProvider.GetInstance.AppSettings.INCFAAfterMinutes*/ && (bool)values[3])//TODO: settingsprovider inj
				return "INCERFA";
			else if(DateTime.Now.Subtract((DateTime)values.First()).TotalMinutes >= 5/*SettingsProvider.GetInstance.AppSettings.SafetyCheckAfterMinutes */&& (bool)values[3])//TODO: settingsprovider inj
			{
				var message = "SAFETY CHECK";
				var seconds = (int)DateTime.Now.Second;
				if(seconds % 2 == 0 && !values[2].Equals(message))
				{
					var mediaPlayer = new MediaPlayer();
					mediaPlayer.Open(new Uri(Environment.CurrentDirectory + @"\alert.wav"));
					mediaPlayer.Play();
				}

				return message;
			} else
			{
				var status = (FlightStatus)values[2];
				var category = (FlightCategory)values[5];
				var returnMessage = status + " / " + category;
				return returnMessage;
			}
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
