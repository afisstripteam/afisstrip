﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using AfisStrip.Model.Entities;

namespace AfisStrip.Converters
{
	public class PlaneUsageTypeConverter : IValueConverter
	{
		
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			PlaneUsageTypeEnum v = (PlaneUsageTypeEnum)value;
			string result; 

			switch (v) {
				case PlaneUsageTypeEnum.UNKNOWN:
					result = "neznámé";
					break;

				case PlaneUsageTypeEnum.TOW_PLANE:
					result = "neznámé";
					break;

				case PlaneUsageTypeEnum.WINCH_FLIGHTS:
					result = "neznámé";
					break;

				case PlaneUsageTypeEnum.LOCAL_FLIGHT:
					result = "neznámé";
					break;

				case PlaneUsageTypeEnum.OUT_AND_RETURN:
					result = "neznámé";
					break;

				case PlaneUsageTypeEnum.OTHER:
					result = "neznámé";
					break;

				default:
					result = "chyba";
					break;

			}


			return result; 
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}

}
