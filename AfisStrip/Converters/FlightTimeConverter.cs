﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace AfisStrip.Converters
{
    public class FlightTimeConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if(!(values.First() is DateTime) || !(values[1] is DateTime))
                return DependencyProperty.UnsetValue;

            var timeTakeOff = (DateTime)values[0];
            var timeLanding = (DateTime)values[1];

            if(timeTakeOff > timeLanding)
                return string.Empty;

            TimeSpan ts = timeLanding.Subtract(timeTakeOff);
            var diff = Math.Floor(ts.TotalMinutes);

            return string.Format("{0} min", diff);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
