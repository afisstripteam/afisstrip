﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace AfisStrip.Converters
{
	public class ElapsedTimeConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var pastDatetime = value;

			if (!(pastDatetime is DateTime)) {
				return DependencyProperty.UnsetValue;
			}

			return string.Format("{0:HH:mm} (-{1:N0} min)", (DateTime)pastDatetime, DateTime.Now.Subtract((DateTime)pastDatetime).TotalMinutes);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
