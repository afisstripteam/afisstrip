﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using AfisStrip.ViewModel.Interfaces;

namespace AfisStrip.View
{
	/// <summary>
	/// Interaction logic for Strips.xaml
	/// </summary>
	public partial class Strips : UserControl
	{
		public Strips(IStripsViewModel viewModel)
		{
			this.InitializeComponent();
			this.DataContext = viewModel;
		}

		private void UIElement_OnMouseDown(object sender, MouseButtonEventArgs e)
		{
			Keyboard.Focus(sender as Border);
			e.Handled = true;
		}

		private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
		{
			var btn = sender as Button;
			var sp = VisualTreeHelper.GetParent(btn) as StackPanel;
			if (sp?.Children == null) {
				return;
			}

			ItemsControl towPlanesContainer = null;

			foreach (var child in sp.Children) {
				if (!(child is ItemsControl)) {
					continue;
				}

				var container = child as ItemsControl;
				if (container.Name == "TowPlaneList") {
					towPlanesContainer = container;
					break;
				}
			}

			if (towPlanesContainer == null) {
				return;
			}

			var newWidth = 0;
			var buttonStyle = this.FindResource("StripBtn") as Style;

			if (Math.Abs(towPlanesContainer.Width) < 0.5) {
				var btnWidth = 0;

				foreach (Setter s in buttonStyle.Setters) {
					if (s.Property == WidthProperty) {
						btnWidth = (int)Math.Ceiling((double)s.Value);
					}
				}

				newWidth = btnWidth * towPlanesContainer.Items.Count;
			}
			
			var animation = new DoubleAnimation(newWidth, new Duration(TimeSpan.FromMilliseconds(200)));
			towPlanesContainer.BeginAnimation(ItemsControl.WidthProperty, animation);
		}
	}
}
