﻿using System.Windows;
using System.Windows.Media;
using Syncfusion.Windows.Controls.Input;

namespace AfisStrip.View
{
    /// <summary>
    /// Interaction logic for ChangeDispatcher.xaml
    /// </summary>
    public partial class ChangeDispatcher : Window
    {
        public string FlightDispatcher
        {
            get
            {
                return FlightDispatcherName.Text + " " + FlightDispatcherSurname.Text;
            }
        }

        public ChangeDispatcher()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var ok = true;

            if(FlightDispatcherName.Text == null || FlightDispatcherName.Text == "")
            {
                FlightDispatcherName.BorderBrush = Brushes.Red;
                ok = false;
            } else
            {
                FlightDispatcherName.ClearValue(SfTextBoxExt.BorderBrushProperty);
            }

            if(FlightDispatcherSurname.Text == null || FlightDispatcherSurname.Text == "")
            {
                FlightDispatcherSurname.BorderBrush = Brushes.Red;
                ok = false;
            } else
            {
                FlightDispatcherSurname.ClearValue(SfTextBoxExt.BorderBrushProperty);
            }

            if(ok)
            {
                DialogResult = true;
            }
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
