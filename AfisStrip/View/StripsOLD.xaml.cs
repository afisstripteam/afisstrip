﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using AfisStrip.ViewModel.Interfaces;

namespace AfisStrip.View
{
	/// <summary>
	/// Interaction logic for Strips.xaml
	/// </summary>
	public partial class StripsOLD : UserControl
	{
		public StripsOLD(IStripsViewModel viewModel)
		{
			InitializeComponent();
			this.DataContext = viewModel;
		}
		
		private void AutoCompleteBox_LostFocus(object sender, RoutedEventArgs e)
		{
			AutoCompleteBox autocompleteBox = (AutoCompleteBox)sender;

			if(autocompleteBox.Text == null || autocompleteBox.Text == "")
			{
				autocompleteBox.BorderBrush = Brushes.Red;
			} else
			{
				autocompleteBox.ClearValue(AutoCompleteBox.BorderBrushProperty);
			}
		}
	}
}
