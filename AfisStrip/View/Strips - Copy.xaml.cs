﻿using AfisStrip.Libs.Hardcodet;
using AfisStrip.Libs.ObservableObjects;
using AfisStrip.Model.Tables;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AfisStrip.View
{
    /// <summary>
    /// Interaction logic for Strips.xaml
    /// </summary>
    public partial class Strips : UserControl
    {
        /// <summary>
        /// DraggedItem Dependency Property
        /// </summary>
        public static readonly DependencyProperty DraggedItemProperty =
            DependencyProperty.Register("DraggedItem", typeof(Strip), typeof(Strips));

        /// <summary>
        /// Gets or sets the DraggedItem property.  This dependency property 
        /// indicates ....
        /// </summary>
        public Strip DraggedItem
        {
            get { return (Strip)GetValue(DraggedItemProperty); }
            set { SetValue(DraggedItemProperty, value); }
        }

        public Strips()
        {
            InitializeComponent();
        }        

        private void DataGrid_OpenDetail(object sender, MouseButtonEventArgs e)
        {
            DataGrid dg = sender as DataGrid;
            if(dg == null)
                return;
            if(dg.RowDetailsVisibilityMode == DataGridRowDetailsVisibilityMode.VisibleWhenSelected)
                dg.RowDetailsVisibilityMode = DataGridRowDetailsVisibilityMode.Collapsed;
            else
                dg.RowDetailsVisibilityMode = DataGridRowDetailsVisibilityMode.VisibleWhenSelected;
        }

        public bool IsDragging { get; set; }

        private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var row = UIHelpers.TryFindFromPoint<DataGridRow>((UIElement)sender, e.GetPosition(stripsGrid));
            if(row == null || row.Item.ToString() == "{NewItemPlaceholder}" || row.IsEditing)
                return;

            //set flag that indicates we're capturing mouse movements
            IsDragging = true;
            DraggedItem = (Strip)row.Item;
        }

        /// <summary>
        /// Completes a drag/drop operation.
        /// </summary>
        private void OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if(!IsDragging || stripsGrid.SelectedItem == null || stripsGrid.SelectedItem.ToString() == "{NewItemPlaceholder}")
            {
                return;
            }

            //get the target item
            Strip targetItem = (Strip)stripsGrid.SelectedItem;

            if(targetItem == null || !ReferenceEquals(DraggedItem, targetItem))
            {
                try
                {
                    TrulyObservableCollection<Strip> StripsList = (TrulyObservableCollection <Strip>) stripsGrid.ItemsSource;


                    //get target index
                    var sourceIndex = StripsList.IndexOf(DraggedItem);
                    var targetIndex = StripsList.IndexOf(targetItem);

                    //move source at the target's location                    
                    StripsList.Move(sourceIndex, targetIndex);

                    for(var i = 0; i < StripsList.Count; ++i)
                    {                     
                        StripsList[i].Order = i;
                    }

                        //select the dropped item
                        stripsGrid.SelectedItem = DraggedItem;

                } catch(Exception ex)
                {

                    Debug.WriteLine(ex);
                }
            }

            //reset
            ResetDragDrop();
        }

        /// <summary>
        /// Closes the popup and resets the
        /// grid to read-enabled mode.
        /// </summary>
        private void ResetDragDrop()
        {
            IsDragging = false;
        }

        /// <summary>
        /// Updates the popup's position in case of a drag/drop operation.
        /// </summary>
        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if(!IsDragging || e.LeftButton != MouseButtonState.Pressed)
                return;

            //make sure the row under the grid is being selected
            Point position = e.GetPosition(stripsGrid);
            var row = UIHelpers.TryFindFromPoint<DataGridRow>(stripsGrid, position);
            if(row != null)
                stripsGrid.SelectedItem = row.Item;
        }
    }
}
