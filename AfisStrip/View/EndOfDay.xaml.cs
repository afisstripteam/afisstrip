﻿using System;
using System.Windows;
using AfisStrip.ViewModel.Interfaces;

namespace AfisStrip.View
{
	/// <summary>
	/// Interaction logic for EndOfDay.xaml
	/// </summary>
	public partial class EndOfDay : Window
	{
		public EndOfDay(IEndOfDayViewModel viewModel)
		{
			InitializeComponent();
			this.DataContext = viewModel;
		}

		private void Close(object sender, RoutedEventArgs e)
		{
			Environment.Exit(0);
		}

		private void Cancel(object sender, RoutedEventArgs e)
		{
			this.DialogResult = false;
		}
	}
}
