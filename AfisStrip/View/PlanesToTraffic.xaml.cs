﻿using System.Windows.Controls;
using AfisStrip.ViewModel.Interfaces;

namespace AfisStrip.View
{
	/// <summary>
	/// Interaction logic for PlanesToTraffic.xaml
	/// </summary>
	public partial class PlanesToTraffic : UserControl
	{
		public PlanesToTraffic(IPlanesToTrafficViewModel viewModel)
		{
			InitializeComponent();
			this.DataContext = viewModel;
		}
	}
}
