﻿using System.Windows.Controls;
using AfisStrip.ViewModel.Interfaces;

namespace AfisStrip.View
{
	/// <summary>
	/// Interaction logic for Traffic.xaml
	/// </summary>
	public partial class TimekeeperBlock : UserControl
	{
		public TimekeeperBlock(ITimekeeperBlockViewModel viewModel)
		{
			InitializeComponent();
			this.DataContext = viewModel;
		}
	}
}
