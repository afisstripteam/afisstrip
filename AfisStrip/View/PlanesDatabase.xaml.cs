﻿using System.Windows.Controls;
using AfisStrip.ViewModel.Interfaces;

namespace AfisStrip.View
{
	/// <summary>
	/// Interaction logic for FlightBook.xaml
	/// </summary>
	public partial class PlanesDatabase : UserControl
	{
		public PlanesDatabase(IPlanesDatabaseViewModel viewModel)
		{
			InitializeComponent();
			this.DataContext = viewModel;
		}
	}
}
