﻿using System.Windows.Controls;
using AfisStrip.ViewModel.Interfaces;

namespace AfisStrip.View
{
	/// <summary>
	/// Interaction logic for FlightBook.xaml
	/// </summary>
	public partial class FlightBook : UserControl
	{
		public FlightBook(IFlightBookViewModel viewModel)
		{
			InitializeComponent();
			this.DataContext = viewModel;
		}
	}
}
